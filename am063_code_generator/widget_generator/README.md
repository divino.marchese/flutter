
# widget generator

**[221218]** un pacchetto da noi creato per vedere come generare automaticamente codice: esso ci dà la ricetta eseguita da `build_runner`. La configurazione attuale prevede in `pubspec.yaml`
```yaml
dependencies:
  flutter:
    sdk: flutter
  analyzer: ^5.1.1
  build: ^2.3.1
  source_gen: ^1.2.6
  build_config: ^1.1.1
```

## creazione del pacchetto

Per creare un pacchetto usiamo un *template* (vedi anche riferimenti qui sotto)
```
flutter create --template=package widget_generator
```

## Configurazione

Il pacchetto `build_config`, [qui(https://pub.dev/packages/build_config), ci permette di scrivere un file di configurazione `build.yaml` 
```yaml
targets:
...
builders:
...
post_process_builders:
...
global_options:
...
```
La sintassi nel dettaglio è descritta [qui](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md#builderkey). Per i `target:` la sintassi è `foo:bar`, esiste `$default` per indicare il pacchetto ed il file omonimo (come nel nostro caso), quindi arriviamoa chiudere la sezione target nel modo seguente
```yaml
targets:
  $default:
    builders:
      annotation_builder:
        enabled: true
```
SI tratta quindi di secrivere i singoli `Builder`
```yaml
builders:
  annotation_builder:
    import: 'package:widget_generator/widget_generator.dart'
    builder_factories: ['annotationBuilder']
    build_extensions: { '.dart': ['.g.dart'] }
    auto_apply: root_package
    build_to: source
```
i vari campi sono abbastanza auto-descrittivi, `auto_apply: root_package` significa che la ricerca per la generazione sarà limitata alla sola applicazione (o pacchetto), `build_to: source` indica che il file generato `blaba.g.dart` sarà nell'albero dei sorgenti e non nella `cache`, quindi rimarra lì in modo permanente. Si tratta ora di scrivere i sorgenti cui la configurazione fa riferimento.

## annotation

Per definire le *annotation* basta creare una classe con un costruttore costante ed eventualmente dei campi che diventeranno le *property* dell'annotation medesima.

## gernerator

Questo è il cuore della generazione. La classe che estendiamo è `GeneratorForAnnotation<T>`, [qui](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation-class.html) per le API il pacchetto non è `build` ma una sua estensione `source_gen`, `T` si riferisce alla classe della *annotation*. Il metodo da leggere e``dynamic generateForAnnotation element()` dando, come sempre, uno sguardo alle API [qio](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation/generateForAnnotatedElement.html).

### i passi della costruzione

Qui entra in gioco il pacchetto `build`, [qui](https://pub.dev/documentation/build/latest/) per le API, che propone le classi basic come `Builder` e `BuildStep`. Per `BuildStep`, vedi API [ui](https://pub.dev/documentation/build/latest/build/BuildStep-class.html) abbiamo la seguente definizione
> A single step in a build process. ... This represents a single inputId, logic around resolving as a library, and the ability to read and write assets as allowed by the underlying build system.

### Semantic Model

Il pacchetto è `analizer`, [qui](https://pub.dev/packages/analyzer) (Dart non ha la *reflection* come Java)
> This package provides a library that performs static analysis of Dart code. 

La classe base `Element`, [qui](https://pub.dev/documentation/analyzer/latest/dart_element_element/Element-class.html) per le API, è la classe base per i nomi del sorgente..
> Generally speaking, the element model is a semantic model of the program that represents things that are declared with a name and hence can be referenced elsewhere in the code.

## registrazione del *builder*

```dart
Builder annotationBuilder(BuilderOptions options) =>
    LibraryBuilder(WidgetGenerator(), generatedExtension: '.g.dart');
```
`BuilderOption` non ha gran documentazione, vedi [qui](https://pub.dev/documentation/build/latest/build/BuilderOptions-class.html). `LibraryBuilder` è di `source_gen`, [qui](https://pub.dev/documentation/source_gen/latest/source_gen/LibraryBuilder-class.html) per le API.

## altri materiali

[1] "YAML tutorial: Get started in 5 minutes" [qui](https://www.educative.io/blog/yaml-tutorial).  
[2] "How to develop a Flutter package | Flutter Tutorial" [qui](https://www.sandromaglione.com/techblog/how-to-develop-a-flutter-package-flutter-tutorial).

    
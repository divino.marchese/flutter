library widget_generator;

import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'package:widget_generator/builders/builder.dart';

Builder annotationBuilder(BuilderOptions options) =>
    LibraryBuilder(WidgetGenerator(), generatedExtension: '.g.dart');

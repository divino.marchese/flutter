import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import '../annotations.dart';

class WidgetGenerator extends GeneratorForAnnotation<MyAnnotation> {
  @override
  generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    // String className = element.displayName;
    // String path = buildStep.inputId.path;
    String prop = annotation.peek('prop')?.stringValue ?? '';
    StringBuffer buf = StringBuffer('String prop = "$prop";\n');
    if (element is ClassElement) {
      buf.write('String className = "${element.displayName}";\n');
      for (Element e in element.children) {
        if (e is FieldElement) {
          buf.write('bool ${e.displayName} = true; \n');
        }
      }
    } else {
      buf.write('String message = "NADA"');
    }
    return buf.toString();
  }
}

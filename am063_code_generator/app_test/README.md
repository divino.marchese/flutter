# app_test

Per generare il file
```
flutter pub run build_runner build
```
Avendo installato `build_runner`, [qui](https://pub.dev/packages/build_runner) la home. Il file `bubspec.yaml`
porta le seguenti modifiche
```yaml
...
dependencies:
  flutter:
    sdk: flutter
  widget_generator:
    path: ../widget_generator/
...
dev_dependencies:
  flutter_test:
    sdk: flutter
  build_runner: ^2.3.3
...
```
Usando le annotation per la classe `Model` in `model.dart` verrà generato un semplice sorgente `model.g.dart`.


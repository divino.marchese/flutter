import 'package:app_test/model.g.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  const MyText({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("A generated widget",
            style: Theme.of(context).textTheme.displayMedium),
        const SizedBox(
          height: 20,
        ),
        Text("annotation property: $prop",
            style: Theme.of(context).textTheme.displayMedium),
        Text("class name: $className",
            style: Theme.of(context).textTheme.displaySmall),
        Text(
            name
                ? "name is a property of $className"
                : "name is not a property of $className",
            style: Theme.of(context).textTheme.displaySmall),
        Text(
            age
                ? "age is a property of $className"
                : "age is not a property of $className",
            style: Theme.of(context).textTheme.displaySmall),
      ],
    );
  }
}

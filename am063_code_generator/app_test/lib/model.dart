import 'package:widget_generator/annotations.dart';

@MyAnnotation(prop: "test prop")
class Model {
  final String name = "Ciccio";
  final int age = 23;
}

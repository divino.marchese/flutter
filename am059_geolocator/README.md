# am059_geolocator

Il pacchetto utilizzato è
- `geolocator` **[home](https://pub.dev/packages/geolocator)** 

Su `AndroidManifest.xml` vanno dati i permessi opportuni, vedi **[documentazione](https://developer.android.com/training/location/permissions)**. L'utente *runtime* può chiedere solo ` ACCESS_COARSE_LOCATION`. La nostra applicazione prevede
l`uso *runtime* dei permessi.

La classe `Position` **[API](https://pub.dev/documentation/geolocator_platform_interface/latest/geolocator_platform_interface/Position-class.html)** possiede varie *property*
- `accuracy` di tipo `double` stima l`accuratezza in orizzontale.
- `altitude` di tipo `double`
- `floor` di tipo `int?` indica quando disponibile il piano dell`edificio.
- `heading` di tipo `double`
- `latitude` di tipo `double` in gradi con decimali
- `longitude` di tipo `double` in gradi con decimali
- `speed` di tipo `double` in metri al secondo
- `speedAccuracy` di tipo `double`
 - `timestamp` di tipo `DateTime?`
   
Nel nostro esempio ascoltiamo uno *stream* in modo asincrono la possione può essere interrogata ad esempio come segue
```dart
void updateLocation() async {
  try {
    Position newPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high)
      .timeout(const Duration(seconds: 5));
    setState(() {
      _position = newPosition;
    });
  } catch (e) {
    debugPrint('Error: $e');
  }
}
```

## emulatore

Abilitare i permessi manualmente `location`.
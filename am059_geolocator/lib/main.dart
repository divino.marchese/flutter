import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am059 geolocator',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am059 geolocator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late StreamSubscription<Position> _subscription;
  Position _position = Position(
      longitude: 0,
      latitude: 0,
      timestamp: DateTime.now(),
      accuracy: 0,
      altitude: 0,
      heading: 0,
      speed: 0,
      speedAccuracy: 0);

  @override
  void initState() {
    super.initState();
    LocationSettings settings = const LocationSettings(
        accuracy: LocationAccuracy.high, distanceFilter: 1);
    checkPermission();
    _subscription = Geolocator.getPositionStream(locationSettings: settings)
        .listen((Position position) {
      debugPrint("update location");
      setState(() {
        _position = position;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _subscription.cancel();
  }

  Future<void> checkPermission() async {
    final GeolocatorPlatform geolocatorPlatform = GeolocatorPlatform.instance;
    bool serviceEnabled = await geolocatorPlatform.isLocationServiceEnabled();
    if (!serviceEnabled) {
      debugPrint("service not enabled, try to enable");
      await Geolocator.requestPermission();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                height: 180,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'geo data',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('timestamp: ${_position.timestamp}'),
                    Text('accurancy: ${_position.accuracy}'),
                    Text('speed: ${_position.speed}'),
                    Text('altitude: ${_position.altitude}'),
                    Text('speedAccuracy: ${_position.speedAccuracy}'),
                    Text('heading: ${_position.heading}'),
                    Text('floor: ${_position.floor}'),
                    Text('altitude: ${_position.altitude}'),
                  ],
                )),
            Text(
              'latitude: ${_position.latitude}',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              'longitude: ${_position.longitude}',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';

class DeviceTile extends StatelessWidget {
  const DeviceTile(
      {Key? key,
      required this.frb,
      required this.device,
      required this.stopScan})
      : super(key: key);

  final DiscoveredDevice device;
  final FlutterReactiveBle frb;
  final Function stopScan;

  _connect(BuildContext context) async {
    frb
        .connectToDevice(
      id: device.id,
      connectionTimeout: const Duration(seconds: 3),
    )
        .listen((update) {
      switch (update.connectionState) {
        case DeviceConnectionState.connected:
          _displayDialog(context);
          break;
        default:
          debugPrint(update.connectionState.toString());
      }
    }, onError: (Object error) {
      debugPrint("error");
    });
  }

  Future<List<Card>> _discoverServices() async {
    List<Card> ret = [];
    var services = await frb.discoverServices(device.id);
    for (final service in services) {
      Uuid sUuid = service.serviceId;
      List<Text> characteristics = await _getCharacteristics(service);
      ret.add(Card(
          margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
          color: const Color.fromARGB(212, 255, 252, 252),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(sUuid.toString(),
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          color: Colors.red)),
                  ...characteristics
                ]),
          )));
    }
    return ret;
  }

  Future<List<Text>> _getCharacteristics(DiscoveredService service) async {
    List<Text> ret = <Text>[];
    for (final characteristic in service.characteristics) {
      Uuid cUuid = characteristic.characteristicId;
      ret.add(Text(cUuid.toString(),
          textAlign: TextAlign.left,
          style: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
          )));
      if (characteristic.isReadable) {
        final characteristic = QualifiedCharacteristic(
            serviceId: service.serviceId,
            characteristicId: cUuid,
            deviceId: device.id);
        List<int> values = [];
        final response = await frb.readCharacteristic(characteristic);
        for (final n in response) {
          values.add(n);
        }
        ret.add(Text(
          'values: ${values.toString()}',
          style: const TextStyle(
            fontSize: 13,
          ),
        ));
      } else {
        ret.add(const Text("<no readable>"));
      }
    }
    return ret;
  }

  // only for test
  Future<List<Text>> _discoverTest() async {
    await frb.discoverServices(device.id);
    debugPrint("discovered services");
    return [const Text('services dicovered')];
  }

  _disconnect(BuildContext context) async {
    frb.clearGattCache(device.id);
    Navigator.of(context).pop();
    await stopScan();
  }

  Future<void> _displayDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(device.name.toString(),
              style:
                  const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          content: SizedBox(
              height: 300,
              width: 350,
              child: SingleChildScrollView(
                  child: FutureBuilder<List<Widget>>(
                      future: _discoverServices(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Column(children: snapshot.data!);
                        } else {
                          return const Center(
                            child: Text("searching services ..."),
                          );
                        }
                      }))),
          actions: <Widget>[
            TextButton(
                child: const Text('disconnect'),
                onPressed: () {
                  _disconnect(context);
                })
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    String name = device.name;
    if (name == '') {
      name = '<no name>';
    }
    return Card(
      elevation: 3,
      child: ListTile(
        leading: const Icon(Icons.bluetooth),
        title: Text(name),
        subtitle: Text('id (MAC): ${device.id}'),
        onTap: () {
          _connect(context);
        },
      ),
    );
  }
}

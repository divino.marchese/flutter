import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:permission_handler/permission_handler.dart';

import 'device_tile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am064 ble',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am064 ble'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _flutterReactiveBle = FlutterReactiveBle();
  final List<DiscoveredDevice> _devices = <DiscoveredDevice>[];
  bool _empty = true;
  bool _scanStarted = false;
  StreamSubscription<DiscoveredDevice>? _scanSubscription;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scanSubscription!.cancel();
  }

  Future<bool> _managePermissions() async {
    if (Platform.isAndroid) {
      PermissionStatus scan = await Permission.bluetoothScan.request();
      PermissionStatus connect = await Permission.bluetoothConnect.request();
      PermissionStatus location = await Permission.location.request();
      if (scan == PermissionStatus.granted &&
          connect == PermissionStatus.granted &&
          location == PermissionStatus.granted) {
        return true;
      }
    } else if (Platform.isIOS) {
      return true;
    }
    return false;
  }

  void _startScan() async {
    await _scanSubscription?.cancel();
    setState(() {
      _devices.clear();
      _empty = true;
    });
    bool permGranted = await _managePermissions();
    if (permGranted) {
      setState(() {
        _scanStarted = true;
        _scanSubscription = _flutterReactiveBle.scanForDevices(
            withServices: [], scanMode: ScanMode.lowLatency).listen(
          (device) {
            _checkAndAdd(device);
          },
        );
      });
    }
  }

  Future<void> _stopScan() async {
    await _scanSubscription?.cancel();
    setState(() {
      _scanStarted = false;
    });
  }

  void _checkAndAdd(DiscoveredDevice device) {
    bool idPresent = false;
    for (var element in _devices) {
      if (device.id == element.id) {
        idPresent = true;
      }
    }
    if (!idPresent) {
      setState(() {
        _devices.add(device);
        debugPrint("added device: ${device.id}");
      });
    }
    if (_devices.isNotEmpty) {
      setState(() {
        _empty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: _scanStarted == false
              ? const Text("Push botton and start scanning ...")
              : _empty
                  ? const CircularProgressIndicator()
                  : ListView(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      children: _devices
                          .map((device) => DeviceTile(
                                frb: _flutterReactiveBle,
                                device: device,
                                stopScan: _stopScan,
                              ))
                          .toList())),
      floatingActionButton: FloatingActionButton(
        onPressed: _startScan,
        tooltip: 'Increment',
        child: _scanStarted
            ? const Icon(Icons.bluetooth_searching)
            : const Icon(Icons.bluetooth),
      ),
    );
  }
}

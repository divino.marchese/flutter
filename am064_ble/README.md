# am064_ble

**[230221]** Questa è la app per il BLE, ci siamo concentrati sul versante Android anche se dovrebbe andare bene anche per IOS.

## pubspec

Usiamo due pacchetti: una per BLE e l'altra per i permessi *runtime*
```
dependencies:
  flutter:
    sdk: flutter
  permission_handler: ^10.2.0
  flutter_reactive_ble: ^5.0.3
```

## permessi

Prima di tutto, come indicato nella documentazione, per Android, andare a modificare `build.gradle`
```
defaultConfig {
        ...
        minSdkVersion 21
        ...
    }
```
Quindi il *manifest* seguendo le indicazioni in [3]. A noi interessano `SCAN`, `CONNECT` e `LOCATION` che, si badi bene, sono *run time* quindi vanno richieste e qui ci viene in aiuto `permission_handler`, il metodo `Future<bool> _managePermissions()` chiede i permessi la prima volta che, dopo installata, viene aperta l'applicazione, in alternativa i permessi andrebbero concessi manualmente. ALtri dettagli del *manifest* si desumono dalla documentazione di `flutter_reactive_ble` e da [3] che dà ulteriore luce alla cosa.

## scansione

Un `Stream` è quanto offre il servizio di scansione. Il metodo
```dart
void _startScan() async {
    ...
        _scanSubscription = _flutterReactiveBle.scanForDevices(
            withServices: [], scanMode: ScanMode.lowLatency).listen(
          (device) {
            _checkAndAdd(device);
          },
        );
      });
    }
  }
```
usa un oggetto `FlutteReactiveBle`, il *fac totum* della libreria e cerca i *device* senza limitazione di **servizi** offerti, per i dettagli si vada alle API. Usiamo `_checkAndAdd(device)` in quanto nella scansione lo stesso *device* si ripresenta più volte e quindi non lo aggiungiamo (riconosciamo un `ìd` presente nella lista di `DiscoveredDevice`); in ANdroid `id` è un indirizzo `MAC`,il ǹame` di un *device* non è sempre presente.

## connessione, *service* e creazione del *dialog*

Il *dialog* viene creato per illustrare in *card* i *service* -- `Future<List<Card>> _discoverServices() async` -- e per ogni *service* le *characteristic* ed i valori associati (se leggibile la *characteristic*). Ecco come avviene la ricerca dei *service* (senza puntare ad una particolare *characteristic*) che offre un *device* (i `DiscoveredDevice`)
```dart
frb.connectToDevice(
    id: device.id,
    connectionTimeout: const Duration(seconds: 3),
)
```
`frb` è sempre il nostro oggetto centralizzato `FlutterReactiveBle`. Tale ricerca offre uno *stream*, ancora, i cui eventi sono del tipo `ConnectionStateUpdate`: il *device* si prepara alla connessione e ritorna eventi di stato, noi attendiamo il `DeviceConnectionState.connected`, gli eltri eventi di stato li mostriamo nel debug `debugPrint(update.connectionState.toString());`.

## `characteristic`

Ci limitiamo a leggerle, non a scrivere col metodo `Future<List<Text>> _getCharacteristics(DiscoveredService service)`.

## chiusura del *dialog*

Al momento della chiusura chiamiamo
```dart
_disconnect(BuildContext context) async {
   frb.clearGattCache(device.id);
   Navigator.of(context).pop();
   await stopScan();
}
```
L'ultimo meotodo interrompe la scansione e modifica il `context`, va fatto qui in quanto altrimenti il *dialog* associato ad un `context` lo vedrebbe modificato e salterebbe con un errore! Il metodo per la cancellazione della *cache*, non necessario,  lavora bene in Android e non IOS, in caso fare un check di piattaforma come per il metodo per i permessi *runtime*.

## nota

Una versione successiva di questo esempio userà il `provider` per distribuire in tutta l'app un oggetto centralizzato.

## materiali

[1] BLE nella documentazione di Andorid [qui](https://developer.android.com/guide/topics/connectivity/bluetooth/ble-overview).  
[2] BLE da Wikipedia (en) [qui](https://en.wikipedia.org/wiki/Bluetooth_Low_Energy).  
[3] "Bluetooth permissions" di Android [qui](https://developer.android.com/guide/topics/connectivity/bluetooth/permissions).

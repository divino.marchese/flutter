# tcp_client

Il client è pensato per l'esercitazione prevista dai laboratori di orientamento AS2324. Il file `tcp_server.dart` è un server eco preparato per le mie lezioni, esso avrà IP `10.0.2.2`.

## visual studio code - VSC

Ecco alcune sequenze di tasti utili (oltre alle solite note)
- `CTRL + k` `CTRL + o`  apriamo un progetto,
- `CTRL + k` `f` chiudiamo il prgetto aperto,
- `CTRL + SHIFT + p` lo usiamo per creare un progetto flutter scegliendo *create flutter project* o, aperto un progetto, per selezionare un *device*
- `CTRL + SHIFT + i` per identare bene il codice privo di errori.

## creazione e prime impostazioni dell'App

Per creare l'app abbiamo due possibilità: aprendo il terminale Linux `CTRL + ALT + T` e scrivendo 
```
flutter create tcp_client
```
(troveremo il nostro progetto dentro `/home/studente`) e lo apriamo
con VSC `CTRL + K` `CTRL + o` , in alternativa apriamo VSC, `CTRL + SHIFT + p` e scegliamo di creare il nuovo progetto.  Proviamo la nostra app su di un emulatore o sul nostro telefonino connesso (usando `CTRL + SHIFT + p` e *select device*). Portandoci sul file `lib?main.dart`, è il file su cui lavoreremo, modifichiamo titolo e stile per la nostra app
```dart
return MaterialApp(
    title: 'tcp client',
    theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
    useMaterial3: true,
    ),
    home: const MyHomePage(title: 'tcp client'),
);
```
Ricarichiamo la nostra applicazione (*reload*). Aggiungiamo in fondo le due funzioni di LOG
```dart
// green
void logSuccess(String msg, String name) {
  developer.log('\x1B[32m$msg\x1B[0m', name: name);
}
// red
void logError(String msg, String name) {
  developer.log('\x1B[31m$msg\x1B[0m', name: name);
}
```
ricordandoci di importare la libreria (gli altri `import` sono in testa al file `main.dart`)
```dart
import 'dart:developer' as developer;
```

## modifichiamo la *statefull widget*

Abbiamo già un'applicazione già pronta, vediamo di cancellare alcune cose in modo da avere uno *skeleton* per il nostro *client*
```dart
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
    // aggiungere poi
  }

  void _connect() {
    // aggiungere poi
  }

  void _action() {
    // aggiungere poi
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        // poi
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _connect,
        tooltip: 'connect',
        child: const Icon(Icons.start),
      ),
    );
  }

  @override
  void dispose() {
    // poi .. qui chiuderemo il socket
  }
}

// green
void logSuccess(String msg, String name) {
  developer.log('\x1B[32m$msg\x1B[0m', name: name);
}
// red
void logError(String msg, String name) {
  developer.log('\x1B[31m$msg\x1B[0m', name: name);
}

```
I metodi `void _connect()` e `void _action()` ci permetteranno di conneterci al *server* (Arduino) ed accendere o spegnere il led! Una *statefull* *widget* è una parte dell'interfaccia grafica della nostra app che viene ad aggiornarsi ogni qual volta lo stato informa il *framework* del fatto che è cambiato; qui di seguito presentiamo lo stato come valori associate a delle variabili (di stato appunto).

## lo stato

Procediamo con lo stato.
```dart
class _MyHomePageState extends State<MyHomePage> {
  Socket? _socket;

  late TextEditingController _controller;
  String _address = "10.0.2.2:3000";
  bool _connected = false;
  bool _ledOn = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }
```
- `TextEditingController` ci permette di recuperare l'indirizzo IP e la porta che poi noi immetteremo in un `TextField`,
- `_address` una variabile `String` per l'indirizzo IP e porta separati da `:`,
- `_connected` una variabile `bool` che ci dice se la nostra app è connessa o meno al server,
- `_ledOn` una variabile `bool`che ci dice se il led è acceso o spento,
- `Soxkwt? socket` ci permette di gestire la connessione TCP, per essa abbiamo bisogno di un *socket*
sul server e di uno sul *client*, questo è il socket per il client!

Nel metodo `void _initState()` creiamo il `TextEditingController`.

## i *getter*

Invece di creare nuove variabili di stato che non ci darebbero ulteriori informazioni definiamo dei *getter* che ci permettondo di rectuperare l'indirizzo **IP** e la **porta**
```dart
@override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  // getters
  String get _ip {
    int idx = _address.indexOf(":");
    return _address.substring(0, idx).trim();
  }

  int get _port {
    int idx = _address.indexOf(":");
    return int.parse(_address.substring(idx + 1).trim());
  }
```
l metodo `String trim()` applicato ad una stringa, ad esempio
```dart
String s = " ciao   ";
s = s.trim()
```
toglie spazi e caratteri *blanc* in genere. IP e porta vengono ricavate come sotto stringhe osservando che `:`
separa IP dalla porta.

## le funzioni di connessione e per accendere e spegnere il led

Eccole:
```dart
int get _port {
    int idx = _address.indexOf(":");
    return int.parse(_address.substring(idx + 1).trim());
}

void _connect() {
    String address = _controller.text.trim();
    if (address.isNotEmpty) {
      _address = address;
    }
    setState(() {
      _socket?.flush();
      _socket?.close();
    });
    Socket.connect(_ip, _port).then((sock) {
      setState(() {
        _socket = sock;
        _connected = true;
        logSuccess(_address, "my app");
      });
    }, onError: (e) {
      logError("unable to connect: $e", "my app");
      exit(1);
    });
}

void _action() {
    if (_ledOn) {
      _socket?.write("led1off");
      logSuccess("on", "my app");
      setState(() {
        _ledOn = false;
      });
} else {
      _socket?.write("led1on");
      logSuccess("off", "my app");
      setState(() {
        _ledOn = true;
      });
    }
}
```
Flutter richiede l'uso di `void setState(...)` per poter rivedere la costruzione parziale o totale della nostra interfaccia grafica.

## completiamo l'interfaccia grafica

```dart
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                height: 100,
                child: Text(
                  'LAP2 orientamento',
                  style: Theme.of(context).textTheme.headlineMedium,
                )),
            SizedBox(
              width: 200,
              height: 100,
              child: TextField(
                controller: _controller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'indirizzo:porta del server \n confermare',
                  hintText: '10.0.2.2:3000',
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: _connected ? _action : null,
              child: _connected
                  ? (_ledOn ? const Text('OFF') : const Text('ON'))
                  : const Text('disabled'),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _connect,
        tooltip: 'connect',
        child: const Icon(Icons.start),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _socket?.flush();
    _socket?.close();
    super.dispose();
  }
}
```
IL metodo `void dispose()` è l'opposto di `void build()`: il secondo viene chiamato ogni volta che lo stato viene a modificarsi con `void setState()` o inizializzato con `void initState()`, il primo quando dismettiamo lo stato e quindi l'interfaccia grafica ad esempio quando chiudiamo la nostra app.
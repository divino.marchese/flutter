import 'dart:io';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'tcp client',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'tcp client'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Socket? _socket;

  late TextEditingController _controller;
  String _address = "10.0.2.2:3000";
  bool _connected = false;
  bool _ledOn = false;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  // getters
  String get _ip {
    int idx = _address.indexOf(":");
    return _address.substring(0, idx).trim();
  }

  int get _port {
    int idx = _address.indexOf(":");
    return int.parse(_address.substring(idx + 1).trim());
  }

  void _connect() {
    String address = _controller.text.trim();
    if (address.isNotEmpty) {
      _address = address;
    }
    setState(() {
      _socket?.flush();
      _socket?.close();
    });
    Socket.connect(_ip, _port).then((sock) {
      setState(() {
        _socket = sock;
        _connected = true;
        logSuccess(_address, "my app");
      });
    }, onError: (e) {
      logError("unable to connect: $e", "my app");
      exit(1);
    });
  }

  void _action() {
    if (_ledOn) {
      _socket?.write("led1off");
      logSuccess("on", "my app");
      setState(() {
        _ledOn = false;
      });
    } else {
      _socket?.write("led1on");
      logSuccess("off", "my app");
      setState(() {
        _ledOn = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                height: 100,
                child: Text(
                  'LAP2 orientamento',
                  style: Theme.of(context).textTheme.headlineMedium,
                )),
            SizedBox(
              width: 200,
              height: 100,
              child: TextField(
                controller: _controller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'indirizzo:porta del server \n confermare',
                  hintText: '10.0.2.2:3000',
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: _connected ? _action : null,
              child: _connected
                  ? (_ledOn ? const Text('OFF') : const Text('ON'))
                  : const Text('disabled'),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _connect,
        tooltip: 'connect',
        child: const Icon(Icons.start),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _socket?.flush();
    _socket?.close();
    super.dispose();
  }
}

// green
void logSuccess(String msg, String name) {
  developer.log('\x1B[32m$msg\x1B[0m', name: name);
}

// red
void logError(String msg, String name) {
  developer.log('\x1B[31m$msg\x1B[0m', name: name);
}

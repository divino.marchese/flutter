# am051_key_stateless

**[240309]** Esercizio preparatorio al successivo sull'uso delle chiavi. Nelle `StatelessWidget` la *property* `key` è nullable e in `Widget`, [API](https://api.flutter.dev/flutter/widgets/Widget-class.html) troviamo per `key` quanto segue
> Controls how one widget replaces another widget in the tree.

E ancora.
> If the runtimeType and key properties of the two widgets are `operator==`, respectively, then the new widget replaces the old widget by updating the underlying element (i.e., by calling `Element.update` with the new widget). Otherwise, the old element is removed from the tree, the new widget is inflated into an element, and the new element is inserted into the tree.

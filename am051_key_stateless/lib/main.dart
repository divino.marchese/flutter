import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am051 key stateless',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am051 key stateless'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<OutlinedButton> buttons = <OutlinedButton>[
    OutlinedButton(
      onPressed: () {
        debugPrint('*** click ONE ***');
      },
      child: const Text("ONE"),
    ),
    OutlinedButton(
      onPressed: () {
        debugPrint('*** click TWO ***');
      },
      child: const Text("TWO"),
    ),
    OutlinedButton(
      onPressed: () {
        debugPrint('*** click THREE ***');
      },
      child: const Text("THREE"),
    ),
  ];

  void _swapButtons() {
    setState(() {
      buttons.insert(2, buttons.removeAt(1));
      buttons.insert(1, buttons.removeAt(0));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('swap buttons',
                style: Theme.of(context).textTheme.headlineSmall),
            Text('in a stateless way',
                style: Theme.of(context).textTheme.headlineSmall),
            const SizedBox(height: 30),
            buttons[0],
            const SizedBox(height: 30),
            buttons[1],
            const SizedBox(height: 30),
            buttons[2],
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _swapButtons,
        tooltip: 'swap buttons',
        child: const Icon(Icons.replay),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

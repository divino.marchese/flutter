# am067_change_notifier

Un modo pulito con cui Flutter usa il *pattern* dell'*Observer* è dato dalla classe `ChangeNotifier` [API](https://api.flutter.dev/flutter/foundation/ChangeNotifier-class.html)
```dart
class Counter with ChangeNotifier {
  int _count = 0;
  int get count => _count;

  void increment() {
    _count += 1;
    notifyListeners();
  }
}
```
Il metodo `notifyListeners()` notifica gli *observer* (ascoltatori) registrabili tramite, ad esempio
```dart
void addListener(
    VoidCallback listener
) 
```
[API](https://api.flutter.dev/flutter/foundation/ChangeNotifier/addListener.html). `ListenableBuilder`, [API](https://api.flutter.dev/flutter/widgets/ListenableBuilder-class.html), è una `StatefusWidget` che implementa `Listenable` [API](https://api.flutter.dev/flutter/foundation/Listenable-class.html) 
```dart
ListenableBuilder(
    listenable: counterNotifier,
    builder: (BuildContext context, Widget? child) {
        return Text('${counterNotifier.count}',
            style: Theme.of(context).textTheme.headlineMedium);
        },
),
```
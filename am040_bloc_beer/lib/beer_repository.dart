import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'beer_model.dart';

class BeerRepository {
  static final BeerRepository _beerRepository = BeerRepository._();
  static const int _perPage = 10;

  BeerRepository._();

  factory BeerRepository() {
    return _beerRepository;
  }

  Future<dynamic> getResponseBeers({@required int page}) async {
    try {
      final url =
          'https://api.punkapi.com/v2/beers?page=$page&per_page=$_perPage';
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final beers = jsonDecode(response.body) as List;
        return beers.map((beer) => BeerModel.fromJson(beer)).toList();
      } else {
        return "status code: ${response.statusCode}";
      }
    } catch (e) {
      return "error:" + e.toString();
    }
  }
}

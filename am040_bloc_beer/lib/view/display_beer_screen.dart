import 'package:am040_bloc_beer/bloc/beer_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../beer_repository.dart';
import 'beer_body.dart';

class DisplayBeerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BeerCubit(
        repository: BeerRepository(),
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text('am040_bloc_beer \u{1F37A}'),
        ),
        body: BeerBody(),
      ),
    );
  }
}

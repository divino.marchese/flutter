import 'package:am040_bloc_beer/beer_repository.dart';
import 'package:am040_bloc_beer/bloc/beer_cubit.dart';
import 'package:am040_bloc_beer/bloc/beer_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../beer_model.dart';
import 'beer_list_item.dart';

class BeerBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BeerCubit(repository: BeerRepository()),
      child: BeersView(),
    );
  }
}

class BeersView extends StatelessWidget {
  final List<BeerModel> _beers = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Center(
        child: BlocBuilder<BeerCubit, BeerState>(builder: (context, state) {
      if (state is BeerInitialState) {
        context.read<BeerCubit>().getBeers();
        return CircularProgressIndicator();
      } else if (state is BeerLoadingState && _beers.isEmpty) {
        context.read<BeerCubit>().isFetching = true;
        return CircularProgressIndicator();
      } else if (state is BeerSuccessState) {
        context.read<BeerCubit>().isFetching = false;
        _beers.addAll(state.beers);
      } else if (state is BeerErrorState && _beers.isEmpty) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              onPressed: () => context.read<BeerCubit>()
                ..getBeers()
                ..isFetching = true,
              icon: Icon(Icons.refresh),
            ),
            const SizedBox(height: 15),
            Text(state.error, textAlign: TextAlign.center),
          ],
        );
      }
      return ListView.separated(
        controller: _scrollController
          ..addListener(() {
            if (_scrollController.offset ==
                    _scrollController.position.maxScrollExtent &&
                !context.read<BeerCubit>().isFetching) {
              context.read<BeerCubit>()
                ..getBeers()
                ..isFetching = true;
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text("....more ...")));
            }
          }),
        itemBuilder: (context, index) => BeerListItem(_beers[index]),
        separatorBuilder: (context, index) => const SizedBox(height: 20),
        itemCount: _beers.length,
      );
    }));
  }
}

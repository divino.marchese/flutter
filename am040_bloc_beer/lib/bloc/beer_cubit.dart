import 'package:am040_bloc_beer/beer_repository.dart';
import 'package:bloc/bloc.dart';

import 'beer_state.dart';

class BeerCubit extends Cubit<BeerState> {
  BeerCubit({this.repository}) : super(BeerInitialState());

  final BeerRepository repository;
  int page = 1;
  bool isFetching = true;

  void getBeers() async {
    emit(BeerLoadingState(message: "... start downlaod ..."));
    final data = await repository.getResponseBeers(page: page);
    if (data is String) {
      emit(BeerErrorState(error: data));
    } else {
      page++;
      emit(BeerSuccessState(beers: data));
    }
  }
}

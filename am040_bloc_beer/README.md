# am040_bloc_beer

L'esempio qui proposto è tratto da un tutorial, [qui](https://dev.to/piedcipher/paginate-api-results-with-bloc-in-flutter-3j1o) per i dettagli; in tale sede viene usato il BLOC secondo il vecchio stile, qui usiamo invece il `Cubit`. Il confronto fra questa e l'altra soluzione ci permette di apprezzarne differenze, vantaggi e svantaggi.

## api

Facendo uso del solito pacchetto `http` [qui](https://pub.dev/packages/http/install) isiamo le [PUNK API](https://punkapi.com/documentation/v2). Possiamo sperimentare **tutte le birre**
```
curl https://api.punkapi.com/v2/beers 
```
**pagina**
```
curl https://api.punkapi.com/v2/beers?page=2&per_page=80 
```
e **singola birra**
```
curl https://api.punkapi.com/v2/beers/1 
```

## generated model

Il pacchetto `build_runner`, [qui](https://pub.dev/packages/build_runner/install), ci permette di automatizzare alcune procedure
```
flutter pub run build_runner build
```
`json_annotation` [qui](https://pub.dev/packages/json_annotation) e `json_serializable` [qui](https://pub.dev/packages/json_serializable/install),

Utile è anche il tutorial ufficiale [qui](https://flutter.dev/docs/development/data-and-backend/json).

``` dart
part 'beer_model.g.dart';

@JsonSerializable()
class BeerModel {
  int id;
  String name;
  String tagline;
  String description;

  @JsonKey(name: 'image_url')
  String imageUrl;

  BeerModel({
    @required this.id,
    @required this.name,
    @required this.tagline,
    @required this.description,
    @required this.imageUrl,
  });

  factory BeerModel.fromJson(Map<String, dynamic> json) =>
      _$BeerModelFromJson(json);
}
```

## il provider singleton

La connessione con la sorgente dei dati, il server REST, viene rappresentata mediante un *singleton*
``` dart
class BeerRepository {
  static final BeerRepository _beerRepository = BeerRepository._();
  static const int _perPage = 10;

  BeerRepository._();

  factory BeerRepository() {
    return _beerRepository;
  }

  Future<dynamic> getResponseBeers({@required int page}) async {
    try {
      final url =
          'https://api.punkapi.com/v2/beers?page=$page&per_page=$_perPage';
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final beers = jsonDecode(response.body) as List;
        return beers.map((beer) => BeerModel.fromJson(beer)).toList();
      } else {
        return "status code 200";
      }
    } catch (e) {
      return "error:" + e.toString();
    }
  }
}
```
grazie ad essa abbiamo una gestione efficace di **dati** ed **errori**.

## state

Come da tradizione, ache seguendo l'uso del BLOC alla vecchia maniera, definiamo una classe per lo **stato**
``` dart
abstract class BeerState {
  const BeerState();
}

class BeerInitialState extends BeerState {
  const BeerInitialState();
}

class BeerLoadingState extends BeerState {
  final String message;

  const BeerLoadingState({
    @required this.message,
  });
}

class BeerSuccessState extends BeerState {
  final List<BeerModel> beers;

  const BeerSuccessState({
    @required this.beers,
  });
}

class BeerErrorState extends BeerState {
  final String error;

  const BeerErrorState({
    @required this.error,
  });
}
```
lavorando con una classe `abstract` si ha la possibilità di dettagliare lo stato efficacemente: nel nostro caso abbiamo 4 possibilità che vanno a definire la **macchina a stati finiti** del *business logic**.

## cubit

Il motore interno del *business logic* è il `Cubit`
``` dart
class BeerCubit extends Cubit<BeerState> {
  BeerCubit({this.repository}) : super(BeerInitialState());

  final BeerRepository repository;
  int page = 1;
  bool isFetching = true;

  void getBeers() async {
    emit(BeerLoadingState(message: "... start downlaod ..."));
    final data = await repository.getResponseBeers(page: page);
    if (data is String) {
      emit(BeerErrorState(error: data));
    } else {
      page++;
      emit(BeerSuccessState(beers: data));
    }
  }
}
```
nel `Cubit` usaimo `isFetching` per gestire lo scrolling* della lista
``` dart
return ListView.separated(
  controller: _scrollController
    ..addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent &&
          !context.read<BeerCubit>().isFetching) { // <===========
            context.read<BeerCubit>()
            ..getBeers()
            ..isFetching = true;
            Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("....more ...")));
      }
  }),
...
);
```

## view

Qui la faccenda è articolata, ma il motore già è funzionante.

`DisplayBeerScreen` è la schermata principale sistemata nell'impalcatura del *material design* al solito modo; a tale livello posioniamo il 
``` dart
BlocProvider(
  create: (context) => BeerCubit(
    repository: BeerRepository(),
  ),
  child: Scaffold(...)
)
```

`BeersView` è una `StatelessWidget`, come d'uso nel BLOC visto che nei pacchetti che usiamo è nascosto un `InheritedWidget`, contiene il `BlocBuilder` che ascolta le transizioni di stato
``` dart
BlocBuilder<BeerCubit, BeerState>(
  builder: (context, state) {
    if (state is BeerInitialState) {
      context.read<BeerCubit>().getBeers();
      return CircularProgressIndicator();
    } else if (state is BeerLoadingState && _beers.isEmpty) {
      context.read<BeerCubit>().isFetching = true;
      return CircularProgressIndicator();
    } else if (state is BeerSuccessState) {
      context.read<BeerCubit>().isFetching = false;
      _beers.addAll(state.beers);
    } else if (state is BeerErrorState && _beers.isEmpty) {
      return Column( ... ); // <>=== qui disegnamo la lista
```
in essa trpviamo le due *property* `_beers` che è la solita `List` di `BeerModel` e `_ScrollController`
``` dart
class BeersView extends StatelessWidget {
  final List<BeerModel> _beers = [];
  final ScrollController _scrollController = ScrollController();
```

Riproponendo [qui](https://api.flutter.dev/flutter/widgets/ListView-class.html) le API di `ListView`, noi usiamo il *named constructor* `ListView.separated`, [qui](https://api.flutter.dev/flutter/widgets/ListView/ListView.separated.html) per il dettaglio.
> Creates a fixed-length scrollable linear array of list "items" separated by list item "separators".
``` dart
return ListView.separated(
  controller: _scrollController
    ..addListener(() { ... }),
  itemBuilder: (context, index) => BeerListItem(_beers[index]),
  separatorBuilder: (context, index) => const SizedBox(height: 20),
  itemCount: _beers.length,
);
```
`itemBuilder` è la solita singola *tile* della lista, essa però viene separata nei suoi elementi da un `separatorBuilder` in un `SizedBox(...)`
[qui](https://api.flutter.dev/flutter/widgets/SizedBox-class.html) per le API
> The separatorBuilder callback will be called with indices greater than or equal to zero and less than itemCount - 1.

qui sotto l'esempio delle API
``` dart
ListView.separated(
  itemCount: 25,
  separatorBuilder: (BuildContext context, int index) => Divider(),
  itemBuilder: (BuildContext context, int index) {
    return ListTile(
      title: Text('item $index'),
    );
  },
)
```

Per ogni singola birra dedichiamo una *tile* della lista
``` dart
class BeerListItem extends StatelessWidget {

  final BeerModel beer;
  const BeerListItem(this.beer);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(beer.name),
      subtitle: Text(beer.tagline),
      childrenPadding: const EdgeInsets.all(16),
      leading: Container(
        margin: EdgeInsets.only(top: 8),
        child: Text(beer.id.toString()),
      ),
      children: [
        Text(
          beer.description,
          textAlign: TextAlign.justify,
        ),
        const SizedBox(height: 20),
        beer?.imageUrl == null // <== sintassi ganza!
            ? Container()
            : Image.network(
                beer.imageUrl,
                loadingBuilder: (context, widget, imageChunkEvent) {
                  return imageChunkEvent == null
                      ? widget
                      : CircularProgressIndicator();
                },
                height: 300,
              ),
      ],
    );
  }
}
```
per `ExpansionTile` [qui](https://api.flutter.dev/flutter/material/ExpansionTile-class.html).


## altri materiali

[1] La pagina di `Bloc` [qui](https://bloclibrary.dev/#/fluttercountertutorial).  
[2] "Flutter: Adding Separator in ListView" di Bhavik Makwana du Medium [qui](https://medium.com/flutter-community/flutter-adding-separator-in-listview-c501fe568c76) un articolo su Medium che può chiarire.

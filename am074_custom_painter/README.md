# am074_custom_painter

- ver: 250116

Un primo esempio di `CustomPainetr`.

## alcuni dettagli sulla classe.

Ci soffermiamo sul metodo `shouldRepaint`. È pur vero che Flutter ottimizza e gestisce
bene in autonomia l'interfaccia, maè bene dare indicazioni nel caso in cui, ad esempio data una nuova istanza, si debba procedere a ridisegnare. Avremmo potuto ad esempio scrivere
```dart
@override
bool shouldRepaint(covariant Heart oldDelegate) => this.size != oldDelegate.size;
```
Data la `StatefulWidget` Flutter provvederebbe a ridisegnare comunque anche in presenza di un ritorno identicamente `false`.

## riferimenti

[1] API per `CustomPainter`
[2] "Paths in Flutter: A Visual Guide" da Medium [qui](https://medium.com/flutter-community/paths-in-flutter-a-visual-guide-6c906464dcd0).

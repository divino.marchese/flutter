# am003_passing_arguments

**[231216]** Analogamente all'applicazione precedente, ma usando i *named route* in stile *web*; in questa applicazione vedremo inoltre come gestire il passaggio di informazione fra uno screen ed un altro: usiamo uno stile Android il quale prevede un *bundle* ovvero un dizionario, `Map` in Dart.

## come migliorare ...

Abbiamo creato un `bundle` come variabile *global*, un po' come faceva Android. Si può procedere, forse più elegantemente, lavorando con una `StatefulWIdget`, per questo rimandiamo al prossimo esercizio.

## riferimenti

[1] "Navigate with named routes" di Google **[guida](https://flutter.dev/docs/cookbook/navigation/named-routes)**.
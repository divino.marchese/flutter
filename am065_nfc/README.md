# am065_nfc

## gradle

```
defaultConfig {
    ...
    minSdkVersion 19
    targetSdkVersion flutter.targetSdkVersion
    versionCode flutterVersionCode.toInteger()
    versionName flutterVersionName
    }
```

## pubspec

```
dependencies:
  flutter:
    sdk: flutter
  nfc_manager: ^3.2.0
```

## scopo dell'applicazione

Questa App è un test. Scopo è lo'indaginme del servizio di connessione e valutare, a seconda dei *record* il recupero dell'informazione.

## materiali

[1] "NFC basics" dalla documentazione ufficiale di Android [ qui](https://developer.android.com/guide/topics/connectivity/nfc/nfc).  
[2] "Introducing NDEF" O'Reilly [qui](https://www.oreilly.com/library/view/beginning-nfc/9781449324094/ch04.html).

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am065 nfc',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am065 nfc'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _nfcAvaidable = false;
  NfcTag? _tag;
  bool _wroten = false;
  String _msg = "";
  String _data = "";

  Future<void> _readTag() async {
    NfcManager.instance.startSession(
      onDiscovered: (NfcTag tag) async {
        setState(() {
          _tag = tag;
        });
        Ndef? ndef = Ndef.from(tag);
        if (ndef == null) {
          setState(() {
            _msg = "NDEF is null";
          });
          NfcManager.instance.stopSession();
          return;
        }
        if (_wroten == false) {
          setState(() {
            _msg = "TAG empty";
          });
          NfcManager.instance.stopSession();
          return;
        }
        String data = "";
        NdefMessage ndefMessage = await ndef.read();
        for (var record in ndefMessage.records) {
          Uint8List payload = record.payload;
          debugPrint("*** RECORDS ***");
          debugPrint(record.typeNameFormat.toString());
          debugPrint(record.type[0].toRadixString(16));
          debugPrint(String.fromCharCodes(payload));
          switch (record.typeNameFormat) {
            case NdefTypeNameFormat.media:
              data += "media: ${String.fromCharCodes(payload)}";
              break;
            case NdefTypeNameFormat.nfcExternal:
              data += "ext: ${String.fromCharCodes(payload)}";
              break;
            case NdefTypeNameFormat.nfcWellknown:
              if (record.type[0] == 0x54) {
                String lang =
                    String.fromCharCodes(payload.sublist(1, payload[0] + 1));
                String text =
                    String.fromCharCodes(payload.sublist(payload[0] + 1));
                data += "($lang): $text";
              } else if (record.type[0] == 0x55) {
                data += "uri: ${String.fromCharCodes(payload.sublist(1))}";
              }
              break;
            default:
              data += "<default>";
              break;
          }
          // data += String.fromCharCodes(record.payload);
          data += "\n";
        }
        setState(() {
          _data = data;
          _msg = "NFC messages";
        });
        NfcManager.instance.stopSession();
      },
    );
  }

  Future<void> _writeTag(String text) async {
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      setState(() {
        _tag = tag;
      });
      var ndef = Ndef.from(tag);
      if (ndef == null || !ndef.isWritable) {
        setState(() {
          _msg = 'Tag is not ndef writable';
        });
        return;
      }
      NdefMessage message = NdefMessage([
        NdefRecord.createText(text),
        NdefRecord.createUri(Uri.parse('https://flutter.dev')),
        NdefRecord.createMime(
            'text/plain', Uint8List.fromList('Hello'.codeUnits)),
        NdefRecord.createExternal(
            'com.example', 'mytype', Uint8List.fromList('mydata'.codeUnits)),
      ]);
      try {
        await ndef.write(message);
        setState(() {
          _wroten = true;
          _msg = 'TAG Ndef wroten';
        });
        NfcManager.instance.stopSession();
      } catch (e) {
        setState(() {
          _msg = '<error>';
        });
        return;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    NfcManager.instance.isAvailable().then((value) {
      setState(() {
        _nfcAvaidable = true;
      });
    });
    _readTag();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: _tag == null
              ? const Text("scan TAG")
              : GestureDetector(
                  onTap: _readTag,
                  child: SizedBox(
                    width: 340,
                    child: Card(
                        elevation: 3,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text("msg: $_msg",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall),
                              const Divider(
                                color: Colors.black,
                                indent: 20,
                                endIndent: 20,
                                thickness: 2,
                              ),
                              Text(_data),
                            ])),
                  ))),
      floatingActionButton: FloatingActionButton(
        onPressed: _nfcAvaidable
            ? () {
                _writeTag("Hello World");
              }
            : null,
        tooltip: 'Increment',
        child: const Icon(Icons.nfc),
      ),
    );
  }
}

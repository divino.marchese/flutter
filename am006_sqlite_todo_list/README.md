# am0006_sqlite_todo_list

Questo è il primo esempio
Il pacchetto `sqflite` ha la sua pagina [qui](https://pub.dev/packages/sqflite).  
Per il file `pubspec.yaml` un'introduzione [qui](https://dart.dev/tools/pub/pubspec).  
Per le versioni e le dipendenze "Package Dependecies" [qui](https://dart.dev/tools/pub/dependencies#version-constraints). Ad esempio
```
# slquite
sqflite: ^2.0.0+4
```
è analogo a `sqflite: ">=2.0.0+4 <3.0.0"`. Abbimao anche la possibilità
```
sqflite: any
```
Come al solito per aggiornare o scaricare, da terminale
```
flutter pub get
```
chiamato automaticamente salvando in VSC `pubspec.yaml`.

## Data Access Object

La classe `DAO`, una connessione, è un *singleton*.

## model

`Comparable` [qui](https://api.flutter.dev/flutter/dart-core/Comparable-class.html) per le api permete l'*overriding*, la re-scrittura, degli operatori di confronto. Oltre alla definizione di una **connessione** qui abbiamo una serie di metodi che "mascherano" le *query* come accade negli [ORM](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping): gli oggetti vengono mappati in modo persistente nel db; per i dettagli sulle *query* [qui](https://github.com/tekartik/sqflite/blob/master/sqflite/doc/sql.md), ogni *query* tipo select ritorna una lista di `Map`, vedi API del pacchetto, mediante
```dart
return data.map((d) => TodoItem.fromMap(d)).toList();
```
abbiamo la conversione a `List` di oggetti.

## Rimandiamo alle API dei `Widget` per uno studio diligente e paziente!


## altri riferimenti

[1] "Persist data with SQLite" tutorial della doc Google [qui](https://flutter.dev/docs/cookbook/persistence/sqlite).  

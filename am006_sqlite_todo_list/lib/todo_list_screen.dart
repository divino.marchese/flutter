import 'package:flutter/material.dart';

import 'dart:async';

import 'add_todo_item_screen.dart';
import 'dao.dart';
import 'model.dart';

class TodoListScreen extends StatefulWidget {
  final String title = "to do List";

  const TodoListScreen({Key? key}) : super(key: key);

  @override
  _TodoListScreenState createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen> {
  List<TodoItem> _todoItems = [];
  late DAO _dao;

  _TodoListScreenState() {
    _dao = DAO();
  }

  @override
  initState() {
    super.initState();

    _dao.open().then((result) {
      // then trick, awit forbidden
      _dao.getTodoItems().then((result) {
        setState(() {
          _todoItems = result;
        });
      });
    });
  }

  void _addTodoItem() async {
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) => const AddTodoItemScreen()));
    _dao.getTodoItems().then((result) {
      setState(() {
        _todoItems = result;
      });
    });
  }

  Widget _createTodoItemWidget(TodoItem item) {
    return ListTile(
      title: Text(item.name),
      contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
      trailing: Checkbox(
        value: item.isComplete,
        onChanged: (value) => _updateTodoCompleteStatus(item, value!),
      ),
      onLongPress: () => _displayDeleteConfirmationDialog(item),
    );
  }

  void _updateTodoCompleteStatus(TodoItem item, bool newStatus) {
    item.isComplete = newStatus;
    _dao.updateTodo(item);
    _dao.getTodoItems().then((items) {
      setState(() {
        _todoItems = items;
      });
    });
  }

  void _deleteTodoItem(TodoItem item) {
    _dao.deleteTodo(item);
    _dao.getTodoItems().then((items) {
      setState(() {
        _todoItems = items;
      });
    });
  }

  Future<void> _displayDeleteConfirmationDialog(TodoItem item) {
    return showDialog(
        context: context,
        barrierDismissible: true, // Allow dismiss when tapping away from dialog
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Delete TODO"),
            content: Text("Do you want to delete \"${item.name}\"?"),
            actions: <Widget>[
              TextButton(
                child: const Text("Cancel"),
                onPressed: Navigator.of(context).pop, // Close dialog
              ),
              TextButton(
                child: const Text("Delete"),
                onPressed: () {
                  _deleteTodoItem(item);
                  Navigator.of(context).pop(); // Close dialog
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    _todoItems.sort();
    // list = iterable -> iterable (via map) -> list
    final todoItemWidgets = _todoItems.map(_createTodoItemWidget).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: todoItemWidgets,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addTodoItem,
        tooltip: 'Add Todo',
        child: const Icon(Icons.add),
      ),
    );
  }
}

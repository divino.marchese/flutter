class TodoItem extends Comparable {
  final int id;
  final String name;
  bool isComplete;

  TodoItem({this.id = 0, this.name = "", this.isComplete = false});

  // named constructor
  TodoItem.fromMap(Map<String, dynamic> map)
      : id = map["id"],
        name = map["name"],
        isComplete = map["isComplete"] == 1; // SQLite has not bool

  @override
  int compareTo(other) {
    if (isComplete && !other.isComplete) {
      return 1;
    } else if (!isComplete && other.isComplete) {
      return -1;
    } else {
      return id.compareTo(other.id);
    }
  }

  Map<String, dynamic> toMap({bool update = false}) {
    if (update) {
      return <String, dynamic>{
        "id": id,
        "name": name,
        "isComplete": isComplete ? 1 : 0
      };
    }
    return <String, dynamic>{"name": name, "isComplete": isComplete ? 1 : 0};
  }
}

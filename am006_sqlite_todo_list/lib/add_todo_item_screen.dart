import 'package:flutter/material.dart';

import 'dao.dart';
import 'model.dart';

class AddTodoItemScreen extends StatefulWidget {
  const AddTodoItemScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddTodoItemScreenState();
}

class _AddTodoItemScreenState extends State<AddTodoItemScreen> {
  final _todoNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Add Todo Item")),
        body: Container(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(labelText: "Todo Name"),
                  controller: _todoNameController,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    TextButton(
                      child: const Text("Cancel"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    TextButton(
                      child: const Text("Save"),
                      onPressed: () {
                        DAO().insertTodo(
                            TodoItem(name: _todoNameController.text));
                        Navigator.pop(context);
                      },
                    )
                  ],
                )
              ],
            )));
  }

  @override
  void dispose() {
    _todoNameController.dispose();
    super.dispose();
  }
}

# am034_isolate

Ricordando di aggiungere sul manifest di Android (solo *relase mode*).
```
<uses-permission android:name="android.permission.INTERNET" />
```
la parte centrale del lavoro, come da prassi, viene svolta qui
``` dart
Future loadIsolate() async {
    ReceivePort receivePort = ReceivePort();

    await Isolate.spawn(isolateEntry, receivePort.sendPort);

    SendPort sendPort = await receivePort.first;

    List message = await sendRecieve(
        sendPort, "https://jsonplaceholder.typicode.com/comments");

    setState(() => list = message);
  }
```
`isolateEntry` è una *function* che rappresenta la computazione operata nel *task* rappresentato dall *Isolate*, [qui](https://api.dart.dev/stable/2.7.2/dart-isolate/Isolate/spawn.html) leggiamo
> The argument ... specifies the initial function to call in the spawned isolate. The ... function is invoked in the new isolate with message as the only argument.

Il protocollo è sempre quello:
- per prima cosa inviare la `SendPort` dell'isolate,
- poi operare ed inviare il messaggio, la porta che riceverà sul main, qui una `SendPort` viene ottenuta come seconda parte della `List` `msf`.
``` dart
static isolateEntry(SendPort sendPort) async {
    ReceivePort port = ReceivePort();

    sendPort.send(port.sendPort);

    await for (var msg in port) {
      String data = msg[0];
      SendPort replyPort = msg[1];

      String url = data;

      http.Response response = await http.get(url);

      replyPort.send(json.decode(response.body));
    }
  }
```
nel metodo 
``` dart
Future sendRecieve(SendPort send, message) {
    ReceivePort responsePort = ReceivePort();

    send.send([message, responsePort.sendPort]);
    return responsePort.first;
  }
```
vediamo il duplice significato di `msg` di `isolateEntry`.
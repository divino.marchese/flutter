import 'package:flutter/material.dart';

void main() {
  runApp(
    const Center(
      child: Text(
        'Hello world!',
        style: TextStyle(
          fontSize: 50.0,
          color: Colors.yellow,
        ),
        textDirection: TextDirection.ltr,
      ),
    ),
  );
}

# am001 hello world

- ver:250112

Quasi un esempio minimale! Una volta introdota le *ap* in *material design*,
ad esempio, leggendo dalla documentazione di `TextDirection` non risulta più necessario
imporre esplicitamente la direzione nel costruttore di `Text`:
```dart
Text(
  'Hello world!',
  style: TextStyle(...),
  textDirection: TextDirection.ltr,
),
```
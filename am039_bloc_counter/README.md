# am039_bloc_counter

Un altro esempio di stato centralizzato.

## `Cubit`

Se il `Bloc`, che da nome al pacchette, è un mix fra *stream* di eventi e macchina a stati finiti, il `Cubit` è una semplice macchina a stati finiti ove lo stato viene **emesso**, ovvero quando cambiamo stato coem nel caso di una `StatefulWidget` emettiamo l'evento in modo da, possibilmente, portarla ad altri `Widget`.

## `BlockProvider`

È alla radice dell'albero dei `Widget` che si intende notificare e si occupa di istanziare il `Cubit`: questo avviene una volta che il metodo `build` ha fatto il suo lavoro, in sostanza è l'*observer*, [api](https://pub.dev/documentation/flutter_bloc/latest/flutter_bloc/BlocProvider-class.html).

## `BlockBuilder`

Permette di ricevere le notifiche del `Cubit` e di esporre la proprietà `state`
> `BlocBuilder` handles building a widget in response to new states

[api](https://pub.dev/documentation/flutter_bloc/latest/flutter_bloc/BlocBuilder-class.html).  

## considerazuioni finali

Come visto col `Provider` il `BuildContext` viene integrato coi due metodi `read` e `watch`.

## materiali

[1] `Bloc` *package* [qiui](https://pub.dev/packages/flutter_bloc).  
[2] `Bloc` API [qui](https://pub.dev/documentation/flutter_bloc/latest/).  

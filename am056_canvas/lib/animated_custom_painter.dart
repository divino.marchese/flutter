import 'package:flutter/material.dart';

class AnimatedCustomPainter extends CustomPainter {
  final _paint = Paint();
  final Animation<double> _sizeTween;
  final Animation<double> _offsetTween;
  final Animation<Color?> _colorTween;

  AnimatedCustomPainter(Animation<double> animation)
      : _sizeTween = Tween<double>(begin: 50, end: 150).animate(animation),
        _offsetTween = Tween<double>(begin: 200, end: 0).animate(animation),
        _colorTween = ColorTween(begin: Colors.red, end: Colors.yellow)
            .animate(animation),
        super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    _paint.color = _colorTween.value ?? Colors.white;
    canvas.drawCircle(
      Offset(
        size.width / 2,
        size.height / 2 + _offsetTween.value,
      ),
      _sizeTween.value,
      _paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

import 'package:flutter/material.dart';

import 'animated_custom_painter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am056 canvas',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am056 canvas'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  bool forward = true;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  _go() {
    if (forward) {
      controller.forward();
    } else {
      controller.reverse();
    }
    setState(() {
      forward = !forward;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: CustomPaint(
            painter: AnimatedCustomPainter(controller),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _go(),
          tooltip: 'animate',
          child: const Icon(
            Icons.replay,
          ),
        ));
  }
}

# am056_canvas

Ultimo esempio di animazione tratto liberamente da quesdto **[tutorial](https://andreygordeev.com/2020/05/11/flutter-custompainter-animation/)**. Seguendo la falsariga dei precedenti esempi abbiamo un nuovo oggetto.
`CustomPainter` **[API](https://api.flutter.dev/flutter/rendering/CustomPainter-class.html)** è un `Listenable`, attende eventi per poter disegnare qualcosa.
- `paint(Canvas canvas SIze size)` disegna su un `canvas`, origine degli assi in alto a sinistra come s'usa in grafica, le cui dimensioni sono nel 2D `size`.
- La classe `Paint` **[API](https://api.flutter.dev/flutter/dart-ui/Paint-class.html)** permette di definire lo stile dell'oggetto disegnato su di un `Canvas`, nel nostro caso abbiamo dato 
```
_paint.color = _colorTween.value ?? Colors.white;
```
- `Canvas` **[API](https://api.flutter.dev/flutter/dart-ui/Canvas-class.html)** ci permette, impostato un `Paint` di disegnare
```
void drawCircle(

    Offset c,
    double radius,
    Paint paint

) 
```
indicando con `Offset` le coordinate del centro.
- `shouldRepaint(...)` nel nostro caso ritorna `false`, se vi fosse una proprietà cge cambia useremo 
``bool shouldRepaint(CustomPainter oldDelegate) => oldeleGate.prop != prop;`.

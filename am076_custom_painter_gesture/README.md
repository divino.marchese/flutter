# am076_custom_painter_gesture

- ver: 250111

In questo esempio abbiamo introdotto in modo sommario la rilevazione dei *gesture*.
Rimandiamo alla documentazione delle classe `GestureDetector`.

## materiali

[1] "Taps, drags, and other gestures" [qui](https://docs.flutter.dev/ui/interactivity/gestures).  
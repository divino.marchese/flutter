import 'dart:math';

import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  List<int> weigths = [];
  double _angle = 0;

  MyPainter(this.weigths);

  MyPainter.rotate({required this.weigths, required double angle}) {
    _angle = angle / 50;
  }

  void _drawSector(
      {required Canvas canvas,
      required Paint paint,
      required Offset center,
      required double radius,
      required double startAngle,
      required double endAngle}) {
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), startAngle,
        endAngle - startAngle, true, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    List<double> norms = _normalize(weigths);
    final int cPLength = Colors.primaries.length;
    int c = 12;
    double start = _angle;
    double stop = _angle;
    Paint p = Paint();
    for (int i = 0; i < weigths.length; i++) {
      p.color = Colors.primaries[c];
      stop += 2 * pi * norms[i];
      _drawSector(
          canvas: canvas,
          paint: p,
          center: Offset(size.width / 2, size.height / 2),
          radius: size.width / 2,
          startAngle: start,
          endAngle: stop);
      start = stop;
      c = (c + 5) % cPLength;
    }
  }

  @override
  bool shouldRepaint(MyPainter oldDelegate) => _angle != oldDelegate._angle;
}

List<double> _normalize(List<int> list) {
  List<double> ret = <double>[];
  int sum = list.reduce((v, e) => v + e);
  for (var n in list) {
    ret.add(n.toDouble() / sum.toDouble());
  }
  return ret;
}

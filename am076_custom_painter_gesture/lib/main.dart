import 'package:am076_custom_painter_gesture/custom_painter.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'custom painter gesture'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<int> weigths = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  double _x = 0;
  double _y = 0;
  double _angle = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.surface,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('gira la ruota ...',
                style: Theme.of(context).textTheme.headlineSmall),
            GestureDetector(
                onHorizontalDragStart: (detail) {
                  _x = detail.globalPosition.dx;
                },
                onVerticalDragStart: (detail) {
                  _y = detail.globalPosition.dy;
                },
                onHorizontalDragUpdate: (detail) {
                  setState(() {
                    _angle -= detail.globalPosition.dx - _x;
                    _x = detail.globalPosition.dx;
                  });
                },
                onVerticalDragUpdate: (detail) {
                  setState(() {
                    _angle += detail.globalPosition.dy - _y;
                    _y = detail.globalPosition.dy;
                  });
                },
                child: CustomPaint(
                    size: Size(400, 600),
                    painter: MyPainter.rotate(weigths: weigths, angle: _angle)))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'round',
        child: const Icon(Icons.play_arrow),
      ),
    );
  }
}

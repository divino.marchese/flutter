import 'dart:math';

import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  List<int> weigths = [];

  MyPainter(this.weigths);

  void _drawSector(
      {required Canvas canvas,
      required Paint paint,
      required Offset center,
      required double radius,
      required double startAngle,
      required double endAngle}) {
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), startAngle,
        endAngle - startAngle, true, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    List<double> norms = _normalize(weigths);
    Random r = Random();
    final int cPLength = Colors.primaries.length;
    int c = r.nextInt(cPLength);
    double start = 0;
    double stop = 0;
    Paint p = Paint();
    for (int i = 0; i < weigths.length; i++) {
      p.color = Colors.primaries[c];
      stop += 2 * pi * norms[i];
      _drawSector(
          canvas: canvas,
          paint: p,
          center: Offset(size.width / 2, size.height / 2),
          radius: size.width / 2,
          startAngle: start,
          endAngle: stop);
      start = stop;
      c = (c + 5) % cPLength;
    }
  }

  @override
  bool shouldRepaint(MyPainter oldDelegate) => true;
}

List<double> _normalize(List<int> list) {
  List<double> ret = <double>[];
  int sum = list.reduce((v, e) => v + e);
  for (var n in list) {
    ret.add(n.toDouble() / sum.toDouble());
  }
  return ret;
}

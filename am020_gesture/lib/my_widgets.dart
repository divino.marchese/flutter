import 'package:flutter/material.dart';

class MyGestureDetector extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // TAP
      onTap: () {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("\"onTap\" in GestureDetector"),
        ));
      },
      // note: padding does not contain GestureDetector
      child: Padding(
        padding: EdgeInsets.all(50.0),
        child: Container(
          height: 50.0,
          width: 100.0,
          decoration: BoxDecoration(
            color: Colors.orange,
            borderRadius: BorderRadius.circular(17.0),
          ),
          child: Center(child: Text('Click me')),
        )
      ) 
    );
  }
}

class MyInkWell extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Padding (
      padding: EdgeInsets.all(50.0),
      child: InkWell (
        // TAP
        onTap: () {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: new Text("\"onTap\" in Inkwell"),
          ));
        },
        // DOUBLE TAP
        onDoubleTap: () {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: new Text("\"onDoubleTap\" in Inkwell"),
          ));
        },
        onLongPress: () {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: new Text("\"onLongPress\" in Inkwell"),
          ));
        },
        child: Container(
          height: 50.0,
          width: 100.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(17.0),
          ),
          child: Center(child: Text('Click me')),
        )
      )
    );
  }
}


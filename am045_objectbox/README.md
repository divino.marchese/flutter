# am045_objectbox

**ObjectBox** è un db NoSQL (ad oggetti), per la
preparazione, vedi [1]
```
flutter pub add objectbox objectbox_flutter_libs:any
flutter pub add --dev build_runner objectbox_generator:any
```
Oppure modificare direttamente `pubspec.yaml`

## preparaqzione di Android

In `settings.gradle` 
```
plugins {
    id "dev.flutter.flutter-plugin-loader" version "1.0.0"
    id "com.android.application" version "8.2.1" apply false
    id "org.jetbrains.kotlin.android" version "1.8.22" apply false
}
```
In caso di dimenticanza ci viene dato avviso di come correggere al fallimento della compilazionme.

## entity

Vedi [2]
```
@Entity()
class User {
  @Id()
  int id = 0;
  String? name;
}
```
`id` è un identificativo dell'oggetto (il nome `id` lo identifica senza ambiguità). Per iniziare a leggere [5].

## generazione del codice

Una volta definite le *entity* e le *associations* possiamo usare
```
flutter pub run build_runner build
```
La classe `ObjectBox` avrà quindi tutti i requisiti.

## creazione dello store e della classe `ObjectBox`

`Store` rappresenta un database, in sostanza è quello che altrove chiamiamo *connection*,
un ***singleton** con metodo get asincrono (usaimo la proposta della documentazione).
Un database è composto da oggetti `Box`, vedi [4], che rappresentano le collezioni degli oggetti. Il metodo asincrono `openStore()` ci viene dato dal file generato. Per il resto abbiamo seguito l'esempio in [1]. La classe `ObjectBox` è anche un *data access object**,
qui ci siamo limitati ad aggiunta e cancellazione completa.

## relation

Qui non sono presenti, ma invitiamo a leggere con attenzione la documentazione [6].

## alcuni dettagli sui widget usati

`Expanded` ci permette di gestire una lista che debordasse dai naturali limiti del suo contenitore, essa occupa lo spazio disponibile.
vedi [api](https://api.flutter.dev/flutter/widgets/Expanded-class.html). `Listview` in particolare il costruttore `builder` merita attenzione [api](https://api.flutter.dev/flutter/widgets/ListView/ListView.builder.html).



## materiali

[1] "Getting Started" [qui](https://docs.objectbox.io/getting-started).  
[2] "ObjectBox Model" [qui](https://docs.objectbox.io/advanced/meta-model-ids-and-uids).  
[3] "Documentation" [qui](https://pub.dev/documentation/objectbox/latest/index.html).  
[4] `Box` [api](https://pub.dev/documentation/objectbox/latest/objectbox/Box-class.html).  
[5] "Enity annotation" [qui](https://docs.objectbox.io/entity-annotations#object-ids-id).  
[5] "Realations" [qui](https://docs.objectbox.io/relations).   
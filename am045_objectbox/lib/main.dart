import 'package:flutter/material.dart';

import 'model/user.dart';
import 'objectbox.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am045_objectbox',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(
        title: 'am045 objectbox',
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late ObjectBox _objectBox;

  final _users = <User>[];

  final TextEditingController _usersController = TextEditingController();

  void _addUser(String name) {
    User user = User(name: name);
    _objectBox.insertUser(name: name).then((id) => {user.id = id});
    setState(() {
      _users.add(user);
    });
  }

  void _deleteAllUsers() {
    _objectBox.removeAllUsers();
    setState(() {
      _users.removeRange(0, _users.length);
    });
  }

  @override
  void initState() {
    super.initState();
    ObjectBox.create().then((ob) {
      _objectBox = ob;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.primary,
          foregroundColor: Theme.of(context).colorScheme.surface,
          title: Text(widget.title),
        ),
        body: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: TextField(
              controller: _usersController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'user name',
              ),
            ),
          ),
          ElevatedButton(
            child: const Text('add user'),
            onPressed: () {
              _addUser(_usersController.text);
            },
          ),
          Expanded(
              child: ListView.builder(
                  padding: const EdgeInsets.all(4),
                  itemCount: _users.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.red,
                          child: Text(index.toString()),
                        ),
                        title: Text("name"),
                        subtitle: Text(_users[index].name ?? ''));
                  })),
          ElevatedButton(
            child: const Text('delete all'),
            onPressed: () {
              _deleteAllUsers();
            },
          ),
        ]));
  }
}

import 'model/user.dart';
import 'objectbox.g.dart';

class ObjectBox {
  late final Store _store;
  late final Box<User> _userBox;

  ObjectBox._create(this._store) {
    _userBox = Box<User>(_store);
  }

  static Future<ObjectBox> create() async {
    final store = await openStore();
    return ObjectBox._create(store);
  }

  Future<int> insertUser({required String name}) {
    User user = User(name: name);
    return _userBox.putAsync(user);
  }

  Future<List<User>> getAllUsers() {
    return _userBox.getAllAsync();
  }

  Future<User?> findUserById(int id) {
    return _userBox.getAsync(id);
  }

  Future<bool> removeUserById(int id) {
    return _userBox.removeAsync(id);
  }

  Future<int> removeAllUsers() {
    return _userBox.removeAllAsync();
  }
}

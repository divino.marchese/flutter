# am037_inherited_counter

Questa è un *remake* della classica app basic di Flutter, la novità sta in `Counter` una `InheritedWidget`, pensata per centralizzare sull'albero dei *widget* una qualche informazione. Si noti il metodo 
```dart
@override
bool updateShouldNotify(Counter oldWidget) => value != oldWidget.value;
```
e si provi a portarlo a `false`! Quando il *widget* viene ricreato, vedi [1] , `true`decide di inoltrare, per via gerarchica (verso il basso), la segnalazione del cambiamento in modo da ricreare eventuali *widget* discendenti
> When this widget is rebuilt, sometimes we need to rebuild the widgets that inherit from this widget but sometimes we do not. For example, if the data held by this widget is the same as the data held by `oldWidget`, then we do not need to rebuild the widgets that inherited the data held by `oldWidget`. A riprova di quanto affermato si provi il metodo alternativo che comunque ritorna `false`! Cambiando lo stato della `StatefulWidget` l'istanza di `Counter` viene ricreata e quindi innescata la chiamata del metodo in questione. 

**NB** Quando un `Widget` è `const` Flutter non lo coinvolge in un eventuale *rebuild*, cosa che accade nella `StatefulWidget` ogni qual volta,
ad esempio `setState()` viene all'opera! `CounterText`, se non fosse `const`, parteciperebbe al *rebuild* ad ogni `setState()`, ma se invece è `const`
entra in gioco `updateShouldNotify(...)`.

## nota finale

Il tutto può apparire un po' tortuoso ma la cosa è voluta
- click
- cambia lo stato
- `Counter` viene ricreata, nel senso di *rebuild*
- al momento della ricreazione notifica `CounterText` per un (eventuale) *rebuild*

## materiali

[1] Metodo `updateShouldNotify()` per `InheritedWidget` [api](https://api.flutter.dev/flutter/widgets/InheritedWidget/updateShouldNotify.html).

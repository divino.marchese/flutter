import 'package:flutter/material.dart';

import 'counter.dart';

class CounterText extends StatelessWidget {
  const CounterText({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      Counter.of(context).value.toString(),
      style: Theme.of(context).textTheme.headlineMedium,
    );
  }
}

import 'package:flutter/material.dart';

class Counter extends InheritedWidget {
  const Counter({
    super.key,
    required int value,
    required super.child,
  }) : _value = value;

  final int _value;
  get value => _value;

  static Counter? maybeOf(BuildContext context) {
    final Counter? result =
        context.dependOnInheritedWidgetOfExactType<Counter>();
    return result;
  }

  static Counter of(BuildContext context) {
    final Counter? result = maybeOf(context);
    assert(result != null, 'No FrogColor found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(Counter oldWidget) => _value != oldWidget._value;
  // bool updateShouldNotify(Counter oldWidget) => false;
}

import 'package:am049_object_box_future_provider/object_box.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'model/user.dart';

void main() {
  runApp(FutureProvider(
      create: (_) => ObjectBox.create(),
      initialData: null,
      child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am049 object box future provider',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'm049 object box future provider'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _usersController = TextEditingController();
  List<User> _entries = <User>[];

  @override
  Widget build(BuildContext context) {
    final _objectBox = context.watch<ObjectBox?>();

    setState(() {
      _entries = _objectBox?.store.box<User>().getAll() ?? <User>[];
    });

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.all(20),
                child: TextField(
                    controller: _usersController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'user',
                    ))),
            ElevatedButton(
              child: const Text('add user'),
              onPressed: () {
                User user = User(name: _usersController.text);
                _objectBox?.store.box<User>().put(user);
                setState(() {
                  _entries.add(user);
                });
              },
            ),
            Expanded(
                child: ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: _entries.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 50,
                        margin: const EdgeInsets.all(2),
                        color: Colors.red[100],
                        child: Center(
                            child: Text(
                          '$index : ${_entries[index].name}',
                          style: const TextStyle(fontSize: 18),
                        )),
                      );
                    })),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _objectBox?.store.box<User>().removeAll();
          setState(() {
            _entries = <User>[];
          });
        },
        tooltip: 'Increment',
        child: const Icon(Icons.delete),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

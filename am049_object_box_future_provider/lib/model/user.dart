import 'package:objectbox/objectbox.dart';

@Entity()
class User {
  int id = 0;
  String? name;

  User({this.name});
}

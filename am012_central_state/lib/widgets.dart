import 'package:flutter/material.dart';

class MyStateWidget extends StatefulWidget {
  const MyStateWidget({super.key});

  @override
  State<MyStateWidget> createState() => _MyState();

  static _MyState? maybeOf(BuildContext context) {
    return context.findAncestorStateOfType<_MyState>();
  }

  static _MyState of(BuildContext context) {
    final _MyState? result = maybeOf(context);
    assert(result != null, 'No MyState found in context');
    return result!;
  }
}

class _MyState extends State<MyStateWidget> {
  int _pick = 1;
  static const List<Color> _colors = [
    Colors.green,
    Colors.yellow,
    Colors.blue,
    Colors.red,
    Colors.brown,
    Colors.purple
  ];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            color: _colors[_pick],
            constraints: const BoxConstraints(
                maxHeight: 180.0,
                maxWidth: 180.0,
                minWidth: 180.0,
                minHeight: 180.0),
          ),
          const SizedBox(height: 20), // a padding trick
          const MyWidget()
        ],
      ),
    );
  }

  void _updatePick() {
    setState(() {
      _pick = (_pick + 1) % _colors.length;
    });
  }
}

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      // a trick to pass an argument to onPressed action ...
      onPressed: () => swapColor(context),
      child: const Text('Change Color', style: TextStyle(fontSize: 20)),
    );
  }

  void swapColor(BuildContext context) {
    _MyState state = MyStateWidget.of(context);
    state._updatePick();
  }
}

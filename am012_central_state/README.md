# am012_central_state

Come accedere allo stato di un antenato? Una strategia è quella di passare, passo dopo passo, fra gli qrgomenti dei `Widget` metodi e proprietà di una `StatefulWidget`, in alternativa si può usare il metodo statico `of`
```dart
static MyState of(BuildContext context) {
  final MyState? result = context.findAncestorStateOfType<MyState>();
  if (result != null) {
      return result;
  }
  throw FlutterError("MyState error");
}
```
o lavorare con i due metodi `mayBeOf` e `of`, come nel caso di `InheritedWidget`.

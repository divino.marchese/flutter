import 'package:flutter/material.dart';
import 'dart:developer' as developer;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am069 logging',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am069 logging'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Try logging',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: const Size(120, 50),
                foregroundColor: Colors.white,
                backgroundColor: Colors.red,
                padding: const EdgeInsets.all(16.0),
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                logError("error", "logButton");
                _incrementCounter();
              },
              child: const Text('error'),
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: const Size(120, 50),
                foregroundColor: Colors.white,
                backgroundColor: Colors.green,
                padding: const EdgeInsets.all(16.0),
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                logSuccess("success", "logButton");
                _incrementCounter();
              },
              child: const Text('success'),
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: const Size(120, 50),
                foregroundColor: Colors.black,
                backgroundColor: Colors.yellow,
                padding: const EdgeInsets.all(16.0),
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                logWhite("warning", "logButton");
                _incrementCounter();
              },
              child: const Text('warning'),
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: const Size(120, 50),
                foregroundColor: Colors.white,
                backgroundColor: Colors.black,
                padding: const EdgeInsets.all(16.0),
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                logWhite("white", "logButton");
                _incrementCounter();
              },
              child: const Text('white'),
            ),
            const SizedBox(height: 30),
            const Text(
              'You have pushed the button this many times:',
            ),
            const SizedBox(height: 20),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

// green
void logSuccess(String msg, String name) {
  developer.log('\x1B[32m$msg\x1B[0m', name: name);
}

// red
void logError(String msg, String name) {
  developer.log('\x1B[31m$msg\x1B[0m', name: name);
}

// yellow
void logWhite(String msg, String name) {
  developer.log('\x1B[37m$msg\x1B[0m', name: name);
}

// white
void logWarning(String msg, String name) {
  developer.log('\x1B[33m$msg\x1B[0m', name: name);
}

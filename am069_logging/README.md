# am069_logging

La seguente applicazione ha il solo scopo dimostrativo di usare il *logging* per un app Flutter. Il metood usato è
```dart
console.log(message, name)
```
Per colorare il messaggio
```dart
developer.log('[ANSI color code][your text][ANSI reset code]');
```
usando i colori
```
Reset:   \x1B[0m
Black:   \x1B[30m
White:   \x1B[37m
Red:     \x1B[31m
Green:   \x1B[32m
Yellow:  \x1B[33m
Blue:    \x1B[34m
Cyan:    \x1B[36m
```
[API](https://api.flutter.dev/flutter/dart-developer/log.html)
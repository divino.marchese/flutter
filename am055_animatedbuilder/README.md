# am055_animationbuilder

`AnimateBuilder` **[API](https://api.flutter.dev/flutter/widgets/AnimatedBuilder-class.html)** ci fornise un altro interessante supporto. In questo esempio creiamo `Boing()` una nostra `Curve` custom che su di un `double` deve andare da `0` ad `1`. QUesto esericizio è la continuazione di **am014_animation**. Altra cosa interessante: andiamo avanti ed indietro con l'animazione.

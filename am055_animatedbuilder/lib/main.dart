import 'dart:math';

import 'package:am055_animatedbuilder/animated_logo.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am055 animated builder',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am055 animated builder'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;
  bool forward = true;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 4), vsync: this);
    animation = Tween<double>(begin: 0, end: 1).animate(controller);
    controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  _animate() {
    setState(() {
      debugPrint('animate');
      animation = CurvedAnimation(
          parent: controller, curve: Boing(), reverseCurve: Curves.easeIn);
      if (forward) {
        forward = false;
        controller.forward();
      } else {
        forward = true;
        controller.reverse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: AnimatedLogo(animation: animation),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _animate(),
        tooltip: 'Increment',
        child: const Icon(Icons.replay),
      ),
    );
  }
}

class Boing extends Curve {
  @override
  double transform(double t) => 0.5 - 0.5 * cos(t * 5 * pi);
}

import 'package:flutter/material.dart';

class AnimatedLogo extends StatelessWidget {
  const AnimatedLogo({Key? key, required this.animation}) : super(key: key);

  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: AnimatedBuilder(
            animation: animation,
            builder: (context, child) {
              return Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                height: 300,
                width: 300,
                // child: const FlutterLogo(),
                child: Image.asset(
                  'assets/images/logo.png',
                  opacity: animation,
                ),
              );
            }));
  }
}

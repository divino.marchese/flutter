# am010_google_sign_in

Abbiamo preso spunto dal [seguente](https://www.gotut.net/flutter-authentication-tutorial/) articilo su Medium.

## register app android

Una volta creato il progetto firebase va registrata l'app android: il nome del pacchetto è nel nolstro caso
```
com.example.am010_google_sign_in
```
per ottenere la chiave di debug, richiesta per effettuare l'autenticazione, si seguono le istruzioni [qui](https://developers.google.com/android/guides/client-auth).

```
keytool -list -v \
-alias androiddebugkey -keystore ~/.android/debug.keystore
```
pwd di default `android`. Otteniamo
```
...
    MD5:  F3:1E:8A:35:E4:0F:4B:F8:0C:CA:6D:C6:8B:4F:EE:BC
    SHA1: 8F:7B:38:29:D9:F6:51:85:90:53:41:1A:A6:20:A6:A1:E5:B8:E1:25
    SHA256: 74:5C:86:BA:82:62:67:02:A4:0C:DB:46:9E:D2:3B:5C:DC:06:C2:1E:94:0E:D8:E3:EF:03:EA:09:69:4C:F0:F8
...
```
e immettiamo `SHA1`. Una volta fatto ciò possiamo procedere scaricando il file `json` che ci verrà proposto e copiamolo direttamente, una volta scaricato, dentro `android/app`.
Si prosegue toccando come indicato `build.gradle` (entrambi facendo attenzione). Se non fatto prima si ricordi di attivare sul progetto firebase l'autenticaziobe android.

## il progetto flutter

Per poter effettuare l'autenticazione devono essere installati i due pacchetti: 
```
dependencies:
  //...

  #AUTHENTICATIN
  firebase_auth: ^0.11.0
  google_sign_in: ^4.0.1+3
```
Le documentazioni sono rispettivamente: [firebase](https://github.com/flutter/plugins/tree/master/packages/firebase_auth) e [google](https://github.com/flutter/plugins/tree/master/packages/google_sign_in).

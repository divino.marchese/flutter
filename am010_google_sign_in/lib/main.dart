import 'package:am010_google_sign_in/auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Google Sign In'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  FirebaseUser mUser;
  String mUserName = "";
  bool isLoggingIn = false;

  Future clickLogin() async {
    //Set isLoggingIn to true at the start of clicking Login
    setState(() {
      isLoggingIn = true;
    });

    mUser = await loginWithGoogle().catchError((e) => setState(() {
      isLoggingIn = false;
    }));
    
    assert(mUser != null);

    setState(() {
      isLoggingIn = false;
      mUserName = mUser.displayName;
    });
  }

  void clickLogout() {
    setState(() {
      isLoggingIn = true;
    });

    
    logoutWithGoogle().whenComplete(() => setState(() {
          isLoggingIn = false;
          mUserName = 'No user is logged in!';
        }));
    
  }

  void clickDelete() {
    setState(() {
      isLoggingIn = true;
    });
    
    deleteUserWithGoogle(mUser).whenComplete(() => setState(() {
          isLoggingIn = false;
          mUser = null;
          mUserName = 'No user is logged in!';
        }));
    
  }

  @override
  Widget build(BuildContext context) {

    num cpbOpacity;
    isLoggingIn ? cpbOpacity = 1.0 : cpbOpacity = 0.0;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding( 
              padding: EdgeInsets.symmetric(vertical: 9.0, horizontal: 18.0),
              child: Text('User: $mUserName'),
            ),
            Padding (
              padding: EdgeInsets.symmetric(vertical: 9.0, horizontal: 18.0),
              child: RaisedButton (
                color: Colors.green,
                textColor: Colors.white,
                splashColor: Colors.greenAccent,
                child: const Text('LOGIN'),
                onPressed: isLoggingIn ? null : clickLogin
              ),
            ),
            Padding (
              padding: EdgeInsets.symmetric(vertical: 9.0, horizontal: 18.0),
              child: RaisedButton(
                color: Colors.blue,
                textColor: Colors.white,
                splashColor: Colors.blueAccent,
                child: const Text('LOGOUT'),
                onPressed: isLoggingIn ? null : clickLogout
              ),
            ),
            Padding (
              padding: EdgeInsets.symmetric(vertical: 9.0, horizontal: 18.0),
              child: RaisedButton(
                color: Colors.red,
                textColor: Colors.white,
                splashColor: Colors.redAccent,
                child: const Text('DELETE'),
                onPressed: mUser == null || isLoggingIn ? null : clickDelete,
              ), 
            ),
            Opacity (
              opacity: cpbOpacity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(width: 20.0),
                  Text('Please wait...')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
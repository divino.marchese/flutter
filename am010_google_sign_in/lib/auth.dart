import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

Future<FirebaseUser> loginWithGoogle() async {
  if(_googleSignIn.currentUser != null) {
    return _auth.currentUser();
  }
  final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  final FirebaseUser user = await _auth.signInWithCredential(credential);
  print("signed in " + user.displayName);
  return user;
}

Future<Null> logoutWithGoogle() async {
  await _auth.signOut();
  await _googleSignIn.signOut();
}

Future<Null> deleteUserWithGoogle(FirebaseUser user) async {  
  await user.delete();
  await logoutWithGoogle();
}
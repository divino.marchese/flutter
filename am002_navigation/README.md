# am002_navigation

**[231216]** Il `Navigator` è un *widget* che, come riferito nelle [API](https://api.flutter.dev/flutter/widgets/Navigator-class.html), ci permette di gestire i suoi figli con una disciplina ŝtack*. `MaterialPageRoute`, come scritto nelle [API](https://api.flutter.dev/flutter/material/MaterialPageRoute-class.html) ci permette di effettiuare una transizione morbida.
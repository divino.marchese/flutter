# am054_redux_counter

**[221210]** `flutter_redx` mette a disposizione dei `Widget`, vedi API [qui](https://pub.dev/documentation/flutter_redux/latest/). `redux` è il pacchetto di Dart che fornisce la meccanica interna [qui](https://pub.dev/documentation/redux/latest/) per le API.

## sistema dinamico

Lo stato viene gestito secondo il sistema dinamico, qui programmato esplicitamente
```
x(t), u(t) -> x(t+1)
```
`x(t)` stato `u(t)` ingresso al tempo `t`.

## implementazione

Per definire lo *store* noi diamo  
- lo stato iniziale 
- ed il *reducer* in cui viene definita la legge del sistema dinamico

`=>` `STore` vedasi [API](https://pub.dev/documentation/redux/latest/redux/Store-class.html); qui sotto un esempio tratto dalla documentazione, per chiarire
```dart
final increment = 'INCREMENT';
final decrement = 'DECREMENT';

int counterReducer(int state, action) {
  switch (action) {
    case increment:
      return state + 1;
    case decrement:
      return state - 1;
    default:
      return state;
  }
}

final store = Store<int>(counterReducer, initialState: 0);

print(store.state); 
store.dispatch(increment);
print(store.state); 
```
In sostanza è il motore dello stato, molto simile al vecchio approcio del BLOC pattern.  

`=>` `StoreProvider<S>` è il `Widget` che mette a disposizione lo stato `S` ai suoi discendenti, vedi [API](https://pub.dev/documentation/flutter_redux/latest/flutter_redux/StoreProvider-class.html): crea un contesto di osservabilità - *observable*.   
`=>` `StoreConnector`, [qui](https://pub.dev/documentation/flutter_redux/latest/flutter_redux/StoreConnector-class.html) per le API, è la parte dell'*observabl* :permette anche di agire sull'oggetto osservato.
> Every time the store changes, the Widget will be rebuilt.

Come possiamo ossservare il `ViewModel` appare come primo argomento ottenuto dallo *store* in modo versatile
nel *builder* troviamo oltre al solito oggetto `BuildContext`. Soffermiamoci sul secondo uso qui in questo semplice esempio
```dart
 StoreConnector<int, VoidCallback>(
    converter: (store) {
        return () => store.dispatch(Actions.Increment);
    },
    builder: (context, callback) {
        return FloatingActionButton(
            // Attach the `callback` to the `onPressed` attribute
            onPressed: callback,
            tooltip: 'Increment',
            child: const Icon(Icons.add),
        );
    },
),
```
in questo caso il `ViewModel` è un `VoidCallback`, la funzione passata come proprità ai bottoni
```dart
store.dispatch(Actions.Increment);
```
effettua sullo *store* l'*action*.

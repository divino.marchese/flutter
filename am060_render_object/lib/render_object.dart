import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class RenderMyWidget extends RenderProxyBox {
  RenderMyWidget({
    Color colorCanvas = Colors.transparent,
    Color colorChild = Colors.transparent,
  })  : _colorCanvas = colorCanvas,
        _colorChild = colorChild,
        super();

  Color _colorChild, _colorCanvas;

  Color get colorCanvas => _colorCanvas;
  set colorCanvas(Color colorCanvas) {
    if (_colorCanvas == colorCanvas) return;
    _colorCanvas = colorCanvas;
  }

  Color get colorChild => _colorChild;
  set colorChild(Color colorChild) {
    if (_colorChild == colorChild) return;
    _colorChild = colorChild;
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    context.canvas.drawColor(colorCanvas, BlendMode.srcOver);
    Paint paint = Paint()
      ..color = colorChild
      ..style = PaintingStyle.fill;
    context.canvas.drawCircle(
        Offset(offset.dx + size.width / 2, offset.dy + size.height / 2),
        size.width / 2 + 10,
        paint);
    /*
    context.canvas.drawRect(
        Rect.fromLTRB(
          offset.dx - 10,
          offset.dy - 10,
          offset.dx + size.width + 10,
          offset.dy + size.height + 10,
        ),
        paint);
    */
    if (child != null) {
      context.paintChild(child!, offset);
    }
  }
}

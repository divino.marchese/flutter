import 'package:flutter/cupertino.dart';

import 'render_object.dart';

class MyWidget extends SingleChildRenderObjectWidget {
  const MyWidget({
    Key? key,
    required this.colorCanvas,
    required this.colorChild,
    required Widget child,
  }) : super(key: key, child: child);

  final Color colorCanvas;
  final Color colorChild;

  @override
  RenderMyWidget createRenderObject(BuildContext context) {
    return RenderMyWidget(
      colorCanvas: colorCanvas,
      colorChild: colorChild,
    );
  }

  @override
  void updateRenderObject(BuildContext context, RenderMyWidget renderObject) {
    renderObject.colorCanvas = colorCanvas;
    renderObject.colorChild = colorChild;
  }
}

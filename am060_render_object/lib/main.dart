import 'package:flutter/material.dart';
import 'widget.dart';

void main() => runApp(const Center(
      child: MyWidget(
        colorCanvas: Colors.orange,
        colorChild: Colors.redAccent,
        child: Text(
          style: TextStyle(color: Colors.yellow, fontSize: 40),
          'Hello World!',
          textDirection: TextDirection.ltr,
        ),
      ),
    ));

# am060_render_object

## starting

Il punto di partenza è il solito `Hello World`.

```dart
void main() => runApp(
      const Center(
        child: Text(
          'Hello World!',
          textDirection: TextDirection.ltr,
        ),
      ),
    );
```

## widget

Ora costruiremo un *widget* legandolo al suo *render object*. `SingleChildRenderObjectWidget` è una classe astratta il cui nome la descrive, vedi [qui](https://api.flutter.dev/flutter/widgets/SingleChildRenderObjectWidget-class.html) per le API,
> *RenderObjectWidgets provide the configuration for RenderObjectElements, which wrap RenderObjects, ...*. A tale *widget* è associato un *element*

```dart
@override
SingleChildRenderObjectElement createElement() => SingleChildRenderObjectElement(this);
```
e gestiamo il `RenderObject` coi due metodi
```dart
@override
RenderMyWidget createRenderObject(BuildContext context) { ... }

@override
void updateRenderObject(BuildContext context, RenderMyWidget renderObject) { ... }
```
`RenderMyWidget` è un `RenderObject` che estende `RenderProxyBox`, per le API [qui](https://api.flutter.dev/flutter/rendering/RenderProxyBox-class.html), definito come:
> *A base class for render boxes that resemble their children.*

Il metodo
```dart
@override
void paint(PaintingContext context, Offset offset) { ... }
```
API [qui](https://api.flutter.dev/flutter/rendering/RenderProxyBoxMixin/paint.html), è la parte per disegnare!   
- `PaintingContext` è il contesto di disegno di un `RenderObject`, [qui](https://api.flutter.dev/flutter/rendering/PaintingContext-class.html) per le API, per ottenere il `Canvas` facciamo riferimento alla proprietà `canvas`. 
- `Offset` fissa le coordinate in alto a sinistra, come da prassi, vedi API [qui](https://api.flutter.dev/flutter/dart-ui/Offset-class.html), con `dx` e `dy` le coordinate in `double`.

Non serve definire il *layout* don
```dart
@override
void performLayout() { ... }
```

## materiali

[1] "The operation instruction of Flutter's RenderBox & principle analysis" [qui](https://programmer.group/the-operation-instruction-of-flutter-s-renderbox-principle-analysis.html). 

# am078_change_notifier_counter

Unn classico esempio di *observer pattern*, gli ascoltatori vengono notificati mediante
`notifyListeners` di `ChangeNotifier`, vedi [1], esso implementa `Listenable` che rappresenta *observable* senza fornire un metodo di notifica.

`ListenableBuilder` è l'*observer*, legato al `ChangeNotifier`.

Il tutto va messo in una `StatefulWidget` per integrare nello stato un `ChangeNotifier`.

## materiali

[1] `ChangeNotifier` [api](https://api.flutter.dev/flutter/foundation/ChangeNotifier-class.html).  
[2] `ListenableBuilder` [qui](https://api.flutter.dev/flutter/widgets/ListenableBuilder-class.html).  

# am036_ble

In questo esempio affrontiamo il **BLE**, Bluetooth Low Energy, scrivendo, come già fatto in precedenza, un **client**, per la precisione un *GATT client*. Il BLE permette al server - sensori od altro - di comunicare una piccola quantità di dati detti *attribute* con un minimo consumo di energia. Per Android, e non solo, invitamo a leggere la documentazione [qui](https://developer.android.com/guide/topics/connectivity/bluetooth-le); una buona introduzione sul BLE la troviamo su Wikipedia(en) [qui](https://en.wikipedia.org/wiki/Bluetooth_Low_Energy) alla voce deducata. Vediamo un po' quali sono i dettagli del protocollo
- **GATT client** e **GATT server** comunicano fra loro, GATT sta per Generic Attribute Profile; un GATT server fornisce un servizio tramire un **GATT profile** (specifiche di offerta del servizio), vedi i dettagli ufficiali [qui](https://www.bluetooth.com/specifications/gatt/services/) e la pagina di Wikipedia (en) di cui sopra;                 
- **characteristic** è il dato per come si prsesenta (il voltaggio di una batteria, latemperatura di un termostato ecc.) quindi assimilabile al concetto di **tipo**; un dispositivo può fornire più di un *characteristic*: è dato da un singolo valore e dei *descriptor* opzionali (vedi poi);
- **service** è l'insieme delle *characteristic* fornite dal dispositivo: *Health Thermometer* fornisce la temperatura e l'intervallo di tempo fra le misure; per i vari profili si rimanda alla pagina sopra citata di Wikipedia
- **descriptor** fornisce ulteriori elementi di informazione alle *characteristic* quali le unità di misura, valore massimo e minimo di un sensore e, come tale, è facoltativo;
- **attribute** è l'insieme dei 3 precedenti elementi di informazione, unserà il minor numero di byte possibile e viene identificato da un UUID del tipo
```
xxxxxxxx-0000-1000-8000-00805F9B34FB
```
[qui](https://www.bluetooth.com/specifications/gatt/services/) per i detagli ufficiali.

## gradle

In `build.gradle` sistemare
```
minSdkVersion 19
```
per garantire ad Android il supporto del BLE.

## manifest

Aggiungere
```
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```

## flutter_blue

`flutter_blue` è il pacchetto ufficiale, vedi [qui](https://pub.dev/packages/flutter_blue), ben aggiornato; èqui+(https://pub.flutter-io.cn/documentation/flutter_blue/latest/) per le API.

### FlutterBlue

`connectedDevice` è *getter* per `Future<BluetoothDevice>` la lista dei *paired device*
```
_bluetoothInit() async {
    // et paired devices not connected
    devicesList = await flutterBlue.connectedDevices; 
    // add connected devices 
    flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        _addDeviceTolist(result.device);
      }
    });
    flutterBlue.startScan();
  }
```
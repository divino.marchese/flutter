import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am036_ble',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'am036_ble'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<BluetoothDevice> _devices = List<BluetoothDevice>();

  final FlutterBlue _flutterBlue = FlutterBlue.instance; // il service
  BluetoothDevice _myDevice;
  bool _connected = false;

  @override
  void initState() {
    super.initState();
    _bluetoothInit();
  }

  _bluetoothInit() async {
    for(BluetoothDevice device in await _flutterBlue.connectedDevices) _addDeviceTolist(device);

    _flutterBlue.startScan(timeout: Duration(seconds: 4));
    _flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        _addDeviceTolist(result.device);
      }
    });
    _flutterBlue.stopScan();
  }

  _addDeviceTolist(final BluetoothDevice device) {
    if (!_devices.contains(device)) {
      setState(() {
        _devices.add(device);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: _connected ? _deviceView() : _connectView());
  }

  ListView _connectView() {
    List<Container> containers = List<Container>();
    for (BluetoothDevice device in _devices) {
      containers.add(Container(
          height: 50,
          child: Row(children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Text(device.name == '' ? '(unknown device)' : device.name),
                  Text(device.id.toString()),
                ],
              ),
            ),
            FlatButton(
                color: Colors.red,
                child: Text(
                  'Connect',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  _connect(device);
                })
          ])));
    }
    containers.add(Container(
      alignment: Alignment.center,
      child: IconButton(
      icon: Icon(Icons.autorenew), onPressed: _bluetoothInit
      )
    ));
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[...containers]
      );
  }

  void _connect(BluetoothDevice device) async {
    try {
      await device.connect();
    } catch (e) {
      if (e.code != 'already_connected') {
        _show("error");
        throw e;
      }
    }
    setState(() {
      _myDevice = device;
      _connected = true;
    });
  }

  FutureBuilder<List<BluetoothService>> _deviceView() {
    return FutureBuilder(
        future: _myDevice.discoverServices(),
        builder: (BuildContext context,
            AsyncSnapshot<List<BluetoothService>> snapshot) {
          List<Widget> children = List<Widget>();
          if (!snapshot.hasData) {
            children = <Widget>[
              Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
                height: 60,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text('Awaiting result...'),
              )
            ];
          } else {
            for (BluetoothService service in snapshot.data) {
              List<Widget> widgetList = List<Widget>();
              for (BluetoothCharacteristic characteristic
                  in service.characteristics) {
                widgetList.add(Text(characteristic.uuid.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ));
                if (characteristic.properties.read) {
                  widgetList.add(_characteristicReadView(characteristic));
                }
                if (characteristic.properties.notify) {
                  _setNotifyTrue(characteristic);
                  widgetList.add(_characteristicNotifyView(characteristic));  
                }
              }
              children.add(ExpansionTile(
                title: Text("service UUID: \n" + service.uuid.toString(),
                    style: TextStyle(fontWeight: FontWeight.bold)),
                children: widgetList,
              ));
            }
          }
          return Container(
              child: Column(
            children: <Widget>[...children],
          ));
        });
  }

  FutureBuilder<List<int>> _characteristicReadView(
      BluetoothCharacteristic characteristic) {
    return FutureBuilder(
        future: characteristic.read(),
        builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
          if (snapshot.hasData) {
            List<int> values = snapshot.data;
            return Container (
              height: 25,
              child: Row(children: <Widget>[
                IconButton(icon: Icon(Icons.arrow_downward), onPressed: null),
                Expanded(child: Text("data: ${_convert(values)}"),)
              ],),
            );
          } else
            return Container(child: Text("reading .... "));
        });
  }

  StreamBuilder<List<int>> _characteristicNotifyView(
      BluetoothCharacteristic characteristic) {
    return StreamBuilder(
        stream: characteristic.value,
        builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.done: 
            if (snapshot.hasData) {
            List<int> values = snapshot.data;
            return Container (
              height: 25,
              child: Row(children: <Widget>[
                IconButton(icon: Icon(Icons.check), onPressed: null),
                Expanded(child: Text("data: ${_convert(values)}"),)
              ],),
            );
          }
          }

          
    
        });
  }

  _setNotifyTrue(BluetoothCharacteristic characteristic) async => await characteristic.setNotifyValue(true);

  String _convert(List<int> uint8List) {
    String msg ="";
    uint8List.forEach((i) {
      msg += String.fromCharCode(i);
    });
    return msg;
  }

  // show snackbar
  void _show(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg)));
  }
}
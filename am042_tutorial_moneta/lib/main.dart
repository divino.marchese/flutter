import 'dart:async';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:sensors_plus/sensors_plus.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const MyHomePage(title: 'Testa o Croce'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late FlipCardController _controller;
  late StreamSubscription _userAccelerometerSub;

  @override
  void initState() {
    super.initState();
    _controller = FlipCardController();
    _userAccelerometerSub =
        userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      // vertical gesture
      setState(() {
        if (event.z.abs() > 2) _flip(_controller);
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _userAccelerometerSub.cancel();
  }

  Future<void> _flip(FlipCardController controller) async {
    AudioCache player = AudioCache();
    const alarmAudioPath = "ding.mp3";
    player.play(alarmAudioPath);
    Random rnd = Random();
    int times = 1 + rnd.nextInt(4);
    for (int i = 1; i <= times; i++) {
      await Future.delayed(const Duration(milliseconds: 500));
      controller.toggleCard();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlipCard(
              controller: _controller,
              direction: FlipDirection.VERTICAL, // default HORIZONTAL
              front: SizedBox(
                width: 250,
                height: 250,
                child: Image.asset('assets/testa.jpeg'),
              ),
              back: SizedBox(
                width: 250,
                height: 250,
                child: Image.asset('assets/croce.jpeg'),
              ),
            )
          ],
        ),
      ),
    );
  }
}

# am044_testa_croce_tutorial

Un altro profetto *tutorial* per i laboratori del Biennio.

[[_TOC_]]

## Iniziamo

Il primo passo vede la creazione dell'applicazione: per questo rimandiamo ai cari tutorial.
Su Visual Studio Code `CTRL=SHIFT=P` e `Flutter: New Project`.
Creato il progetto del "Bottone" possiamo cancellare i commenti in verde se ci danno fastidio (sono comunque utili per imparare).

## il titolo, il colore della barra e l'icona

Cambiamo il titolo dell'applicazione
```dart
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Testa o Croce',
```
Per il colore usiamo la classe `Colors` vedi [qui](https://api.flutter.dev/flutter/material/Colors-class.html)
```dart
theme: ThemeData(
    primarySwatch: Colors.orange,
),
```
e per l'icona
```dart
floatingActionButton: FloatingActionButton(
    onPressed: _incrementCounter,
    tooltip: 'Increment',
    child: const Icon(Icons.animation),
), 
```
invitiamo a vedere la documentazione della classe `Icons` [qui](https://api.flutter.dev/flutter/material/Icons-class.html).

## flip card

A questo unto introduciamo l'animazione, per comodità useremo un **pacchett** che farà per noi il lavoro: `flip_card`. Modifichiamo il **file di configurazione** della nostra app `pubspec.yaml` nel modo seguente
```
dependencies:
  flutter:
    sdk: flutter
  flip_card: ^0.6.0
```
Se non carica il pacchetto, sul terminale di Visual Studio Code dare `flutter pub get`.
Ora leggeremo quanto scritto nel `README.md` del pacchetto [qui](https://pub.dev/packages/flip_card).

### step I: sistemiamo lo stato

Cancelliamo
```dart
int _counter = 0;

void _incrementCounter() {
  setState(() {
      ...
    _counter++;
  });
}
```
e mettere
```dart
class _MyHomePageState extends State<MyHomePage> {
  
  late  FlipCardController _controller;

  @override
  void initState() {
    super.initState();
    _controller = FlipCardController();
  }

```
Il `_controller` lo useremo poi!

### step 2: inseriamo la card

Cancelliamo ed inseriamo come sotto indicato.
```dart
child: Column(
  mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      FlipCard(
        direction: FlipDirection.HORIZONTAL, // default
        front: const Text('Testa'),
        back: const Text('Croce')
      )
    ],
  ),
```
il file `main.dart` diventa rosso in quanto c'è un errore, cancelliamo il *floating action button*
```dart
floatingActionButton: FloatingActionButton(
  onPressed: ...,
  tooltip: '...',
  child: const Icon(Icons.animation),
), 
```
e tutto si sistema, usando `CTRL+SHIFT+i` il codice (senza errori) si mette in ordine (indentazione). Possiamo testare su Chrome o su un device emulato, ma anche sul nostro telefonino. Con `CTRL+SHIFT+P` si sceglie `Flutter: Launch Emulator` e si sceglie (avendone creato uno in Android Studio). Per eseguire (su emulatore o Chrome) `Run debugging` (sul menù `Run`).

## step 3: inseriamo le immagini della moneta

La guida di riferimento è questa [qui](https://docs.flutter.dev/development/ui/assets-and-images).
Su `pubspec.yaml` troviamo
```
# assets:
#    - img/a_dot_burr.jpeg
à    - img/a_dot_ham.jpeg
```
togliamo i commenti e modifichiamo 
```
assets:
  - assets/testa.jpeg
  - assets/croce.jpeg
```
a questo punto nella directory del nostro progetto, anche da file manager, aggiungiamo la `assets` dentro la directory `img` e dentro i due file: uno per tasta e l'altro per croce. Ora rimpiazziamo
```dart
FlipCard(
  direction: FlipDirection.VERTICAL, 
    front: SizedBox(
      width: 250,
      height: 250,
      child: Image.asset('assets/testa.jpeg'),
    ),
    back: SizedBox(
      width: 250,
      height: 250,
      child: Image.asset('assets/croce.jpeg'),
    )
)
```
facendo come al solito alle parentesi tonde e dopo sistemando, in caso, il codice col solito `CTRL+SHIFT+i`.

## step 4: agitiamo e giriamo

Una piccola premessa.

### permessi android

in `AndroidManifest.xml` vanno settai i permessi per l'uso dell'accelerometro.
```
...
<uses-permission android:name="android.permission.ACTIVITY_RECOGNITION"/>

   <application
...
```

Ve ne siete accorti che basta toccare la moneta per girarla, ora aggiungiamo la possibilità di usare il telefonino agitandolo dal basso all'alto. Ci serve il pacchetto per gestire i sensori: andiamo su `pubspec.yaml` e modifichiamo con un'aggiunta come qui sotto mostrato.
```
dependencies:
  flutter:
    sdk: flutter
  flip_card: ^0.6.0
  sensors_plus: ^1.3.1
```
ecco come modificare il codice (ed ecco il motivo di cosa abbiamo scritto in precedenza)
```dart
late FlipCardController _controller;
late StreamSubscription _userAccelerometerSub;

@override
void initState() {
  super.initState();
  _controller = FlipCardController();
  _userAccelerometerSub =
      userAccelerometerEvents.listen((UserAccelerometerEvent event) {
    // vertical gesture
    setState(() {
      if (event.z.abs() > 2) _flip(_controller);
    });
  });
}

Future<void> _flip(FlipCardController controller) async {
  Random rnd = Random();
  int times = 1 + rnd.nextInt(4);
  for (int i = 1; i <= times; i++) {
    await Future.delayed(const Duration(milliseconds: 500));
    controller.toggleCard();
  }
}
```
La sequenza logica vede
- attivato l'accelerometro viene ascoltato
- una volta generato l'evento si attiva il `_controller` che può girare la moneta `controller.toggleCard()` ... naturalmente noi gireremo la moneta un numero *random* di volte. 

Tra una girata e l'altra
```dart
await Future.delayed(const Duration(milliseconds: 500));
```
attendiamo mezzo secondo.

## step 5: il suono

Aggiungiamo un pacchetto su `pubspec.yaml`
```
dependencies:
  flutter:
    sdk: flutter
  flip_card: ^0.6.0
  sensors_plus: ^1.3.1
  audioplayers: ^0.20.1
```
salviamo così da scaricare i pacchetti. Aggiungiamo l'*asset* come fatto per le immagini, sempre su `pubspec.yaml`
```
assets:
  - assets/testa.jpeg
  - assets/croce.jpeg
  - assets/ding.mp3
```
aggiungiamo quindi le ultime modifiche
```dart
Future<void> _flip(FlipCardController controller) async {
  AudioCache player = AudioCache();
  const alarmAudioPath = "ding.mp3";
  player.play(alarmAudioPath);
  Random rnd = Random();
  int times = 1 + rnd.nextInt(4);
  for (int i = 1; i <= times; i++) {
    await Future.delayed(const Duration(milliseconds: 500));
    controller.toggleCard();
  }
}
```
ed e`finita!

## step 6: cose non raccontate

Abbiamo aggiunto anche
```dart
@override
void initState() {
   ...   
}

@override
void dispose() {
  super.dispose();
  _userAccelerometerSub.cancel();
}
```
il metodo `void dispose()` fa sì che, quando l'app è in background non riceva gli eventi dall'accelerometro.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sensors/flutter_sensors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am028 sensors',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am028 sensors'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _accelAvailable = false;
  bool _gyroAvailable = false;
  bool _accelEnable = false;
  bool _gyroEnable = false;

  List<double> _accelData = List.filled(3, 0.0);
  List<double> _gyroData = List.filled(3, 0.0);
  StreamSubscription? _accelSubscription;
  StreamSubscription? _gyroSubscription;

  @override
  void initState() {
    _checkAccelerometerStatus();
    _checkGyroscopeStatus();
    super.initState();
  }

  @override
  void dispose() {
    _stopAccelerometer();
    _stopGyroscope();
    super.dispose();
  }

  void _checkAccelerometerStatus() async {
    await SensorManager()
        .isSensorAvailable(Sensors.ACCELEROMETER)
        .then((result) {
      setState(() {
        _accelAvailable = result;
      });
    });
  }

  Future<void> _startAccelerometer() async {
    if (_accelEnable) return;
    if (_accelAvailable) {
      final stream = await SensorManager().sensorUpdates(
        sensorId: Sensors.ACCELEROMETER,
        interval: Sensors.SENSOR_DELAY_FASTEST,
      );
      setState(() {
        _accelEnable = true;
      });
      _accelSubscription = stream.listen((sensorEvent) {
        setState(() {
          _accelData = sensorEvent.data;
        });
      });
    }
  }

  void _stopAccelerometer() {
    if (!_accelEnable) return;
    _accelSubscription?.cancel();
    _accelSubscription = null;
    setState(() {
      _accelEnable = false;
    });
  }

  void _checkGyroscopeStatus() async {
    await SensorManager().isSensorAvailable(Sensors.GYROSCOPE).then((result) {
      setState(() {
        _gyroAvailable = result;
      });
    });
  }

  Future<void> _startGyroscope() async {
    if (_gyroEnable) return;
    if (_gyroAvailable) {
      final stream =
          await SensorManager().sensorUpdates(sensorId: Sensors.GYROSCOPE);
      setState(() {
        _gyroEnable = true;
      });
      _gyroSubscription = stream.listen((sensorEvent) {
        setState(() {
          _gyroData = sensorEvent.data;
        });
      });
    }
  }

  void _stopGyroscope() {
    if (_gyroSubscription == null) return;
    _gyroSubscription?.cancel();
    _gyroSubscription = null;
    setState(() {
      _gyroEnable = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              "Accelerometer:",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            Text(
              _accelAvailable ? "AVAIDABLE" : "NOT AVAIDABLE",
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            Text(
              _accelEnable ? "ENABLED" : "DISABLED",
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[0](X) = ${_accelData[0]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[1](Y) = ${_accelData[1]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[2](Z) = ${_accelData[2]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  child: const Text("Start"),
                  color: Colors.green,
                  onPressed:
                      _accelAvailable ? () => _startAccelerometer() : null,
                ),
                const Padding(
                  padding: EdgeInsets.all(8.0),
                ),
                MaterialButton(
                  child: const Text("Stop"),
                  color: Colors.red,
                  onPressed:
                      _accelAvailable ? () => _stopAccelerometer() : null,
                ),
              ],
            ),
            const SizedBox(
              //Use of SizedBox
              height: 80,
            ),
            const Text(
              "Gyroscope:",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            Text(
              _gyroAvailable ? 'AVAIDABLE' : 'NOT AVAIDABLE',
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            Text(
              _gyroEnable ? 'ENABLED' : 'DISABLED',
              style:
                  const TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[0](X) = ${_gyroData[0]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[1](Y) = ${_gyroData[1]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Text(
              "[2](Z) = ${_gyroData[2]}",
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 16.0)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  child: const Text("Start"),
                  color: Colors.green,
                  onPressed: _gyroAvailable ? () => _startGyroscope() : null,
                ),
                const Padding(
                  padding: EdgeInsets.all(8.0),
                ),
                MaterialButton(
                  child: const Text("Stop"),
                  color: Colors.red,
                  onPressed: _gyroAvailable ? () => _stopGyroscope() : null,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

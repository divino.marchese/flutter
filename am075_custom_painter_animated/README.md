# am075_custom_painter_animated

- ver: 250110

In questo esempio prendoamo in considerazione le animazioni.

## `Animation`

Rappresenta l'evoluzione di un singolo valore che cambia nel tempo. 
Essa presenta un **valore** ed uno **stato**: può andara avanti, indietro ed essere
stoppata all'inizio o alla fine. Implementa `Listenable` e quindi abbiamo a disposzione
due *listener*, uno per il valore ed uno per lo stato (quindi due *callback*).
Per i dettaglio vedi le API in [1].

## `AnimationController`

Permette di gestire una animazione, è esso stessa una `Animation`. Essendo legata alla gestione
dei *frame* operata da `Flutter` ha bisogno di un `Ticker` offerto da un `TickerProvider`,
Un `Ticker` chiama il suo *callback* mediante il suo `TickerPorvider` ad ogni *frame* (quando il `Widget` è attivo).
L'animazione controllata ha poi una durata.

## `TickerProvider`

Fornisce un `Ticker` e viene usato di solito come *mixin* dello `State` di una `StatefulWidget`,
come ci si può aspettare. Il più efficiente è `SingleTickerProviderStateMixin`. Viene passato
all `AnimationController` nel costruttore tramite il parametro del costruttore `vsync`. 

## materiali

[1] `Animation` [qui](https://api.flutter.dev/flutter/animation/Animation-class.html).  
[2] `AnimationController` [qui](https://api.flutter.dev/flutter/animation/AnimationController-class.html).  
[3] `Ticker` [qui](https://api.flutter.dev/flutter/scheduler/Ticker-class.html).  
[4] `TickerProvider` [qui](https://api.flutter.dev/flutter/scheduler/TickerProvider-class.html).  

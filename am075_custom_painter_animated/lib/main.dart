import 'package:am075_coustom_painter_animated/animated_custom_painter.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am075 custom painter animated',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am075 custom painter animated'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  bool _forward = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
  }

  @override
  void dispose() {
    // Properly dispose the controller. This is important!
    _controller.dispose();
    super.dispose();
  }

  void _startAnimation() {
    _forward ? _controller.forward() : _controller.reverse();
    setState(() {
      _forward = !_forward;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.surface,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "animation ..",
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            SizedBox(
              width: 400,
              height: 600,
              child: CustomPaint(
                painter: AnimatedCustomPainter(_controller.view),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _startAnimation,
        tooltip: 'animate',
        child: _forward
            ? const Icon(Icons.arrow_upward)
            : const Icon(Icons.arrow_downward),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

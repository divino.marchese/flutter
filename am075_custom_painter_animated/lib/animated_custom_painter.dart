import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedCustomPainter extends CustomPainter {
  final _paint = Paint();
  final Animation<double> _size;
  final Animation<double> _offset;
  final Animation<Color?> _color;

  AnimatedCustomPainter(Animation<double> animation)
      : _size = Tween<double>(begin: 50, end: 150).animate(animation),
        _offset = Tween<double>(begin: 200, end: 0).animate(animation),
        _color = ColorTween(begin: Colors.yellowAccent, end: Colors.lightBlue)
            .animate(animation),
        super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    _paint.color = _color.value ?? Colors.white;
    final Paint border = Paint();
    border
      ..color = Colors.black
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    canvas.drawCircle(
      Offset(
        size.width / 2,
        size.height / 2 + _offset.value,
      ),
      _size.value,
      _paint,
    );
    canvas.drawArc(
      Offset(
            size.width / 2 - _size.value,
            size.height / 2 - _size.value + _offset.value,
          ) &
          Size(_size.value * 2, _size.value * 2),
      0,
      2 * pi,
      false,
      border,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

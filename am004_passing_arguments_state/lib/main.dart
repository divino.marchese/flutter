import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

final Map<String, String> bundle = {"message": ""};

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigation with Arguments',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/second': (context) => const SecondScreen(),
      },
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController textFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Screen'),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("scrivi qui sotto"),
            SizedBox(
              width: 150.0,
              height: 70,
              child: TextField(
                controller: textFieldController,
              ),
            ),
            ElevatedButton(
                child: const Text("Send"),
                onPressed: () {
                  _sendData(context);
                })
          ],
        ),
      ),
    );
  }

  void _sendData(BuildContext context) {
    String textToSend = textFieldController.text;
    Navigator.pushNamed(
      context,
      '/second',
      arguments: textToSend,
    );
  }
}

class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  String text = 'Hello';

  @override
  Widget build(BuildContext context) {
    _getData(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Screen'),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: Center(
        child: Text(text, style: const TextStyle(fontSize: 40)),
      ),
    );
  }

  void _getData(BuildContext context) {
    // start the SecondScreen and wait for it to finish with a result
    String? result = ModalRoute.of(context)?.settings.arguments.toString();
    setState(() {
      text = result ?? "";
    });
  }
}

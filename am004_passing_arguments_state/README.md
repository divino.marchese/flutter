# am004_passing_arguments_state

**231216]** Riprendendo l'esercizio precedente proviamo a lavorare con le `StatefulWidget`
- `TextEditingController` [API](https://api.flutter.dev/flutter/widgets/TextEditingController-class.html).
- `ModalRoute` ci permette di mantenere le informazioni del `Navigator`, [API](https://api.flutter.dev/flutter/widgets/ModalRoute-class.html), Il Metodo *static* `ModalRoute-of(BuildContext context)` ci ritorna il prossimo `ModalRoute` (vedi API), esso è *nullable* potendo non esserci, ed ecco la cautela degli operatori per la *null safety*.
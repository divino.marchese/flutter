import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am035_bluetooth',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BluetoothApp(title: 'am035_bluetooth'),
    );
  }
}

class BluetoothApp extends StatefulWidget {
  final String title;

  BluetoothApp({Key key, this.title}) : super(key: key);

  @override
  _BluetoothAppState createState() => _BluetoothAppState();
}

class _BluetoothAppState extends State<BluetoothApp> {
  // for a SnackBar
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // globl bluetooth data
  FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;
  BluetoothState _state =
      BluetoothState.UNKNOWN; // the state of bluetooth connection
  List<BluetoothDevice> _devicesList = []; // the devices list
  // for a single device
  BluetoothDevice _device; // a generic device
  BluetoothConnection _connection; // connection to device

  bool _btDisabled = true; // disable connect button
  bool _connected = false; // enable sending message
  String _msg =
      'NOTE: If you cannot find the device in the list, ' // red message
      'please pair the device by '
      'going to the bluetooth settings';

  @override
  void initState() {
    super.initState();
    _bluetoothInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  "paired devices",
                  style: TextStyle(fontSize: 24, color: Colors.green),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Device:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    DropdownButton(
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() => _device = value),
                      value: _device,
                    ),
                    RaisedButton(
                      onPressed: _btDisabled
                          ? null
                          : _connected ? _disconnect : _connect,
                      child: Text(_connected ? 'Disconnect' : 'Connect'),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Card(
                  elevation: 4,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            _device == null ? 'device?' : _device.name,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.green,
                            ),
                          ),
                        ),
                        FlatButton(
                          onPressed:
                              _connected ? _sendOnMessageToBluetooth : null,
                          child: Text("ON"),
                        ),
                        FlatButton(
                          onPressed:
                              _connected ? _sendOffMessageToBluetooth : null,
                          child: Text("OFF"),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: Text(
                      _msg,
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  // widget helper for list
  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devicesList.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devicesList.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }

  // bluetooth init
  Future<void> _bluetoothInit() async {
    // monitoring the Bluetooth adapter state changes.
    _bluetooth.onStateChanged().listen((state) {
      show(state.toString());
      if (state == BluetoothState.STATE_ON) {
        setState(() {
          _state = state;
          _btDisabled = false;
        });
      } else {
        setState(() {
          _state = state;
          _btDisabled = true;
          _msg = "not connected";
        });
      }
    });

    // enable bluetooth
    await _enableBluetooth();

    // It is an error to call [setState] unless [mounted] is true.
    if (!mounted) {
      return;
    }

    // obtain paired devices
    await _getPairedDevices();
    // populate list
    await _getDeviceItems();
  }

  Future<void> _enableBluetooth() async {
    if (_state != BluetoothState.STATE_ON) {
      await _bluetooth.requestEnable();
    }
    setState(() {
      _btDisabled = false;
    });
  }

  Future<void> _getPairedDevices() async {
    List<BluetoothDevice> devicesList = [];
    // To get the list of paired devices
    try {
      devicesList = await _bluetooth.getBondedDevices();
    } on PlatformException {
      show("Error: unable to get paired devices");
    }
    setState(() {
      _devicesList = devicesList;
    });
  }

  // Method to connect to bluetooth
  void _connect() async {
    show('connectiong');
    if (_device == null)
      show('No device selected');
    else {
      // await FlutterBluetoothSerial.instance.isEnabled;
      try {
        _connection = await BluetoothConnection.toAddress(_device.address);
        if (_connection.isConnected) {
          show('connected');
          setState(() {
            _connected = true;
            _msg = '"' + _device.name + '" connected';
          });
        }
      } catch (e) {
        show('exception occurred');
        print(e.toString());
      }
      _receiveMessage(); // listen messages froms erver
    }
  }

  void _disconnect() async {
    // Closing the Bluetooth connection
    await _connection.close();
    if (!_connection.isConnected) {
      show('disconnected');
      setState(() {
        _connected = false;
        _msg = '"' + _device.name + '" disconnected';
      });
    }
  }

  // Method to send message,
  // for turning the bletooth device on
  void _sendOnMessageToBluetooth() {
    if (_connection != null && _connection.isConnected) {
      show('Device Turned On');
      _connection.output.add(ascii.encode('1' + '\n\r'));
    }
  }

  // Method to send message,
  // for turning the bletooth device off
  void _sendOffMessageToBluetooth() {
    if (_connection != null && _connection.isConnected) {
      show('Device Turned Off');
      _connection.output.add(ascii.encode('0' + '\n\r'));
    }
  }

  // recieve message if connected
  void _receiveMessage() {
    if (_connection != null && _connection.isConnected) {
      _connection.input.listen((uint8List) {
        String msg = '';
        uint8List.forEach((i) {
          msg += String.fromCharCode(i);
        });
        setState(() {
          _msg = 'received from "' + _device.name + '": ' + msg.toString();
        });
      });
    }
  }

  // show snackbar
  void show(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg)));
  }
}

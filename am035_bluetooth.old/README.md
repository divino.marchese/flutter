# am035_bluetooth

Questo è un esempio di serial bluetooth **client**. Per un inquadramento dei vari protocolli Bluetooth rimandiamo alla voce di Wikipedia (en) [qui](https://it.wikipedia.org/wiki/Bluetooth). Leggere per i pemessi da scrivere sul *manifest* (Android) la documentazione_ [qui](https://developer.android.com/guide/topics/connectivity/bluetooth). Qui useremo il protocollo *seriale* classi co per il bluetooth.

Useremo il pacchetto [flutter_bluetooth_serial](https://github.com/edufolly/flutter_bluetooth_serial), per vedere le classi [qui](https://pub.dev/documentation/flutter_bluetooth_serial/latest/flutter_bluetooth_serial/flutter_bluetooth_serial-library.html).

## il server 

La nostra app è un **client** noi useremo per i test, su di un altro telefono l'app [bluetooth SPP server](https://play.google.com/store/apps/details?id=mobi.minipedia.btserverandroid&hl=it).

## gradle

Su `build.gradle` portare a `18` il `minSdkVersion`
```
defaultConfig {
        // TODO: Specify your own unique Application ID (https://developer.android.com/studio/build/application-id.html).
        applicationId "com.example.am035_bluetooth"
        minSdkVersion 18
        targetSdkVersion 28
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
```

## BluetoothSerial

Veniamo quindi al pacchetto flutter (non nuovissimo o aggiornato come BLE ma ancora ben utilizzabile). Per le api [qui](https://pub.dev/documentation/flutter_bluetooth_serial/latest/flutter_bluetooth_serial/flutter_bluetooth_serial-library.html#classes).

### FlutterBluetoothSerial

`FlutterBluetoothSerial` è la classi principale, fa riferimento al `local bluetooth adapter` (vedi anche Androisd API), per ottenere il sercizio noi useremo
```
FlutterBluetoothSerial.istance
```

### BluetoothState

`BluetoothState` rappresenta lo stato del servizio che può assumere vari valori. Mediante il metodo 
```
Stream<BluetoothState> onStateChanged () 
```
del servizio, un oggetto di classe `FlutterBluetoothSerial` noi monitoriamo mediante *stream* gli eventi di cambiamento di stato. Diamo inizialmente lo stato `UNKNOWN` per poi ascoltare i cambiamenti di stato ed abilitare parte dell'interfaccia.

### BluetoothDevice

Per ottenere i *device* accoppiati, i *paired device*, oggetti della classe `BluetoothDevice`, chiamiamo del servizio 
```
Future<List<BluetoothDevice>> getBondedDevices () 
```
Nel nostro esempio i device li abbiamo accoppiati in precedenza. Per scansionare i device disponibili (non accoppiati) rimandiamo alla documentazione ed alla ricca app di esempio.

### BluetoothConnection

La connessione che ci permette di interagire col dispositivo accoppiato che prevede un **server in esecuzione** la otteniamo
```
void _connect() async {
    ...
    if (_device == null)
      show('No device selected');
    else {
      try {
        _connection = await BluetoothConnection.toAddress(_device.address);
        if (_connection.isConnected) {
          ...
          setState(() {
            _connected = true;
          });
        }
      } catch (e) {...}
    }
  }
```

### inviare i dati

Usiamo un `Sinc`, come se avessimo un *socket*
```
Future send(Uint8List data) async {
    connection.output.add(data);
    await _connection.output.allSent;
}
```

### ricevere dati

Una volta connessi al device riceviamo i dati mediante gli eventi di uno stream (di *unsigned btte*)
```
void _receiveMessage() {
    if (_connection != null && _connection.isConnected) {
      _connection.input.listen((uint8List) {
        String msg = '';
        uint8List.forEach((i){ msg += String.fromCharCode(i);});
        setState(() {
          _msg = 'received from "' + _device.name + '": ' + msg.toString();
        });
       });
    }
  }
```

## materiali 

[1] "Flutter: Creating an IoT based app to interact with any home electrical equipment" di S. Biswas su Medium [qui](https://medium.com/flutter-community/flutter-creating-an-iot-based-app-to-interact-with-any-home-electrical-equipment-36d510b2478).  
[2] Esempio ufficiale tratto da GitHun [qui](https://github.com/edufolly/flutter_bluetooth_serial/tree/master/example/lib).
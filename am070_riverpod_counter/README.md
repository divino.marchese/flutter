# am070_riverpod_counter

`riverpod` è anagramma di  `provider`, in questa recente versione usa *annotation* e *build runner*. Usa `build_runner` e le *custom annotation* per generare il codice in `main.g.dart` avendo preventivamente incluso tale file prima di generarlo con `part main.g.dart`. Per altri dettagli si rimanda ad [1].

## `ProviderScoper`

È il *widget* apicale per esporre i `@riverpod`, vedi [3].

## `@riverpod`

È l'*annotation* che incontriamo per generare il *provider* [4] mediante `riverpod_generator`, la "ricetta", e `build_runner`, lo strumento per eseguire la "ricetta". Nel nostro caso la classe è privata, vedi `_`, ed auto-generata, vedi `$`
```dart
_$Counter
```
Notiamo che la classe estesa è `AutoDIsposeNotifier` [4], che rimada a `AutoDisposeNotifier`. Nel metodo `build` definiamo lo stato iniziale, poi `increment`lo aggiungiamo noi. I metodi `watch` e `read` si comportano come abbiamo appreso già negli esempi del `providerr`. Qui sotto, quindi, il **modello dinamico**
```dart
@riverpod
class Counter extends _$Counter {
  @override
  int build() => 0;

  void increment() => state++;
}
```
Invece di usare un `Consumer` noi useremo un `<consumerWidget` (per agilità e sintesi).

## generazione

Dando come al solito il comando
```
dart run build_runner build
```
utile anche `doctor` e `clean`.

## materiali

[1] Installazione [qui](https://riverpod.dev/docs/introduction/getting_started).  
[2] `build_runner` [qui](https://pub.dev/packages/build_runner).  
[3] `fluter_riverpod` [api](https://pub.dev/documentation/flutter_riverpod/latest/).  
[4] `riverpod_annotation` [api](https://pub.dev/documentation/riverpod_annotation/latest/riverpod_annotation/).  
import 'package:flutter/material.dart';

class MyPaint extends CustomPainter{

  Offset _gp; // external global position
  double _movex;
  double _movey;
  

  MyPaint(this._movex, this._movey, this._gp);

  @override
  void paint(Canvas canvas, Size size) {

    // first circle 
    Paint paint = Paint()
    ..strokeWidth = 8
    ..style = PaintingStyle.stroke
    ..color = Colors.orange;
    Size dim  = Size(size.width*0.15, size.width*0.15);
    Offset pos = Offset(size.width*0.5 -dim.width*0.5 + _movex, size.height*0.5 - dim.height*0.5 + _movey);
    canvas.drawRect(pos & dim, paint);
  }

  @override
  bool shouldRepaint(MyPaint oldDelegate) 
    => (oldDelegate._gp.dx != _gp.dx || oldDelegate._gp.dy != _gp.dy);
  
}
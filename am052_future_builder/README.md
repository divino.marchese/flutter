# am052_future_builder

**[231217]** Un esempio semplice, con interessanti pratiche di programmazione, per `FutureBuilder` **[API](https://api.flutter.dev/flutter/widgets/FutureBuilder-class.html)**. 

## nota

La lista di dati non appare più come stato della *stateful widget*! `AsyncSnapshot` **[API](https://api.flutter.dev/flutter/widgets/AsyncSnapshot-class.html)**
> *Immutable representation of the most recent interaction with an asynchronous computation.*

ci permette di recuperare lo stato del nostro *future* e quindi gli eventuali dati: possiamo scegliere di fatto
```dart
List<Data> dataList = snapshot.requireData;
```
ovvero, più classicamente, rispettando la *null safety*
```dart
List<Data> dataList = snapshot.data ?? <Data>[];
```
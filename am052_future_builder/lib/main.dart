import 'dart:math';

import 'package:flutter/material.dart';

import 'data.dart';
import 'data_tile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am052 future builder',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am052 future builder'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static final Random _rnd = Random();

  final Future<List<Data>> _getData = Future<List<Data>>.delayed(
      Duration(seconds: 3 + _rnd.nextInt(4)),
      () => List<Data>.generate(7, (i) => Data()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: Center(
        child: FutureBuilder<List<Data>>(
            future: _getData,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Data> dataList = snapshot.requireData;
                return ListView(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    children:
                        dataList.map((data) => DataTile(data: data)).toList());
              } else {
                return const CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}

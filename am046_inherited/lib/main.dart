import 'package:flutter/material.dart';

import 'my_inherited_widget.dart';

void main() {
  runApp(
    MyInheritedWidget(
      message: "Hello from My Hinerited",
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am046 inherited',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const FirstScreen(title: 'am046 inherited'),
    );
  }
}

class FirstScreen extends StatelessWidget {
  final String title;

  const FirstScreen({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    final message = MyInheritedWidget.of(context).message;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.surface,
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            SizedBox(height: 20,),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => SecondScreen(title: "second screen")),
                );
              },
              child: Text("Go to Second Screen"),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  final String title;
  
  const SecondScreen({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    final message = MyInheritedWidget.of(context).message;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.surface,
        title: Text(title),
      ),
      body: Center(
        child: Text(
          message,
          style: Theme.of(context).textTheme.headlineMedium,
          ),
      ),
    );
  }
}


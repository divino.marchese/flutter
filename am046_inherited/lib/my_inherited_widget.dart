import 'package:flutter/material.dart';

class MyInheritedWidget extends InheritedWidget {
  final String message;

  const MyInheritedWidget({
    super.key,
    required this.message,
    required super.child,
  });

  @override
  bool updateShouldNotify(MyInheritedWidget oldWidget) {
    return message != oldWidget.message;
    // return false
  }

  static MyInheritedWidget? maybeOf(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInheritedWidget>();
  }

  static MyInheritedWidget of(BuildContext context) {
    final MyInheritedWidget? result = maybeOf(context);
    assert(result != null, 'No MyInheritedWidget found in context');
    return result!;
  }
}

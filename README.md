# Flutter Mini Course

Questa serie di esercizi nascono dopo il mio **Android Mini Course** (oramai datato) pensato per i miei studenti dal quale, negli anni, mi sono discostato. 

Aggiornamento AS 2024-2025

# proposta per un percorso

Dividiamo il tutto in nuclei tematici. Ogni esempio è documentato ed il numero ne indica la posizione in funzione della sua data di creazione. Gli esempi indicati con (!) non sono da ritenersi aggiornati pur essendo validi.

## primi esempi

Come in `Android` si aveva l'`Intent` ...

- **am000_strucca_boton**: template iniziale.
- **am001_hello_world**: il primo esempio non quello di default che si genera con `flutter create`.
- **am070_log** (!): per dare qualche messaggio sulla console di Log.  
- **am071_stateful** (!) per comprendere meglio cosa sia una `StatefullWidget`.`
- **am002_navigation** (!): navigare fra schede.
- **am003_passing_arguments** (!): come il precedente solo che usa i `NamedRoutes` in stile *web*, passaggio di argomenti tramite variabile globale.
- **am004_passing_arguments_state** (!): come il precedente ma usando lo stato delle `StatefulWidget` in particolare `ModalRoute`.

## stato ed observer

- **am012_central_state** (anche **am032_todo_list**: accesso allo stato di `Widget` da un suo discendente non diretto.   
- **am046_inherited**: primo esempio con `InheritedWidget`. 
- **am037_inherited_counter** e **am079_inherited_color**: altri due esempi `InheritedWidget` a seguire il precedente. 
- **am078_change_notifier_counter**: un esempio di `ChangeNotifier`, una classe per notificare eventi.
- **am072_hello_provider**: ancora sullo stato globale usando il pacchetto `provider`.   
- **am047_counter_provider**: app basic questa volta fatta col `ChangeNotifierProvider` che ci offre il primo esempio di **macchina a stati**.
- **am048_future_provider**: uno stato globale atteso con un valore di default (iniziale).  
- **am039_bloc_counter**: dove il *bloc pattern* viene realizzato mediante la classe `Cubit` per lo stato globale, ancora una **macchina a stati**.   
- **am070_riverpod_counter** usa `riverpod`, una variante di `provider` e `build_runner`.  

## il framework visto dall'interno

I dettagli di un'interfaccia *responsive* si possono intuire nei due seguenti esempi.
- **am051_key_stateless** (!) e **am051_key_stateful** (!) le `key` e il loro ruolo nel disegno della UI.  
- **am060_render_object** dove costruiamo da zero un `Ẁidget` legandolo al suo `RenderObject`.

## il database interno

In questi esempi vediamo l'uso del DB interno relazionale SQLite e un DB ad oggetti.  
- **am032_todo_list** (!) la base dei successivi esempi.
- **am023_todo_list_floor** (!) la precedente con SQLite che permette di salvare i meos.
- **am062_todo_list_objectbox** (!) interessante soluzione con ObjectBox, un DB ad oggetti (NOSQL).

## widget per eventi

- **am067_change_notifier** (!) per vedere all'opera il *pattern* dell'*Obserber*,
- **am052_future_builder** (!) per `Future` e
- **am053_stream_builder** (!)`Stream`.

## client REST

Prima di procedere con l'esempio invitiamo a leggere alcuni materiali tratti dalla serie "Networking" [qui](https://docs.flutter.dev/cookbook/networking) cer il CRUD: 
- "Fetch data from the internet" [qui](https://docs.flutter.dev/cookbook/networking/fetch-data): per recuperare un solo oggetto.
- "Parse JSON in the background" [qui](https://docs.flutter.dev/cookbook/networking/background-parsing): per recuperare più oggetti.
- "Send data to the internet" [qui](https://docs.flutter.dev/cookbook/networking/send-data),
- "Updating data over the internet using the http package" [qui](https://docs.flutter.dev/cookbook/networking/update-data),
- "Delete data on the internet' [qui](https://docs.flutter.dev/cookbook/networking/delete-data).

Utile la lettuira di `Uri` class [API](https://api.dart.dev/stable/3.3.2/dart-core/Uri-class.html), `jsondecode` (funzione) [API](https://api.dart.dev/stable/3.3.2/dart-convert/jsonDecode.html) e `http` [pub.dev](https://pub.dev/packages/http).
- **am041_tutorial_maree** (!): con una guida dettagliata, adatta anche ai novizi, per costruire l'applicazione. 


## BLE

- **am064_ble_provider** (anche in versione senza *provider* **am064_ble**).

## NFC

- **am065_nfc** una semplice App di test.

## scansione di barcode e qrcode

- **am058_scanner** uno *scanner* per codici a barre o QR code.

## argomenti avanzati

- **am063_code_generator**: in alcuni esempi abbiamo visto `build_runner` all'opera per automatizzare la generazione di file, qui proviamo a creare un pacchetto di prova per il *code generating*.

## da sistemare o con pacchetti obsoleti

Esempi da sistemare al più presto perchè interessanti ma presentanti problemi o non più aggiornati.
- **am054_bloc_redux_counter** come il precedente solo che usa il *redux*.
- **am014_animation** che usa `AnimatedWidget`
- **am055_animatedbuilder** che usa `AnimatedBuilder` 
- **am056_canvas** anima un *canvas*

## il mio tema

Per i miei esempi uso *material design* con il tema seguente
```dart
ThemeData(
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.red, backgroundColor: Colors.white),
      useMaterial3: true,
)
```
per `AppBar` ho scelto il *vintage*
```dart
AppBar(
    title: Text(title),
    foregroundColor: Theme.of(context).colorScheme.surface,
    backgroundColor: Theme.of(context).colorScheme.primary,
),
```


## altri materiali online

[1] "Flutter CookBook" [qui](https://flutter.dev/docs/cookbook).  
[2] "Flutter Gems" [qui](https://fluttergems.dev/).  
[3] "List of state management approaches" [qui](https://docs.flutter.dev/development/data-and-backend/state-mgmt/options).




# am053_stream_builder

**[231217]** Questo esempio usa lo `StreamBuilder` **[API](https://api.flutter.dev/flutter/widgets/StreamBuilder-class.html)**. Usiamo di `AsyncSnapshot` **[API](https://api.flutter.dev/flutter/widgets/AsyncSnapshot-class.html)** non 
`data` ma `requiredData` rispettando cossì il tipo di dato - *null safety* ma non gestendo l'errore. 

La sintassi dello `StreamBuilder` e`un po' involuta, ma l'abbiamo presa, modificandola, dall'esempioo nelle API.
import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

import 'data.dart';
import 'data_tile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am053 stream builder',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am053 stream builder'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static final Random _rnd = Random();

  final Stream<Data> _getData = (() {
    late StreamController<Data> controller;
    controller = StreamController<Data>(onListen: () async {
      for (int i = 1; i <= 7; i++) {
        await Future<void>.delayed(Duration(seconds: 1 + _rnd.nextInt(3)));
        controller.add(Data());
      }
      controller.close();
    });
    return controller.stream;
  })();

  @override
  Widget build(BuildContext context) {
    List<Data> dataList = <Data>[];
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: Center(
        child: StreamBuilder<Data>(
            stream: _getData,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                dataList.add(snapshot.requireData);
                return ListView(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    children:
                        dataList.map((data) => DataTile(data: data)).toList());
              } else {
                return const CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}

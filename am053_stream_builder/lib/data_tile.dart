import 'package:flutter/material.dart';

import 'data.dart';

class DataTile extends StatelessWidget {
  const DataTile({Key? key, required this.data}) : super(key: key);

  final Data data;

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: CircleAvatar(
            backgroundColor: data.color, child: Text(data.value.toString())),
        title: const Text('content'),
        subtitle: Text('data number: ${data.value}'));
  }
}

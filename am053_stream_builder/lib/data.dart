import 'dart:math';

import 'package:flutter/material.dart';

class Data {
  static final Random _rnd = Random();
  static const List<Color> _colors = [
    Colors.blue,
    Colors.red,
    Colors.yellow,
    Colors.green,
    Colors.pink,
    Colors.brown
  ];

  final int _int = _rnd.nextInt(6);

  Color get color => _colors[_int];
  int get value => _int;
}

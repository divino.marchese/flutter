import 'package:flutter/material.dart';

import 'model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'http',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'am025_http_list_scrolling'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  // default message
  String _message = "loaded";
  Color _color = Colors.green; 
  // post
  List<Post> _posts =List<Post>();
  int _dowanloadedPosts = 0;
  // scroll controller
  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    try {
      _fetchPosts();
    } catch(e) {
      setState(() {
        _message = "unable to download";
        _color = Colors.purple;
      });
    }
    super.initState();
  }

  _fetchPosts() async {
    setState(() {
      _color = Colors.orange;
      _message = "loading...";
    });
    for(int id = 1; id<=4; id++) {
      Post post;
      try {
        post = await fetchPost(id+_dowanloadedPosts);
      } catch(e) {
        setState(() {
        _message = "unable to download";
        _color = Colors.purple[300];
        });
        return;
      }
      setState(() {
        _posts.add(post); 
      });
    } 
    setState(() {
      _dowanloadedPosts +=4;
      _color = Colors.green;
      _message = "loaded";
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        _message = "reach the bottom";
        _color = Colors.yellow;
      });
      _fetchPosts();  
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        _message = "reach the top";
        _color = Colors.yellow;
      });
    }
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 50.0,
            color: _color,
            child: Center(
              child: Text(_message),
            ),
          ),
          Expanded(
            child: ListView.builder(
              controller: _controller,
              itemCount: _posts.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text("$index: " + _posts[index].title),
                  subtitle: Text(_posts[index].body)
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
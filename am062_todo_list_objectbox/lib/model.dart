import 'package:objectbox/objectbox.dart';

@Entity()
class Todo {
  @Id()
  int id = 0;

  Todo({required this.name, required this.checked});
  final String name;
  bool checked;
}

# am062_todo_list_objectbox

**[231218]** App ricreata! Riproponiamo la solita lista ma questa volta usiamo ObjectBox, un noto DB ad oggetti [qui](https://objectbox.io/flutter-database/); per il pacchetto Dart [quii](https://pub.dev/packages/objectbox). 
Per sistemare eventuakli problemi
```
flutter clean
flutter pub get
flutter pub upgrade
dart run build_runner build --delete-conflicting-outputs
```
Aggiungiamo su `pubspec.yaml`
```
dependencies:
  flutter:
    sdk: flutter
  objectbox: ^2.4.0
  objectbox_flutter_libs: any

...

dev_dependencies:
  flutter_test:
    sdk: flutter
  build_runner: ^2.0.0
  objectbox_generator: any

## entity

Con un DB ad oggetti si definisce una *entity*
```dart
@Entity()
class Todo {
  @Id()
  int id = 0;

  Todo({required this.name, required this.checked});
  final String name;
  bool checked;
}
```
`@Id()` non sarebbe necessaria in quanto `id` viene recepita, dal suo nome, come chiave.

## la connessione

Definito il *model* possiamo procedere ddando
```
dart run build_runner build
```
Quindi possiamo importare in `main.dart`
```dart
import 'objectbox.g.dart';
```
e possiamo usare in `main.dart`
```dart
_store = await openStore();
```
e quindi dallo `Store`, una connessione, ricavare il `Box<Todo>`, [qui](https://pub.dev/documentation/objectbox/latest/objectbox/Box-class.html) per le API, che useremo per fare richieste.
```dart
_box = _store.box<Todo>();
```
In `main.dart` abbiamo usato due variabili globali
```dart
late final Store _store;
late final Box<Todo> _box;
```
Fra i prodotti di `build_runner` abbiamo anche il file `objectbox-model.json`.

## CRUD e lettura

Ora le operazioni di sincronizzazione fra la lista dei `Todo` ed il DB. Per la lettura
```dart
List<Todo> todos = _box.getAll();
```
ci permette di recuperare titti i `Todo`, altrimenti utilizziamo `Todo get(id)` per un singolo `Todo`. Eventualmente abbiamo uansintassi più elegante per *query* complesse usando la classe `Query`, [qui](https://pub.dev/documentation/objectbox/latest/objectbox/Query-class.html) per le API
```dart
Query<Todo> query = _box.query(...).build();
List<Todo> todos = query.find();
query.close();
```
`query.close()` rilascia la risurse chiudendo la richiesta. Per creare 
```dart
Todo todo = Todo(name: name, checked: false);
todo.id = _box.put(todo);
```
e per aggiornare
```dart
_box.put(todo);
```
riconsocendo una stessa chiave aggiorna!

## nota

- Nella documentazione viene chiesto di usare un min SDK per Android. Noi non abbiamo toccato nulla e funziona! Ma, comunque, è bene fare attenzione.
- `TodoItem` usa una `key` identificata da `ValueKey<int>(todo.id)`.

## materiali

[1] "ObjectBox Getting Started Guide" [qui](https://docs.objectbox.io/getting-started).
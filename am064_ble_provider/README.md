# am064_ble_provider

A new Flutter project.

## pubspec

```
provider: ^6.0.5
permission_handler: ^10.2.0
flutter_reactive_ble: ^5.0.3
```

## uso del provider

Abbiamo realizzato un *service* `BLEService` come nelle migliori tradizioni dando la possibilità all'intera applicazione di accedervi! La nostra applicazione non beneficia delle *notifiche* che permetterebbero di "rollare" di nuovo i *widget*, anche per questo usiamo
```dart
BLEService service = context.read<BLEService>();
```
al posto di `watch`! Il codiceora è più leggibile e meno involuto! `BLEService` è praticamente pronto anche per app più complesse ove si limiti l'uso dei *widget* con stato! 

## materiali

[1] "provider", home page del pacchetto Fluter [qui](https://pub.dev/packages/provider).
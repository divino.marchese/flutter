import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';

class BLEService with ChangeNotifier {
  final FlutterReactiveBle _frb = FlutterReactiveBle();

  String? _deviceId;
  Uuid? _serviceId;
  Uuid? _characteristicId;

  void setDeviceId(String id) {
    _deviceId = id;
    notifyListeners();
  }

  String? getDeviceId() {
    return _deviceId;
  }

  void setServiceId(Uuid id) {
    _serviceId = id;
    notifyListeners();
  }

  Uuid? getServiceId() {
    return _serviceId;
  }

  void setCharacteristicId(Uuid uuid) {
    _characteristicId = uuid;
    notifyListeners();
  }

  Uuid? getCharacteristicId() {
    return _characteristicId;
  }

  Stream<DiscoveredDevice> scan(List<Uuid> servicesUuid) {
    return _frb.scanForDevices(
        withServices: servicesUuid, scanMode: ScanMode.lowLatency);
  }

  Stream<ConnectionStateUpdate> connect() {
    return _frb.connectToDevice(
      id: _deviceId!,
      connectionTimeout: const Duration(seconds: 3),
    );
  }

  Future<List<DiscoveredService>> discoverServices() {
    return _frb.discoverServices(_deviceId!);
  }

  QualifiedCharacteristic getCharacteristic() {
    return QualifiedCharacteristic(
        serviceId: _serviceId!,
        characteristicId: _characteristicId!,
        deviceId: _deviceId!);
  }

  Future<List<int>> readCharacteristic(QualifiedCharacteristic characteristic) {
    return _frb.readCharacteristic(characteristic);
  }

  void clearCache() {
    _frb.clearGattCache(_deviceId!);
  }
}

import 'dart:async';
import 'dart:io' show Platform;

import 'package:am064_ble_provider/ble_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'device_tile.dart';

void main() {
  runApp(const MyApp());
}

class Counter {}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am064 ble provider',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ChangeNotifierProvider(
          create: (_) => BLEService(),
          child: const MyHomePage(title: 'am064 ble provider')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<DiscoveredDevice> _devices = <DiscoveredDevice>[];
  bool _empty = true;
  bool _scanStarted = false;
  StreamSubscription<DiscoveredDevice>? _scanSubscription;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scanSubscription!.cancel();
  }

  Future<bool> _managePermissions() async {
    if (Platform.isAndroid) {
      PermissionStatus scan = await Permission.bluetoothScan.request();
      PermissionStatus connect = await Permission.bluetoothConnect.request();
      PermissionStatus location = await Permission.location.request();
      if (scan == PermissionStatus.granted &&
          connect == PermissionStatus.granted &&
          location == PermissionStatus.granted) {
        return true;
      }
    } else if (Platform.isIOS) {
      return true;
    }
    return false;
  }

  void _checkAndAdd(DiscoveredDevice dDevice) {
    bool idPresent = false;
    for (var element in _devices) {
      if (dDevice.id == element.id) {
        idPresent = true;
      }
    }
    if (!idPresent) {
      setState(() {
        _devices.add(dDevice);
        debugPrint("added device: ${dDevice.id}");
      });
    }
    if (_devices.isNotEmpty) {
      setState(() {
        _empty = false;
      });
    }
  }

  void _startScan(BLEService service) async {
    await _scanSubscription?.cancel();
    setState(() {
      _devices.clear();
      _empty = true;
    });
    bool permGranted = await _managePermissions();
    if (permGranted) {
      setState(() {
        _scanStarted = true;
        _scanSubscription = service.scan([]).listen(
          (dDevice) {
            _checkAndAdd(dDevice);
          },
        );
      });
    }
  }

  void _stopScan() async {
    await _scanSubscription?.cancel();
    setState(() {
      _scanStarted = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    BLEService service = context.read<BLEService>();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: _scanStarted == false
              ? const Text("Push botton and start scanning ...")
              : _empty
                  ? const CircularProgressIndicator()
                  : ListView(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      children: _devices
                          .map((device) => DeviceTile(device: device))
                          .toList())),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _startScan(service);
        },
        tooltip: 'Increment',
        child: _scanStarted
            ? const Icon(Icons.bluetooth_searching)
            : const Icon(Icons.bluetooth),
      ),
    );
  }
}

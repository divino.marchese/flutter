import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:provider/provider.dart';

import 'ble_service.dart';

class DeviceTile extends StatelessWidget {
  const DeviceTile({Key? key, required this.device}) : super(key: key);

  final DiscoveredDevice device;

  _connect(BuildContext context) async {
    BLEService service = Provider.of(context, listen: false);
    service.setDeviceId(device.id);
    service.connect().listen((update) {
      switch (update.connectionState) {
        case DeviceConnectionState.connected:
          _displayDialog(context);
          break;
        default:
          debugPrint(update.connectionState.toString());
      }
    }, onError: (Object error) {
      debugPrint("error");
    });
  }

  Future<List<Card>> _discoverServices(BLEService service) async {
    List<Card> ret = [];
    var services = await service.discoverServices();
    for (final dService in services) {
      Uuid sUuid = dService.serviceId;
      service.setServiceId(dService.serviceId);
      List<Text> characteristics = await _getCharacteristics(service, dService);
      ret.add(Card(
          margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
          color: const Color.fromARGB(212, 255, 252, 252),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(sUuid.toString(),
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          color: Colors.red)),
                  ...characteristics
                ]),
          )));
    }
    return ret;
  }

  Future<List<Text>> _getCharacteristics(
      BLEService service, DiscoveredService dService) async {
    List<Text> ret = <Text>[];
    for (final characteristic in dService.characteristics) {
      Uuid cUuid = characteristic.characteristicId;
      service.setCharacteristicId(cUuid);
      ret.add(Text(cUuid.toString(),
          textAlign: TextAlign.left,
          style: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
          )));
      if (characteristic.isReadable) {
        final characteristic = service.getCharacteristic();
        List<int> values = [];
        final response = await service.readCharacteristic(characteristic);
        for (final n in response) {
          values.add(n);
        }
        ret.add(Text(
          'values: ${values.toString()}',
          style: const TextStyle(
            fontSize: 13,
          ),
        ));
      } else {
        ret.add(const Text("<no readable>"));
      }
    }
    return ret;
  }

  // only for test
  Future<List<Text>> _discoverTest(BLEService service) async {
    await service.discoverServices();
    debugPrint("discovered services");
    return [const Text('services dicovered')];
  }

  _disconnect(BuildContext context, BLEService service) async {
    service.clearCache();
    Navigator.of(context).pop();
  }

  Future<void> _displayDialog(BuildContext context) async {
    BLEService service = context.read<BLEService>();
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(device.name.toString(),
              style:
                  const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          content: SizedBox(
              height: 300,
              width: 350,
              child: SingleChildScrollView(
                  child: FutureBuilder<List<Widget>>(
                      future: _discoverServices(service),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Column(children: snapshot.data!);
                        } else {
                          return const Center(
                            child: Text("searching services ..."),
                          );
                        }
                      }))),
          actions: <Widget>[
            TextButton(
                child: const Text('disconnect'),
                onPressed: () {
                  _disconnect(context, service);
                })
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    String name = device.name;
    if (name == '') {
      name = '<no name>';
    }
    return Card(
      elevation: 3,
      child: ListTile(
        leading: const Icon(Icons.bluetooth),
        title: Text(name),
        subtitle: Text('id (MAC): ${device.id}'),
        onTap: () {
          _connect(context);
        },
      ),
    );
  }
}

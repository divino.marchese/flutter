# tcp client

Modifiche a `pubspec.yaml`: abbiamo aggiunto 
```yaml
dependencies:
  ...
  flutter_launcher_icons: "^0.14.2"

flutter_launcher_icons:
  android: true
  image_path: "assets/images/icon.png"  # Updated path to my actual icon
  adaptive_icon_background: "#ffffff"
  adaptive_icon_foreground: "assets/images/icon.png"
```
Il pacchetto `flutter_launcher_icons`ci permette mediante 
```
dart run flutter_launcher_icons
```
di creare le icone per la nostra app.

## modifica del manifesto

Abbiamo modificato - app Android - il file `AndroidManifest.xml` aggiungendo il permesso per *internet*
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android">
    <uses-permission android:name="android.permission.INTERNET" />                                                                         
    <application
    ...
```

## creazion e degli APK

Seguendo la documentazione ufficiale abbiamo usato
```
flutter build apk --split-per-abi
```
e scelto, fra i `3`il pacchetto `arm`.

## il file `main.dart`

Abbiamo usato una `StatefulWidget` di facile lettura, null'altro!

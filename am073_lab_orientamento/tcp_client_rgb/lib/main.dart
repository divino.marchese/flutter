import 'dart:io';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TCP client RGB',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.blue,
        ),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'TCP client RGB'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Socket? _socket;

  final TextEditingController _controller = TextEditingController();
  String _address = "10.0.2.2:3000";
  bool _connected = false;
  bool _picked = false;
  // create some values
  Color _pickerColor = Color(0xff443a49);

  // getters
  String get _ip {
    int idx = _address.indexOf(":");
    return _address.substring(0, idx).trim();
  }

  int get _port {
    int idx = _address.indexOf(":");
    return int.parse(_address.substring(idx + 1).trim());
  }

  void _connect() {
    String address = _controller.text.trim();
    if (address.isNotEmpty) {
      _address = address;
    }
    // close connection
    setState(() {
      _socket?.flush();
      _socket?.close();
    });
    // open connection
    Socket.connect(_ip, _port).then((sock) {
      setState(() {
        _socket = sock;
        _connected = true;
        logSuccess(_address, "my app");
      });
    }, onError: (e) {
      logError("unable to connect: $e", "my app");
      exit(1);
    });
  }

  // pick color
  void _changeColor(Color color) {
    setState(() => _pickerColor = color);
  }

  void _action() {
    String r = _pickerColor.r.toStringAsFixed(2);
    String g = _pickerColor.g.toStringAsFixed(2);
    String b = _pickerColor.b.toStringAsFixed(2);
    // _socket?.write(color);
    logSuccess("send color", "[$r, $g, $b]");
    setState(() {
      _picked = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Colors.white,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Lab Zuccante",
                style: Theme.of(context).textTheme.headlineLarge),
            SizedBox(
              height: 100,
            ),
            SizedBox(
              width: 200,
              height: 100,
              child: TextField(
                controller: _controller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'IP:port - confirm',
                  hintText: '10.0.2.2:3000',
                ),
              ),
            ),
            ElevatedButton(
                // onPressed: _connected ? () => _showColorPicker(context) : null,
                onPressed: _picked ? _action : () => _showColorPicker(context),
                /*
              child: _connected
                  ? _picked
                      ? const Text('send color')
                      : const Text('pick color')
                  : const Text('disabled'),
              */
                child: _picked
                    ? const Text('send color')
                    : const Text('pick color')),
            SizedBox(
              height: 200,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _connect,
        tooltip: 'connect',
        child: const Icon(Icons.start),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _socket?.flush();
    _socket?.close();
    super.dispose();
  }

  // show dialog

  _showColorPicker(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Pick a color!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: _pickerColor,
            onColorChanged: _changeColor,
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('Got it'),
            onPressed: () {
              setState(() {
                _picked = true;
              });
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

/* logging function */

// green
void logSuccess(String msg, String name) {
  developer.log('\x1B[32m$msg\x1B[0m', name: name);
}

// red
void logError(String msg, String name) {
  developer.log('\x1B[31m$msg\x1B[0m', name: name);
}

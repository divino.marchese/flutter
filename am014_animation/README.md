# am014_animation

Questo esempio riprende il tutorial **[tutorial](https://flutter.dev/docs/development/ui/animations/tutorial)** che invitiamo a leggere.

## Costruzione dell'animazione

Animare significa disegnare, sfruttando il *framerate* di Flutter (60?fps), l'ǜoluzione di un oggetto intendendo l'evoluzione di una o più sue proprietà.

### il controller

`AnimationController` **[API](https://api.flutter.dev/flutter/animation/AnimationController-class.html)** permette di
- eseguire una'animazione
- indicare la durata
esso fa uso di un *ticker*, qui sotto `vsync`, tramite cui vengono scansionati i "fotogrammi".
```dart
AnimationController(duration: const Duration(seconds: 2), vsync: this);
```
Per i metodi usati sio rimanda alle API. Il ticker veien dato dal `mixin` `SingleTickerProviderStateMixin` **[API](https://api.flutter.dev/flutter/widgets/SingleTickerProviderStateMixin-mixin.html)** viene abilitato per un dato sottoalbero di widget.

### l'animazione e la sua evoluzione

Il caso più semplice è dato da un'animazione rettilinea (moto uniforme senza accelerazione)
```dart
Tween<double>(begin: 0, end: 300)
```
`Tween<T>` è un **interpolatore lineare** **[API](https://api.flutter.dev/flutter/animation/Tween-class.html)** mediante `animate(...)` otteniamo un `Animation` pronta per essere animata mediante il controller.

A partire dall'interplatore lineare possiamo mediante un `CurvedAnimation` **[API](https://api.flutter.dev/flutter/animation/CurvedAnimation-class.html)** modificare la nostra animazione dando, secondo la forma di una curba anche personalizzata, aaccelerazione (rallentamere, rendere più veloce, invertire il verso dell'animazione ecc.). Nel nostro esempio vedasi `_animate()` che usa una curva cubica predefinita.
```dart
final Animation<double> curve =
CurvedAnimation(parent: controller, curve: Curves.easeOut);
...
animation = Tween<double>(begin: 0, end: 300).animate(curve);
...
controller.forward();
```

### Animated Widget

Più comodamente si può lavorare con un `AnimatedWidget` **[API](https://api.flutter.dev/flutter/widgets/AnimatedWidget-class.html)**
> *A widget that rebuilds when the given Listenable changes value.*

```dart
class AnimatedLogo extends AnimatedWidget {
  const AnimatedLogo({Key? key, required Animation<double> animation})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        // child: const FlutterLogo(),
        child: Image.asset('assets/images/logo.png'),
      ),
    );
  }
}
```
I *listener* è un *observable* che dice quando il *widget* va aggiornato, vi sono varie ed interessanti sue sottoclassi.

### dispose

Infine liberiamo un po' di memoria con
```dart
@override
void dispose() {
  super.dispose();
  controller.dispose();
}
```

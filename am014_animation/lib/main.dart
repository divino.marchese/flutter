import 'package:flutter/material.dart';

import 'animated_logo.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am014_animation',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am014 animation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;
  bool versus = true;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 300).animate(controller);
    controller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  _animate() {
    setState(() {
      controller.stop();
      controller.reset();
      final Animation<double> curve =
          CurvedAnimation(parent: controller, curve: Curves.easeOut);
      if (versus) {
        animation = Tween<double>(begin: 300, end: 40).animate(curve);
      } else {
        animation = Tween<double>(begin: 40, end: 300).animate(curve);
      }
      versus = !versus;
      controller.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(child: AnimatedLogo(animation: animation)),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _animate(),
          tooltip: 'Increment',
          child: const Icon(
            Icons.replay,
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }
}

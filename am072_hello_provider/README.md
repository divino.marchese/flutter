# am072_hello_provider

In questo esempio presentiamo l'applicazione più banale che possa usare il `Provider`.
Il limite è che qui abbiamo uno stato centralizzato che non dà alcuna modifica. Utile ad esempio coi DB, come vedremo nei prossimi esempi. Per il pacchetto rimandiamo alla sua [1]. `BuildContext` viene modificato in `ReadContext, vedi [2], in modo da offrire il metodo
```dart
context.read<Central>()
```

## materiali

[1] `Provider` home [qui](https://pub.dev/packages/provider/install).  
[2] API [qui](https://pub.dev/documentation/provider/latest/provider/). 

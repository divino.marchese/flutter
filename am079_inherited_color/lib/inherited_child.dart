import 'package:am079_inherited_color/my_inherited.dart';
import 'package:flutter/material.dart';

class InheritedChild extends StatelessWidget {
  const InheritedChild({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      color: MyInherited.of(context).color,
    );
  }
}
import 'package:flutter/material.dart';

class MyInherited extends InheritedWidget {
  const MyInherited({
    super.key,
    required this.color,
    required super.child,
  });

  final Color color;

  static MyInherited? maybeOf(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInherited>();
  }

  static MyInherited of(BuildContext context) {
    final MyInherited? result = maybeOf(context);
    assert(result != null, 'No MyInherited found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(MyInherited oldWidget) => color != oldWidget.color;
  // bool updateShouldNotify(MyInherited oldWidget) => false;

}
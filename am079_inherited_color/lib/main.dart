import 'dart:math';

import 'package:am079_inherited_color/inherited_child.dart';
import 'package:am079_inherited_color/my_inherited.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am079 inherited color'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  get math => null;

  @override
  Widget build(BuildContext context) {

    return MyInherited(
        color: _getRandomColor(),
        child: Scaffold(
          appBar: AppBar(
            foregroundColor: Theme.of(context).colorScheme.surface,
            backgroundColor: Theme.of(context).colorScheme.primary,
            title: Text(widget.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'Show color',
                ),
                SizedBox(height: 20,),
                const InheritedChild()
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => setState(() {}),
            tooltip: 'change color',
            child: const Icon(Icons.play_arrow),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ));
  }

  Color _getRandomColor() {
    return Colors.primaries[Random().nextInt(Colors.primaries.length)];
  }
}

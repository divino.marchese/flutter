# am079_inherited_color

In questo esempio `InheritedChild` viene ricostruito solo se cambia di colore! 
Seza l'inserimento nella `StatefulWidget` di una `InheritedChild` `const` l'aggiornamento avverrebbe ad ogni `setstate(...)`. Altra cosa interessante è valutare il ruolo di 
```dart
nPressed: () => setState(() {}),
```
nell'innesco del *rebuild*.
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am035 serial bluetooth',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am035 serial bluetooth'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // GLOBAL BT DATA
  final FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;
  BluetoothState _state =
      BluetoothState.UNKNOWN; // the state of bluetooth connection
  List<BluetoothDevice> _devicesList = []; // the devices list

  // FOR A SINGLE DEVICE
  BluetoothDevice? _device; // a generic device
  BluetoothConnection? _connection; // connection to device

  bool _btDisabled = true; // disable connect button
  bool _connected = false; // enable sending message
  String _msg =
      'NOTE: If you cannot find the device in the list, ' // red message
      'please pair the device by '
      'going to the bluetooth settings';

  @override
  void initState() {
    super.initState();
    _bluetoothInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                "select a paired device",
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const Text(
                    'device:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  DropdownButton(
                    items: _getDeviceItems(),
                    onChanged: (BluetoothDevice? value) {
                      setState(() => _device = value);
                    },
                    value: _device,
                  ),
                  ElevatedButton(
                    onPressed: _btDisabled
                        ? null
                        : _connected
                            ? _disconnect
                            : _connect,
                    child: Text(_connected ? 'Disconnect' : 'Connect'),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          _device?.name ?? 'device?',
                          style: const TextStyle(
                            fontSize: 20,
                            color: Colors.green,
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed:
                            _connected ? _sendOnMessageToBluetooth : null,
                        child: const Text("ON"),
                      ),
                      ElevatedButton(
                        onPressed:
                            _connected ? _sendOffMessageToBluetooth : null,
                        child: const Text("OFF"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Center(
                  child: Text(
                    _msg,
                    style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // INIT
  Future<void> _bluetoothInit() async {
    // monitoring the Bluetooth adapter state changes.
    _bluetooth.onStateChanged().listen((state) {
      _showSnackBar(state.toString());
      if (state == BluetoothState.STATE_ON) {
        setState(() {
          _state = state;
          _btDisabled = false;
        });
      } else {
        setState(() {
          _state = state;
          _btDisabled = true;
          _msg = "not connected";
        });
      }
    });

    // enable bluetooth
    await _enableBluetooth();

    // It is an error to call [setState] unless [mounted] is true.
    if (!mounted) {
      return;
    }

    // obtain paired devices
    await _getPairedDevices();
    // populate list
    await _getDeviceItems();
  }

  // ENABLE BLUETOOTH
  Future<void> _enableBluetooth() async {
    if (_state != BluetoothState.STATE_ON) {
      await _bluetooth.requestEnable();
    }
    setState(() {
      _btDisabled = false;
    });
  }

  // GET PAIRED DEVICES
  Future<void> _getPairedDevices() async {
    List<BluetoothDevice> devicesList = [];
    // To get the list of paired devices
    try {
      devicesList = await _bluetooth.getBondedDevices();
    } on PlatformException {
      _showSnackBar("Error: unable to get paired devices");
    }
    setState(() {
      _devicesList = devicesList;
    });
  }

  // GET DEVICE ITEMS
  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devicesList.isEmpty) {
      items.add(const DropdownMenuItem(child: Text('NONE'), value: null));
    } else {
      for (BluetoothDevice device in _devicesList) {
        items.add(DropdownMenuItem(
          child: Text(device.name ?? 'void'),
          value: device,
        ));
      }
    }
    return items;
  }

  // CONNECT AND DISCONNECT
  void _connect() async {
    _showSnackBar('connectiong');
    debugPrint('connecting');
    if (_device == null) {
      _showSnackBar('No device selected');
      return;
    }
    try {
      _connection = await BluetoothConnection.toAddress(_device!.address);
      if (_connection != null) {
        debugPrint('we have a connection');
      }
      if (_connection!.isConnected) {
        _showSnackBar('connected');
        setState(() {
          _connected = true;
          _msg = '"${_device!.name}" connected';
        });
      }
    } catch (e) {
      _showSnackBar('exception occurred');
      debugPrint(e.toString());
    }
    _receiveMessage(); // listen messages froms erver
  }

  void _disconnect() async {
    // Closing the Bluetooth connection
    await _connection?.close();
    if (_connection?.isConnected ?? false) {
      _showSnackBar('disconnected');
      setState(() {
        _connected = false;
        _msg = '"${_device!.name}" disconnected';
      });
    }
  }

  // RECEIVE MESSAGE
  void _receiveMessage() {
    if (_connection != null && _connection!.isConnected) {
      _connection?.input?.listen((uint8List) {
        String msg = '';
        for (var b in uint8List) {
          msg += String.fromCharCode(b);
        }
        setState(() {
          _msg = 'received from "${_device!.name}": $msg';
        });
      });
    }
  }

  // Method to send message,
  // for turning the bletooth device on
  void _sendOnMessageToBluetooth() {
    if (_connection != null && _connection!.isConnected) {
      _showSnackBar('Device Turned On');
      _connection?.output.add(ascii.encode('1' '\n\r'));
    }
  }

  // Method to send message,
  // for turning the bletooth device off
  void _sendOffMessageToBluetooth() {
    if (_connection != null && _connection!.isConnected) {
      _showSnackBar('Device Turned Off');
      _connection?.output.add(ascii.encode('0' '\n\r'));
    }
  }

  // SHOW SNACKBAR
  void _showSnackBar(String msg) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(msg)));
  }
}


import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class DiscoveryPage extends StatefulWidget {
  const DiscoveryPage({Key? key}) : super(key: key);

  @override
  _DiscoveryPageState createState() => _DiscoveryPageState();
}

class _DiscoveryPageState extends State<DiscoveryPage> {
  final FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;

  StreamSubscription<BluetoothDiscoveryResult>? _streamSubscription;
  List<BluetoothDiscoveryResult> results = <BluetoothDiscoveryResult>[];

  bool isDiscovering = false;

  @override
  void initState() {
    super.initState();
    if (!isDiscovering) {
      _bluetooth.requestEnable().then(((value) => _startDiscovery()));
    }
  }

  void _startDiscovery() {
    setState(() {
      results.clear();
      isDiscovering = true;
    });
    _streamSubscription =
        FlutterBluetoothSerial.instance.startDiscovery().listen((result) {
      setState(() {
        final existingIndex = results.indexWhere(
            (element) => element.device.address == result.device.address);
        if (existingIndex >= 0) {
          results[existingIndex] = result;
        } else {
          results.add(result);
        }
      });
    });
    _streamSubscription!.onDone(() {
      setState(() {
        isDiscovering = false;
      });
    });
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: isDiscovering
            ? const Text('discovering ...')
            : const Text('discovered!'),
      ),
      body: ListView.builder(
        itemCount: results.length,
        itemBuilder: (BuildContext context, index) {
          return Card(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      _action(index);
                    },
                    child: Text(_actionLabel(index)),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      height: 50,
                      width: 140,
                      child: Text(results[index].device.name ??
                          results[index].device.address)),
                  Container(
                    height: 30,
                    width: 30,
                    color: _getColor(index),
                    child: Center(
                      child: _isBonded(index)
                          ? const Icon(
                              Icons.lock_open_rounded,
                              color: Colors.white,
                            )
                          : const Icon(Icons.lock_outline, color: Colors.white),
                    ),
                  )
                ]),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: isDiscovering ? Colors.grey : Colors.red,
        onPressed: isDiscovering
            ? null
            : () {
                _startDiscovery();
              },
        child: isDiscovering
            ? const Icon(Icons.replay)
            : const Icon(Icons.download),
      ),
    );
  }

  void _action(int index) {
    BluetoothDevice device = results[index].device;
    if (!device.isBonded) {
      _bluetooth.openSettings();
      return;
    }
    Navigator.pushNamed(
      context,
      '/device',
      arguments: device,
    );
  }

  String _actionLabel(int index) =>
      results[index].device.isBonded ? 'go' : 'pair';

  Color _getColor(int index) {
    if (results[index].rssi >= -50) {
      return Colors.green;
    }
    if (results[index].rssi >= -60) {
      return Colors.yellow;
    }
    if (results[index].rssi >= -70) {
      return Colors.orange;
    }
    if (results[index].rssi >= -80) {
      return Colors.red;
    }
    return Colors.purple;
  }

  bool _isBonded(index) {
    return results[index].device.isBonded;
  }
}

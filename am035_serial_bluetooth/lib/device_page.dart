import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class DevicePage extends StatefulWidget {
  const DevicePage({Key? key}) : super(key: key);

  @override
  State<DevicePage> createState() => _DevicePageState();
}

class _DevicePageState extends State<DevicePage> {
  late BluetoothDevice _device;
  late BluetoothConnection _connection;
  bool _isConnected = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _device = _getPageData(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('device: ${_device.name ?? _device.address}'),
      ),
      body: Center(
          child: Column(
        children: [
          Row(
            children: [
              ElevatedButton(
                  onPressed: () {
                    _isConnected ? _disConnect() : _connect();
                  },
                  child: Text(_isConnected ? 'disconnect' : 'connect'))
            ],
          )
        ],
      )),
    );
  }

  BluetoothDevice _getPageData(BuildContext context) {
    BluetoothDevice result =
        ModalRoute.of(context)?.settings.arguments as BluetoothDevice;
    return result;
  }

  Future<void> _connect() async {
    try {
      BluetoothConnection connection =
          await BluetoothConnection.toAddress(_device.address);
      debugPrint('Connected to the device');
      setState(() {
        _connection = connection;
        _isConnected = true;
      });
    } catch (e) {
      debugPrint('CONNECTION ERROR ${e.toString()}');
    }
  }

  Future<void> _disConnect() async {
    await _connection.close();
    if (_connection.isConnected) {
      debugPrint('disconnect');
      setState(() {
        _isConnected = false;
      });
    }
  }
}

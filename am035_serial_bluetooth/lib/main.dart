import 'package:am035_serial_bluetooth/device_page.dart';
import 'package:am035_serial_bluetooth/discovery_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am035 serial bluetooth',
      initialRoute: '/',
      routes: {
        '/': (context) => const DiscoveryPage(),
        '/device': (context) => const DevicePage(),
      },
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
    );
  }
}

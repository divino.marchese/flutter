# am035_serial_bluetooth

## android manifest

```
<uses-permission android:name="android.permission.BLUETOOTH"
    android:maxSdkVersion="30" />
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"
    android:maxSdkVersion="30" />

<!-- Needed only if your app looks for Bluetooth devices.
     If your app doesn't use Bluetooth scan results to derive physical
     location information, you can strongly assert that your app
     doesn't derive physical location. -->
<uses-permission android:name="android.permission.BLUETOOTH_SCAN" />

<!-- Needed only if your app makes the device discoverable to Bluetooth devices. -->
<uses-permission android:name="android.permission.BLUETOOTH_ADVERTISE" />

<!-- Needed only if your app communicates with already-paired Bluetooth devices. -->
<uses-permission android:name="android.permission.BLUETOOTH_CONNECT" />

<!-- Needed only if your app uses Bluetooth scan results to derive physical location. -->
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```


**[doc](https://developer.android.com/guide/topics/connectivity/bluetooth/permissions)**

## gradle

Nel file `build.gradle`
```
android {                                                                                     
   defaultConfig {                                                                             
     minSdkVersion 19                                                                          
   }                                                                                           
}
```

## enable bluetooth

Non fa bulla se il BT è attivo `BluetoothState.STATE_ON`, altrimewnti lo attiva


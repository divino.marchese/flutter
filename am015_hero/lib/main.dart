import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    timeDilation = 1.0; // 1.0 means normal animation speed.

    return MaterialApp(
      title: 'Hero Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: HeroAnimation(),
    );
  }
}

class MyHero extends StatelessWidget {
  
  const MyHero({ Key key, this.photo, this.onTap, this.width }) : super(key: key);

  final String photo;
  final Function onTap;
  final double width;

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          // here we can tap
          child: InkWell(
            onTap: onTap,
            child: Image.asset(
              photo,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}

// the home page
class HeroAnimation extends StatelessWidget {
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("First Route"),
      ),
      body: Center(
      child: MyHero(
          photo: 'images/flippers-alpha.png',
          width: 300.0,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context)  => SecondRoute()
              )
            );
          }
        )
      )
    );
  }
}

// the second Route
class SecondRoute extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Container(
        color: Colors.lightGreen,
        padding: const EdgeInsets.all(16.0),
        alignment: Alignment.topLeft,
        child: MyHero(
          photo: 'images/flippers-alpha.png',
          width: 100.0,
          onTap: () {
            Navigator.pop(context);
          },
        ),
      )
    );
  }
}
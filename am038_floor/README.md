# am038_floor

Floor è un pacchetto che sta raggiungendo la maturità, anche se lentamente, ma che ha il vantaggio di essere molto simile ad altri ORM blasonati sia pr Node che Android o Java. IL pacchetto in Pub lo troviamo [qui](https://pub.dev/packages/floor). Vediamo le impostazioni in `pubspec.yaml` 
```
dev_dependencies:
  flutter_test:
    sdk: flutter
  floor_generator: ^0.18.0
  build_runner: ^1.10.3
```
Esiste anche un sito ufficiale in costruzione (220110) [qui](https://www.floor.codes/).

## ORM

Un **ORM Object-Relational Mapping** oramai costitituisce uno strato di molte applicazioni, sia server che client, vediamo [qui](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping) cosa dice WikiPedia. Un ORM permette di mappare gli oggetti in modo persstente diu di un DB e soprattottuo di mappare operazioni tramite **DAO Data Access Object** su query. Nella directory `entity` definiamo le classiccon l'uso delle *annotation* 
``` dart

@entity
class Person {
  @primaryKey
  final int id;

  final String name;

  Person(this.id, this.name);
}
```
Nella documentazione troviamo altri dettagli per `@entity` più complesse, vedi [qui](https://www.floor.codes/getting-started/). I dati vengono mappati secondo la regola seguente (SQLIte non ha il *boolean* essendo scritto in C)

- `int` -> `INTEGER`
- `double` -> `REAL`
- `String` -> `TEXT`
- `bool` -> `INTEGER` (`0` = `false`, `1` = `true`)
- `Uint8List` -> `BLOB`

**NB**: il costruttore in una classe `@entity` va messo!  
**NB** `@primaryKey` è `int` come da prassi con SQLite.

Possiamo anche lavorare con codici più complessi
``` dart
@Entity(tableName: 'person')
class Person {
  @PrimaryKey(autoGenerate: true)
  final int id;

  @ColumnInfo(name: 'custom_name', nullable: false)
  final String name;

  Person(this.id, this.name);
}
```
Per `1:n` usiamo le **chiavi esterne**
``` dart
@Entity(
  foreignKeys: [
    ForeignKey(
      childColumns: ['owner_id'],
      parentColumns: ['id'],
      entity: Person,
    )
  ],
)
class Dog {
  @PrimaryKey()
  final int id;

  final String name;

  final int ownerId;

  Dog(this.id, this.name, this.ownerId);
}
```

Nella directory `DAO` mappiamo funzioni su query, nel nostro esempio dedichiamo un DAO alla solta `@entity` `Person`
``` dart
@dao
abstract class PersonDao {
  @Query('SELECT * FROM Person')
  Future<List<Person>> findAllPersons();

  @Query('SELECT * FROM Person WHERE id = :id')
  Stream<Person> findPersonById(int id);

  @insert
  Future<void> insertPerson(Person person);

  @Query('DELETE FROM Person')
  Future<void> deleteAllPersons();
}
```
altri esempi di *query* (sempre tratto dalle documentazione)
``` dart
@Query('SELECT * FROM Person WHERE id = :id')
Future<Person?> findPersonById(int id);

@Query('SELECT * FROM Person WHERE id = :id AND name = :name')
Future<Person?> findPersonByIdAndName(int id, String name);

@Query('SELECT * FROM Person')
Future<List<Person>> findAllPersons(); // select multiple items

@Query('SELECT * FROM Person')
Stream<List<Person>> findAllPersonsAsStream(); // stream return

@Query('DELETE FROM Person')
Future<void> deleteAllPersons(); // query without returning an entity

@Query('SELECT * FROM Person WHERE id IN (:ids)')
Future<List<Person>> findPersonsWithIds(List<int> ids); // query with IN clause
```
per il DML
``` dart
@insert
Future<List<int>> insertPersons(List<Person> person);

@update
Future<int> updatePersons(List<Person> person);

@delete
Future<int> deletePersons(List<Person> person);
```
abbiamo come visto anche la possibilità di lavorare - utile per dati pesanti od effetti speciali - la possibilità di lavorare con gli `Stream`
``` dart
// definition
@Query('SELECT * FROM Person')
Stream<List<Person>> findAllPersonsAsStream();

// usage
StreamBuilder<List<Person>>(
  stream: dao.findAllPersonsAsStream(),
  builder: (BuildContext context, AsyncSnapshot<List<Person>> snapshot) {
    // do something with the values here
  },
);
```
A questo punto si tratta di far lavorare `build_runner` [qui](https://pub.dev/packages/build_runner) per le API, e [qui](https://github.com/dart-lang/build/blob/master/docs/getting_started.md) per iniziare; tipicamente abbiamo due pacchetti, uno per generare ed (`floor_generator`) un altro contenente le annotation (vedi i nostri file)
```
flutter packages pub run build_runner build
```
Che risolve il problema delle dipendenze in `database.dart`
``` dart
import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/person_dao.dart';
import 'entity/person.dart';

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Person])
abstract class AppDatabase extends FloorDatabase {
  PersonDao get personDao;
}
```
generando il file `database.g.dart`: il file non va toccato
> `// GENERATED CODE - DO NOT MODIFY BY HAND`

tale file procede come [qui](https://flutter.dev/docs/cookbook/persistence/sqlite) creando tutto ciò che ci serve.

# note

La tastioera non scompare come vorremmo dopo l'immissione ... di vada a [4] ad esempio per sistemare.

# materiali 

[1] "Floor: ORM for Flutter" di Jai khambhayta su Medium [qui](https://medium.com/@khambhaytajaydip/floor-orm-for-flutter-4aa6fe39bb31).
[2] "[Part 2] Code generation in Dart: Annotations, source_gen and build_runner" di Jorge Coca su Medium [qui](https://medium.com/flutter-community/part-2-code-generation-in-dart-annotations-source-gen-and-build-runner-bbceee28697b).
[3] "Conquering Code Generation in Dart – Part 1: Write your first builder" di Jermaine Oppong [qui](https://dev.to/graphicbeacon/conquering-code-generation-in-dart-part-1-write-your-first-builder-4g9g).  
[4] "How to Dismiss the Keyboard in Flutter the Right Way" [qui](https://flutterigniter.com/dismiss-keyboard-form-lose-focus/).
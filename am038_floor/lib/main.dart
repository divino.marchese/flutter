import 'package:flutter/material.dart';

import 'dao/person_dao.dart';
import 'db.dart';
import 'entity/person.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am038_floor',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'am038_floor'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static int _id = 0;
  late AppDatabase _db;
  late PersonDao _dao;
  List<Person> _persons = <Person>[];

  TextEditingController nameController = TextEditingController();

  void _addPerson() {
    setState(() {
      Person person = Person(++_id, nameController.text);
      _dao.insertPerson(person); // update db
      _persons.add(person); // update list
    });
  }

  void _deleteAllPersons() {
    _dao.deleteAllPersons();
    setState(() {
      _persons = <Person>[];
      _id = 0;
    });
  }

  @override
  void initState() {
    super.initState();
    $FloorAppDatabase.databaseBuilder('app_database.db').build().then((db) => {
          db.personDao.findAllPersons().then((ps) => setState(() {
                _db = db;
                _dao = db.personDao;
                _persons = ps;
              }))
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('am038_floor'),
        ),
        body: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: TextField(
              controller: nameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'name',
              ),
            ),
          ),
          ElevatedButton(
            child: const Text('add person'),
            onPressed: () {
              _addPerson();
            },
          ),
          Expanded(
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: _persons.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 50,
                      margin: const EdgeInsets.all(2),
                      color: Colors.red[100],
                      child: Center(
                          child: Text(
                        '${_persons[index].id}: ${_persons[index].name}',
                        style: const TextStyle(fontSize: 18),
                      )),
                    );
                  })),
          ElevatedButton(
            child: const Text('delete db'),
            onPressed: () {
              _deleteAllPersons();
            },
          ),
        ]));
  }
}

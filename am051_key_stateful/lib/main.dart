import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am051 key stateful',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.red, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am051 key stateful'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // SWAP

/*
  List<StatefulButton> buttons = <StatefulButton>[
    StatefulButton(text: 'one', key: UniqueKey()),
    StatefulButton(text: 'two', key: UniqueKey()),
    StatefulButton(text: 'three', key: UniqueKey()),
  ];
  */

  // DO NOT SWAP

  List<StatefulButton> buttons = <StatefulButton>[
    const StatefulButton(text: 'one'),
    const StatefulButton(text: 'two'),
    const StatefulButton(text: 'three'),
  ];

  void _swapButtons() {
    setState(() {
      buttons.insert(2, buttons.removeAt(1));
      buttons.insert(1, buttons.removeAt(0));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('swap buttons',
                style: Theme.of(context).textTheme.headlineSmall),
            Text('in a stateful way',
                style: Theme.of(context).textTheme.headlineSmall),
            const SizedBox(height: 30),
            buttons[0],
            const SizedBox(height: 30),
            buttons[1],
            const SizedBox(height: 30),
            buttons[2],
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _swapButtons,
        tooltip: 'swap buttons',
        child: const Icon(Icons.replay),
      ),
    );
  }
}

class StatefulButton extends StatefulWidget {
  final String text;

  const StatefulButton({super.key, required this.text});

  @override
  State<StatefulButton> createState() => _StatefulButtonState();
}

class _StatefulButtonState extends State<StatefulButton> {
  late final String _text;

  @override
  void initState() {
    _text = widget.text;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      key: widget.key,
      onPressed: () {
        debugPrint('*** click ${_text.toUpperCase()} ****');
      },
      child: Text(_text.toUpperCase()),
    );
  }
}

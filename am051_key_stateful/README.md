# am051_key_stateful

**[240309]** In questa versione ritroviamo il ruolo delle chiavi, questa volta in una `StatefulWIdget`. Da [API](https://api.flutter.dev/flutter/widgets/Widget-class.html) leggiamo
> *Widgets are the central class hierarchy in the Flutter framework. A widget is an immutable description of part of a user interface. Widgets can be inflated into elements, which manage the underlying render tree*

Quindi una tale descrizione può apparire più volte - si pensi ad una lista ed ai suoi *tile* - dando pero luogo a diversi `Element`. Veniamo quindi alle `StatefulWidget`, [API](https://api.flutter.dev/flutter/widgets/StatefulWidget-class.html)

> A StatefulWidget keeps the same State object when moving from one location in the tree to another if its creator used a GlobalKey for its key. Because a widget with a GlobalKey can be used in at most one location in the tree, a widget that uses a GlobalKey **has at most one associated element**. The framework takes advantage of this property when moving a widget with a global key from one location in the tree to another by grafting the (unique) subtree associated with that widget from the old location to the new location (instead of recreating the subtree at the new location). The State objects associated with StatefulWidget are grafted along with the rest of the subtree, which means the State object is reused (instead of being recreated) in the new location. However, in order to be eligible for grafting, the widget must be inserted into the new location in the same animation frame in which it was removed from the old location.

In mancanza della chiave lo stato non viene reinizializzato, lo swap dei *widget* avviene come sopra indicato, ma lo stato non si muove! 

Un [video](https://www.youtube.com/watch?v=kn0EOS-ZiIc&t=218s) spiega bene il tutto!
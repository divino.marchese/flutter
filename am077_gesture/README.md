# am077_gesture

- ver: 250112

Un semplice esempio per comprendere nelle *gesture* la differenza fra
```dart
_x = data.globalPosition.dx;
_y = data.globalPosition.dy;
```
e
```dart
_x = data.localPosition.dx;
_y = data.localPosition.dy
```

## materiali

[1] "Taps, drags, and other gestures" dalla documentazione ufficiale [qui](https://docs.flutter.dev/ui/interactivity/gestures).

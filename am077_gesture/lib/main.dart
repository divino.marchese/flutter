import 'dart:developer' as developer;

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'am077 gesture'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _x = 0;
  double _y = 0;
  String _msg = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        foregroundColor: Theme.of(context).colorScheme.surface,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Test the gesture',
                style: Theme.of(context).textTheme.headlineSmall),
            SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTapDown: (data) {
                // _x = data.globalPosition.dx;
                // _y = data.globalPosition.dy;
                _x = data.localPosition.dx;
                _y = data.localPosition.dy;
                setState(() {
                  _msg = "event: TapDown";
                });
                developer.log("TapDown");
              },
              child: ColoredBox(
                  color: Colors.blueGrey,
                  child: SizedBox(
                      width: 300,
                      height: 300,
                      child: Center(
                        child: Text(
                          "(${_x.toStringAsFixed(2)}, ${_y.toStringAsFixed(2)})",
                          style: TextStyle(color: Colors.white, fontSize: 30),
                        ),
                      ))),
            ),
            SizedBox(height: 50),
            Text(_msg)
          ],
        ),
      ),
    );
  }
}

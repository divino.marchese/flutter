# am066_provider_basic

**[231216]** In questo esempio presentiamo l'applicazione più banale che possa usare il `Provider`.
Il limite fondamentale è che ui abbiamo uno stato centralizzato che non possiede alcuna modifica. Utile ad esempio coi DB, come vedremo nei prossimi esempi. Per il pacchetto rimandiamo alla sua home [qui](https://pub.dev/packages/provider/install). Le API per `Provider` sono [qui](https://pub.dev/documentation/provider/latest/provider/Provider-class.html) ed il `BuildContext` modificato in modo da offrire il metodo
```dart
context.read<Central>()
```
[qui](https://pub.dev/documentation/provider/latest/provider/ReadContext.html). 
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'am066 provider basic',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
              primarySwatch: Colors.red, backgroundColor: Colors.white),
          useMaterial3: true,
        ),
        home: Provider<Person>(
            create: (BuildContext context) => Person(name: "Andrea", age: 53),
            child: const MyHomePage(title: 'am066 provider basic')));
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key, required String title});

  final String title = "am066 provider basic";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Lo stato centralizzato:',
            ),
            Msg()
          ],
        ),
      ),
    );
  }
}

class Msg extends StatelessWidget {
  const Msg({super.key});

  @override
  Widget build(BuildContext context) {
    final String msg = context.read<Person>().msg;
    return Text(
      msg,
      style: Theme.of(context).textTheme.headlineMedium,
    );
  }
}

class Person {
  Person({required this.name, required this.age});

  final String name;
  final int age;

  String get msg => "My name is $name, and I'm $age years old";
}

# am031_key_statefull

Il tutto è stato tratto da [quui](https://medium.com/flutter/keys-what-are-they-good-for-13cb51742e7d), il precedente esempio costituisce un requisito.

## widget ed elements

Leggiamo [qui](https://api.flutter.dev/flutter/widgets/Widget-class.html)

> Widgets can be inflated into elements

> A given widget can be included in the tree zero or more times. In particular a given widget can be placed in the tree multiple times. Each time a widget is placed in the tree, it is inflated into an Element, which means a widget that is incorporated into the tree multiple times will be inflated multiple times.

## Perché il key?

Prima di tutto: `key` è un parametro opzionale ma che può rivelarsi utile. Se noi non passiamo un *key* flutter lo farà per noi!

### update ed unmount degli element

**update** significa sostituire un elemento `active` con un altro `active`.  
**unmount** significa rimuovere l'elemento ed eventualmente sostituirlo con un altro ottenuto da un widget mediante *inflateing*  
Leggiamo infatti [qui](https://api.flutter.dev/flutter/widgets/Element-class.html)
> At some point, the parent might decide to change the widget used to configure this element, for example because the parent rebuilt with new state. When this happens, the framework will call **update** with the new widget. The new widget will always have the same runtimeType and key as old widget. If the parent wishes to change the runtimeType or key of the widget at this location in the tree, it can do so by unmounting this element and inflating the new widget at this location.

Ricordiamo inoltre (vedi [qui](https://api.flutter.dev/flutter/foundation/Key-class.html))
> A new widget will only be used to update an existing element if its key is the same as the key of the current widget associated with the element.

Insistendo [qui](https://api.flutter.dev/flutter/widgets/Widget/key.html)
> If the runtimeType and key properties of the two widgets are operator==, respectively, then the new widget replaces the old widget by updating the underlying element (i.e., by calling Element.update with the new widget). Otherwise, the old element is removed from the tree, the new widget is inflated into an element, and the new element is inserted into the tree.

### per gli statefull widget

Tali widget hanno associato un oggetto che può mutare: lo **stato**. Leggiamo infatti [qui](https://api.flutter.dev/flutter/widgets/StatefulWidget-class.html)
> The framework calls createState whenever it inflates a StatefulWidget, which means that multiple State objects might be associated with the same StatefulWidget if that widget has been inserted into the tree in multiple places. Similarly, if a StatefulWidget is removed from the tree and later inserted in to the tree again, the framework will call createState again to create a fresh State object, simplifying the lifecycle of State objects.

Lo stato di un `StatefullWidget` è associato ad un `BuildContext` che fa riferimento alla posizione del *widget* .... infatti
> Because a given StatefulWidget instance can be inflated multiple times (e.g., the widget is incorporated into the tree in multiple places at once), there might be more than one State object associated with a given StatefulWidget instance. Similarly, if a StatefulWidget is removed from the tree and later inserted in to the tree again, the framework will call StatefulWidget.createState again to create a fresh State object, simplifying the lifecycle of State objects.

Riportando le prime fasi del ciclo di vita ritroviamo esplicitamente il contesto cui è associato lo stato
> - The framework creates a State object by calling StatefulWidget.createState.
The newly created State object is associated with a BuildContext. This association is permanent: the State object will never change its BuildContext. However, the BuildContext itself can be moved around the tree along with its subtree. At this point, the State object is considered mounted.
> - The framework calls initState. Subclasses of State should override initState to perform one-time initialization that depends on the BuildContext or the widget, which are available as the context and widget properties, respectively, when the initState method is called.
...
> ... uring this time, a parent widget might rebuild and request that this location in the tree update to display a new widget with the same runtimeType and Widget.key. When this happens, the framework will update the widget property to refer to the new widget and then call the didUpdateWidget method with the previous widget as an argument. State objects should override didUpdateWidget to respond to changes in their associated widget (e.g., to start implicit animations). The framework always calls build after calling didUpdateWidget, which means any calls to setState in didUpdateWidget are redundant.

## Come otenere un valore per key

Se abbiamo una lista di lemennti differenti, dei valori stringa ad esempio, possiamo usare
``` dart
key = ValueKey("uno"); 
```
vedi [qui](https://api.flutter.dev/flutter/foundation/ValueKey-class.html). Oppure, come in `Dismissible` oppure 
``` dart
key = UniqueKey();
```
ed il tutto va per le `LocalKey`.
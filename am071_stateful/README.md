# am071_stateful

Una `StatefulWidget`, [API](https://api.flutter.dev/flutter/widgets/StatefulWidget-class.html) permette di attivare modifiche della nostra interfaccia in un'ottica *resposive* è un approccio che distanza Flutter dal *pattern* MVC più utile in un UI statica.. 

## Gli elementi del gioco

Un `Widget`, [API](https://api.flutter.dev/flutter/widgets/Widget-class.html) è configurazione immutabile di parte di un interfaccia individuate da un `Element`, una sorta di piano di progetto!

### `State`

Uno `State` [API](https://api.flutter.dev/flutter/widgets/State-class.html) è la "logica interna" di un `StatefulWidget`, si associa cmediante un `BuildContext` (un `Èlement`) al suo *widget*. Il metodo `setState()` genera immediatamente un evento di **notifica** per il *framework* - siamo quindi in una logica, appunto, di programmazione ad eventi o asincrona!

### StatefulWidget

È il `Widget`che si lega al suo `State` che notifica, ovvero innesca `build`: la prima volta immediatamente con un eventuale `initState()` poi con un `setState`. Lo rimarchiamo: il ruolo di tale `Widget` è quello di **innesco** del `Builder`, assieme ad uno `State` che, di fatto, è l'unico oggetto mutabile per uno `StatefulWidget` che non muta ma viene di volta in volta "ricostruito". 

import 'package:equatable/equatable.dart';

abstract class PostEvent extends Equatable {}

// there is only one event
class Fetch extends PostEvent {
  @override
  String toString() => 'Fetch';
}
# am017_bloc_list

In questo esempio proponiamo un'applicazione REST: i servizio ci sono offerti da [jsonplaceholder](http://jsonplaceholder.typicode.com/).

## equatable

[GiyHub](https://github.com/felangel/equatable) permette di ridefinire l'operatore `==` 

## JSONPlaceholder

Il sito è [qui](http://jsonplaceholder.typicode.com/) e permette di servirsi di un FAKE REST per testare le app.

## ListView

Glutter offre un modo pratico per gestire la `ListView` (vedi la differenza con la programmazione Hava in Android).
```
return ListView.builder(
  itemBuilder: (BuildContext context, int index) {
    return index >= state.posts.length
        ? BottomLoader()
        : PostWidget(post: state.posts[index]);
        },
        itemCount: state.hasReachedMax
        ? state.posts.length
        : state.posts.length + 1,
        controller: _scrollController,
  );
)
```
per la documentazione [qui](https://api.flutter.dev/flutter/widgets/ListView-class.html). Per `ScrollController` [qui](https://api.flutter.dev/flutter/widgets/ScrollController-class.html). Da leggere anche [questo](https://medium.com/@DakshHub/flutter-displaying-dynamic-contents-using-listview-builder-f2cedb1a19fb) articolo su Medium.

## no debug

[qui](https://dartcode.org/docs/running-flutter-apps-in-profile-or-release-modes/) come co portarsi per non lanciare in `debug mode`.


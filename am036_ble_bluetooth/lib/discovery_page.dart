import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class DiscoveryPage extends StatefulWidget {
  const DiscoveryPage({Key? key}) : super(key: key);

  @override
  State<DiscoveryPage> createState() => _DiscoveryPageState();
}

class _DiscoveryPageState extends State<DiscoveryPage> {
  final FlutterBlue _flutterBlue = FlutterBlue.instance;

  StreamSubscription<ScanResult>? _streamSubscription;
  List<ScanResult> results = <ScanResult>[];

  bool isDiscovering = false;

  @override
  void initState() {
    super.initState();
    _startDiscovery();
  }

  void _startDiscovery() {
    setState(() {
      results.clear();
      isDiscovering = true;
    });

    _streamSubscription =
        _flutterBlue.scan(timeout: const Duration(seconds: 4)).listen((result) {
      final existingIndex = results
          .indexWhere((element) => element.device.name == result.device.name);
      if (existingIndex >= 0) {
        results[existingIndex] = result;
      } else {
        results.add(result);
        debugPrint('${result.device.name} found! rssi: ${result.rssi}');
      }
    });
    _streamSubscription!.onDone(() {
      setState(() {
        isDiscovering = false;
      });
    });

    setState(() {
      results.clear();
      isDiscovering = true;
    });
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: isDiscovering
            ? const Text('discovering ...')
            : const Text('discovered!'),
      ),
      body: ListView.builder(
        itemCount: results.length,
        itemBuilder: (BuildContext context, index) {
          return Card(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      _action(index);
                    },
                    child: const Text('go'),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      height: 50,
                      width: 140,
                      child: Text(results[index].device.id.id)),
                  Container(
                    height: 30,
                    width: 30,
                    color: _getColor(index),
                    child: Center(
                      child: _isBLE(index)
                          ? const Icon(
                              Icons.lock_open_rounded,
                              color: Colors.white,
                            )
                          : const Icon(Icons.lock_outline, color: Colors.white),
                    ),
                  )
                ]),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: isDiscovering ? Colors.grey : Colors.red,
        onPressed: isDiscovering
            ? null
            : () {
                _startDiscovery();
              },
        child: isDiscovering
            ? const Icon(Icons.replay)
            : const Icon(Icons.download),
      ),
    );
  }

  void _action(int index) {
    BluetoothDevice device = results[index].device;
    Navigator.pushNamed(
      context,
      '/device',
      arguments: device,
    );
  }

  bool _isBLE(int index) =>
      results[index].device.type == BluetoothDeviceType.le;

  Color _getColor(int index) {
    if (results[index].rssi >= -50) {
      return Colors.green;
    }
    if (results[index].rssi >= -60) {
      return Colors.yellow;
    }
    if (results[index].rssi >= -70) {
      return Colors.orange;
    }
    if (results[index].rssi >= -80) {
      return Colors.red;
    }
    return Colors.purple;
  }
}

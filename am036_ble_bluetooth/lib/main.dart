import 'package:flutter/material.dart';

import 'device_page.dart';
import 'discovery_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am035 serial bluetooth',
      initialRoute: '/',
      routes: {
        '/': (context) => const DiscoveryPage(),
        '/device': (context) => const DevicePage(),
      },
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
    );
  }
}

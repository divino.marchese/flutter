import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class DevicePage extends StatefulWidget {
  const DevicePage({Key? key}) : super(key: key);

  @override
  State<DevicePage> createState() => _DevicePageState();
}

class _DevicePageState extends State<DevicePage> {
  late BluetoothDevice _device;
  bool _isConnected = false;
  List<BluetoothService> _services = <BluetoothService>[];

  @override
  Widget build(BuildContext context) {
    _device = _getPageData(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('device: ${_device.name}'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    _isConnected ? _disConnect() : _connect();
                  },
                  child: Text(_isConnected ? 'disconnect' : 'connect'))
            ],
          ),
          ListView.builder(
            itemCount: _services.length,
            itemBuilder: (BuildContext context, index) {
              return Card(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          _getCharacteristics(index);
                        },
                        child: const Text('go'),
                      ),
                      Container(
                          padding: const EdgeInsets.all(16.0),
                          height: 50,
                          width: 140,
                          child: Text(results[index].device.id.id)),
                      Container(
                        height: 30,
                        width: 30,
                        color: _getColor(index),
                        child: Center(
                          child: _isBLE(index)
                              ? const Icon(
                                  Icons.lock_open_rounded,
                                  color: Colors.white,
                                )
                              : const Icon(Icons.lock_outline,
                                  color: Colors.white),
                        ),
                      )
                    ]),
              );
            },
          ),
        ],
      )),
    );
  }

  BluetoothDevice _getPageData(BuildContext context) {
    BluetoothDevice result =
        ModalRoute.of(context)?.settings.arguments as BluetoothDevice;
    return result;
  }

  Future<void> _connect() async {
    try {
      await _device.connect();
    } catch (e) {
      debugPrint("ERROR: ${e.toString()}");
    }
    setState(() {
      _isConnected = true;
    });
  }

  Future<void> _disConnect() async {
    await _device.disconnect();
    setState(() {
      _isConnected = false;
    });
  }

  Future<void> _getServices() async {
    _services = await _device.discoverServices();
  }

  Future<void> _getCharacteristics() async {}
}

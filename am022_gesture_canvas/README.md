# am022_gesture_canvas

Questo esempio riprende il precedente con alcune aggiunte:  
1) gestiamo la gesture **Pan** in modo da muovere le due coordinate contemporaneamente, vedi disambiguation,  
2) nell'esempio precedente abbiamo evitato l'ostacolo di come convertire **global position** to **local position**, qui invece lo affrontiamo
```
onPanDown: (DragDownDetails details) {
  RenderBox box = context.findRenderObject();
  Offset local = box.globalToLocal(details.globalPosition);
  update(local.dx, local.dy);
  print("pan down");
},
```
poiché `cobtext` fa riferimento ad una widget istanziata abbiamo creato appositamente `MyGestureDetector` e `update` è una funzione che passiamo come argomento del costruttore (trick già utilizzato altrove per far sì che statelesswidget possano modificare lo stato di una statefullwifget di cui son fliglie):
```
RenderBox box = context.findRenderObject();
```
[qui](https://medium.com/flutter-community/flutter-what-are-widgets-renderobjects-and-elements-630a57d05208) un interessante articolo per capirci di , [qui](https://api.flutter.dev/flutter/widgets/BuildContext/findRenderObject.html) le API del metodo: si cerca il più vicino (inclusione) `RenderObject` del widget corrente, `context` è il suo contesto, e a questo punto possiamo convertire le coordinate
```
Offset local = box.globalToLocal(details.globalPosition);
```

[[_TOC_]]

# am041_maree_tutorial

**[240403]** Un tutorial per il laboratorio. Realizziamo l'"App Maree" che scarica i dati dal Comune di Venezia, più precisamente da [qui](http://dati.venezia.it/sites/default/files/dataset/opendata/livello.json). Pensato per i novizi contiene anche elementi avanzati di dettaglio.

# creare applicazione

Lo STEP 0 è equivalente a dare a terminale
```
flutter create
```

## STEP 0: l'applicazione base di Flutter

`CTRL`+`SHIFT`+`P` `Flutter: new Project` `New Application` e dare il nome del progetto; chi scrive ga scelto `am041_maree_tutorial`.

a questo punto appare l'applicazione che Flutter ci propone ricca di commenti che poi, per comodità, andremo a cancellare: un bottone che incrementa un contatore: provare tale applicazione con `run`; ci verrà proposta su Google Crome, ma possiamo scegliere un emulatore o un nostro dispositivo configurato per lo sviluppo. Noi useremo proprio questa applicazione modificandola ed aggiungendo elementi.

# l'onda

## STEP 1: installiamo il pacchetto per l'onda

In `pubspec.yaml` è il file di configurazione della nostra applicazione; procediamo modificando come segue
```
dependencies:
  flutter:
    sdk: flutter
  wave: ^0.2.2
```
e salviamo (così scaricheremo il pacchetto). 

## STEP 2: modifichiamo l'estetica

Passiamo a `main.dart` il file principale della nostra applicazione: in Una app Flutter lì c'è tutto!
Cambiamo il titolo ed il colore della barra *material design*.
```dart
...
return MaterialApp(
    title: 'App Maree',
    theme: ThemeData(
        primarySwatch: Colors.teal,
    ),
    home: const MyHomePage(title: 'App Maree'),
);
...
```
per la classe `Colors` che ci dà a disposizione i colori vedi [qui](https://api.flutter.dev/flutter/material/Colors-class.html)

Cancelliamo pure i commenti `// ...` che in verde: essi aiutano il neofita a capire che oggetti stiamo usando nella nostra app: dart infatti è un linguaggio da oggetti.


## STEP 3: disegniamo l'onda

`Center` è un `Widget` che ci permette di centrare il suo contenuto, vedi [qui](https://api.flutter.dev/flutter/widgets/Center-class.html) per le API. Ora tocchiamo solo `Center`
```dart
@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center
```
e lo cambiamo così
```dart
body: Center(
          child: Stack(
        children: <Widget>[
          Container(
              alignment: Alignment.bottomCenter,
              child: WaveWidget(
                config: CustomConfig(
                  colors: [
                    // tre onde dello stesso colore con trasparenza
                    Colors.teal.withOpacity(0.3),
                    Colors.teal.withOpacity(0.3),
                    Colors.teal.withOpacity(0.3),
                  ],
                  durations: [4000, 5000, 7000], // durata delle onde in ms
                  heightPercentages: [
                    0.01,
                    0.02,
                    0.005
                  ], // altezza in percentuale di ogni onda
                  blur: const MaskFilter.blur(
                      BlurStyle.solid, 3), // bordo dell'onda sfocato
                ),
                waveAmplitude: 10.00, //
                waveFrequency: 3, // number of curves in waves
                backgroundColor: Colors.white, // background colors
                size: const Size(
                  double.infinity,
                  200,
                ),
              )),
          Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Rialto',
                  style: TextStyle(color: Colors.teal.shade700, fontSize: 40)),
              Text('12m',
                  style: TextStyle(color: Colors.teal.shade700, fontSize: 80)),
            ],
          )),
        ],
      )),
```
`CTRL`+`SHIFT`+`i` per aggiustare il codice in modo che sia ben indentato, attenzione alle parentesi e a non cancellare in modo scorretto: `CTRL`+ `z` per tornare indietro. Ed abbiamo quindi messo la nostra onda. `Stack` è un altro `Widget` e ci permette - lo dice la parola - di sovrapporre un *widget* ad un altro, l'ultimo sta in cima e si sovrappone al sottostante in una lista chiamata `children`, [qui](https://api.flutter.dev/flutter/widgets/Stack-class.html) per le API.

# I dati sulle maree

Ora procediamo al recupero dati dal sito

## STEP 4: installiamo il pacchetto http e prepariamo il *model*

Per fare richieste `http` ad un server web aggiungiamo in `pubspec.yaml` un'altra dipendenza
```
flutter:
    sdk: flutter
  wave: ^0.2.2
  http: ^1.2.1
```
[qui](https://pub.dev/packages/http) il pacchetto! Creiamo il file `data.dart` ovvero il *model*: grazie ad esso i dati che vengono dal sito in link diventano oggetti che possiamo usare nella nostra app. Il sito del Comune di Venezia offre oggetti `json` che rappresentano i nostri dati
```
{
  "ordine":"2",
  "ID_stazione":"01033",
  "stazione":"Chioggia porto",
  "nome_abbr":"Ch_Porto",
  "latDMSN":"451357.00",
  "lonDMSE":"121650.00",
  "latDDN":"45.232500",
  "lonDDE":"12.280556",
  "data":"2020-01-02 16:50:00",
  "valore":"-0.03 m"
}
```
Provare a convertire il dato sopra usando [questo](https://javiercbk.github.io/json_to_dart/) tool web e si troverà parte del codice in `data.dart`. 

**NB** L'accesso ad internet richiede opportuni permessi nel *manifest* dell'applicazione Android: andare al file!

## STEP 5: portiamo i dati sull'interfaccia

Modifichiamo `main.dart` come segue (ridefiniamo lo stato)
```dart
class _MyHomePageState extends State<MyHomePage> {

  List<Data> _data = [];
```
abbiamo cancellato `int_ counter = 0;`. La lista contiene tutti i dati delle stazioni di marea.
Aggiungiamo a seguire (spaziando di una riga)
```dart
void _getData() async {
    List<Data> data = await fetchDataList();
    setState(() {
      _data = data;
    });
  }
```
questa è l'azione che compirà il bottone: andiamo quindi alla modifica! 
```dart
floatingActionButton: FloatingActionButton(
    onPressed: _getData,
    tooltip: 'Increment',
    child: const Icon(Icons.arrow_downward),
),
```
A questo punto dobbiamo portare i dati nell'interfaccia della app; procediamo con la modifica (se non ci sono dati mostreremo un messaggio altrimenti porteremo il primo risultato), sostituiamo `Center(...)` , il testo alla fine, ponendo attenzione alle parentesi
```dart
Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _data.isEmpty
                ? <Widget>[
                    Text('get data!',
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 40))
                  ]
                : <Widget>[
                    Text(_data[0].data,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 30)),
                    Text(_data[0].stazione,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 30)),
                    Text(_data[0].valore,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 80)),
                  ],
          )),
```
(attenti alle parentesi) di nuovo, eventualmente, aggiustiamo il codice con `CTRL`+`SHIFT`+`i` in modo che sia ben indentato.

## STEP 6: aggiustiamo l'altezza dell'onda con la marea

Le onde si alzano se la marea si alza
```dart
size: Size(
    double.infinity,
    100 + (_data.isEmpty ? 0 : _data[0].valoreDouble * 200),
),
```
Ora praticamente la nostra app sarebbe terminata, ma possiamo aggiungere l'accelerometro! In tal modo con il movimento della mano possiamo aggiornare i dati.

# l'accelerometro

Il pacchetto che utilizzeremo è `sensors_plus` [qui](https://pub.dev/packages/sensors_plus). 

## STEP 7: aggiungiamo l'accelerometro ed utilizziamolo

Aggiorniamo quindi ancora `pubspec.yaml`
```
flutter:
  sdk: flutter
wave: ^0.2.2
http: ^1.2.1
sensors_plus: ^5.0.0
```
sistemiamo `main.dart`
```dart
class _MyHomePageState extends State<MyHomePage> {

  List<Data> _data = [];
  late StreamSubscription _userAccelerometerSub;
  double? _eventZ;

  @override
  void initState() {
    super.initState();
    _userAccelerometerSub =
        accelerometerEventsStream().listen((AccelerometerEvent event) {
      setState(() {
        _eventZ = event.z;
      });

      if (_eventZ != null && _eventZ!.abs() > 2) {
        _getData();
      }
    });
  }

  @override
  void dispose() {
    _userAccelerometerSub.cancel();
    super.dispose();
  }

  void _getData() async {
      ...
```
Senza entrare troppo nei dettagli osserviamo seil nostro telefonino viene scosso verticalmente
```dart
if (... _eventZ.abs() > 2) _getData();
```
`abs()` indica sia verso il basso che verso l'alto!

# i permessi sul manifest

Come da documentazione di Android dobbiamo dare per terminare i seguenti permessi.
- **uses permission** vedi [qui](https://developer.android.com/guide/topics/manifest/uses-permission-element)
- **internet** vedi [qui](https://developer.android.com/training/basics/network-ops/connecting).
- **sensori di movimento** vedi [qui](https://developer.android.com/guide/topics/sensors/sensors_motion).
- **user activity** vedi [qui](https://developer.android.com/guide/topics/location/transitions).

- **sensori di movimento** vedi `ACTIVITY_PERMISSION` [qui](https://developer.android.com/reference/android/Manifest.permission#ACTIVITY_RECOGNITION).

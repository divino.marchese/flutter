import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

import 'data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Maree',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.teal, backgroundColor: Colors.white),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'App Maree'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Data> _data = [];
  late StreamSubscription _userAccelerometerSub;
  double? _eventZ;

  @override
  void initState() {
    super.initState();
    _userAccelerometerSub =
        accelerometerEventStream().listen((AccelerometerEvent event) {
      setState(() {
        _eventZ = event.z;
      });

      if (_eventZ != null && _eventZ!.abs() > 2) {
        _getData();
        debugPrint("moving Z");
      }
    });
  }

  @override
  void dispose() {
    _userAccelerometerSub.cancel();
    super.dispose();
  }

  void _getData() async {
    List<Data> data = await fetchDataList();
    setState(() {
      _data = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Theme.of(context).colorScheme.onBackground,
      ),
      body: Center(
          child: Stack(
        children: <Widget>[
          Container(
              alignment: Alignment.bottomCenter,
              child: WaveWidget(
                config: CustomConfig(
                  colors: [
                    // tre onde dello stesso colore con trasparenza
                    Colors.teal.withOpacity(0.3),
                    Colors.teal.withOpacity(0.3),
                    Colors.teal.withOpacity(0.3),
                  ],
                  durations: [4000, 5000, 7000], // durata delle onde in ms
                  heightPercentages: [
                    0.01,
                    0.02,
                    0.005
                  ], // altezza in percentuale di ogni onda
                  blur: const MaskFilter.blur(
                      BlurStyle.solid, 3), // bordo dell'onda sfocato
                ),
                waveAmplitude: 10.00, //
                waveFrequency: 3, // number of curves in waves
                backgroundColor: Colors.white, // background colors
                size: Size(
                  double.infinity,
                  100 + (_data.isEmpty ? 0 : _data[0].valoreDouble * 200),
                  // 100 + 360
                ),
              )),
          Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _data.isEmpty
                ? <Widget>[
                    Text('get data!',
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 40))
                  ]
                : <Widget>[
                    Text(_data[0].data,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 30)),
                    Text(_data[0].stazione,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 30)),
                    Text(_data[0].valore,
                        style: TextStyle(
                            color: Colors.teal.shade700, fontSize: 80)),
                  ],
          )),
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: _getData,
        tooltip: 'get data',
        child: const Icon(Icons.arrow_downward),
      ),
    );
  }
}

# am061_riverpod_counter

**[240214]** `riverpod` è un pacchetto che riscrive il `provider`, [home](https://riverpod.dev/), [pub.dev](https://pub.dev/packages/flutter_riverpod). La app che presentiamo è sempre quelal di partenza "strucca boton".  
 
## modello dinamico

Lo stato viene gestito secondo il sistema dinamico, qui programmato esplicitamente
```
x(t), u(t) -> x(t+1)
```
`x(t)` stato `u(t)` ingresso al tempo `t`: dato lo stato e l'ingresso al tempo `t` otteniamo lo stato all'ingresso `t+1`. Lo stato ci permette di ridurre il modello dinamico ad un modello a memoria `1`o Markoviano.


## implementazione

Abbiamo `5` classi per gestire l'interazione: un modo elegante per presentare l'*Observer pattern*.

`ProviderScope` [API](https://pub.dev/documentation/hooks_riverpod/latest/hooks_riverpod/ProviderScope-class.html) è ra *widget root* di una app che usa `riverpod`.

`StateNotifier`, [API](https://pub.dev/documentation/hooks_riverpod/latest/hooks_riverpod/StateNotifier-class.html), è l'*observable* che mette, fra l'altro, a disposizione la proprietà `state`: il **modello dinamico** lo troviamo nella classe
```dart
class Counter extends StateNotifier<int> {
  Counter() : super(0);
  void increment() => state++;
}
```

`StateNotifierProvider`, [API](https://pub.dev/documentation/hooks_riverpod/latest/hooks_riverpod/StateNotifierProvider-class.html) permette di creare uno `StateNotifier` ed esporlo, in sostanza crea l'*observable*, lo `StateNotifier`, e permette di registrare gli *observer* 
Mediante la proprietà `provider` otteniamo lo `StateNotifier`. 

`WidgetRef` [API](https://pub.dev/documentation/hooks_riverpod/latest/hooks_riverpod/WidgetRef-class.html) è un tramite grazie a cui possiamo fornire i `Provider` all'interno di `ConsumerWidget` (vedi qui sotto), fornisce anche i metodi `read`e `watch`.

`ConsumerWidget` [APU](https://pub.dev/documentation/hooks_riverpod/latest/hooks_riverpod/ConsumerWidget-class.html) è una `StatelessWidget` che ascolta i `Provider` e, grazie a `watch` fornitoci da `WidgetRef` viene, in caso, ricostruita.
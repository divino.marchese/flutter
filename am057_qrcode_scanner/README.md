# am057_qrcode_scanner

Usiamo il pacchetto
- `qr_code_scanner` **[home](https://pub.dev/packages/qr_code_scanner)**.

Qaalche problema può risultare dal plugin Kotlin e da Gradle.

## Nota

QUesto esempio lo presentiamo in quanto completo e "ben fatto", notiamo però quanto segue. 
> *Since the underlying frameworks of this package, zxing for android and MTBBarcodescanner for iOS are both not longer maintaned, this plugin is no longer up to date and in maintenance mode only. Only bug fixes and minor enhancements will be considered.*


Ecco alcune classi rilevanti.  
`BarCode` **[API](https://pub.dev/documentation/qr_code_scanner/latest/qr_code_scanner/Barcode-class.html)** mantiene informazioni sul codice scansionato.    
`QRViewController` **[API](https://pub.dev/documentation/qr_code_scanner/latest/qr_code_scanner/QRViewController-class.html)** gestisce lo *stream* dei dati.  
Il metodo `reassemble()` utile solo in fase di sviluppo viene chiamato ad esempio durante un *hot reload*.  
`Expanded` **[API](https://api.flutter.dev/flutter/widgets/Expanded-class.html)** interessante *widget* di *layout*.  
`BoxFit` è un *enumeration* **[API](https://api.flutter.dev/flutter/painting/BoxFit.html)**, `contain`
 indica: più largo possibile per contenere il suo contenuto.  
`MediaQuery` permette di recuperare informazioni sullo spazio offerto dal nostro dispositivo **[API](https://api.flutter.dev/flutter/widgets/MediaQuery-class.html)**, utile se ruotiamo il dispositivo o se il nostro display non è così grande.  
`QRViw` è la parte centrale della nostra applicazione **[API](https://pub.dev/documentation/qr_code_scanner/latest/qr_code_scanner/QRView-class.html)** (la chiave globale, non necessaria, è utile in caso di rotazione o altro per riciclare il *widget*).
- `onQRViewCreated`: viene chiamato quando al momento della creazione,  
- `onPermissionSet`: ... una volta che i permessi vengono dati!

## QR code

Per generare un QRCode di prova andiamo su

https://www.online-qrcode-generator.com/
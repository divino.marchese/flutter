# am047_counter_provider

In questo esempio riprendiamo le nostre considerazioni sulla gestione delo **stato** di una app: abbandoniamo l'uso di una `Widget` con stato per usare il `Provider`, vedi [1].
- `ChangeNotifierProvider`. [API](https://pub.dev/documentation/provider/latest/provider/ChangeNotifierProvider-class.html), ha come *generic* `Counter`, il sistema dinamico per la gestione dello stato centralizzato. Per recuperare tale l'oggetto usiamo
```dart
Counter counter = context.watch<Counter>();
```
che poermette non solo di ottenere il valore ma anche la *subscrption* che permetterà di rilevare le notifiche (aggiornando, `read` legge ma non si aggiorna ioè non riceve le notifiche).
- `Counter` "mixa" `ChangeNotifier` [qui](https://api.flutter.dev/flutter/foundation/ChangeNotifier-class.html) permette di notificare le modifiche. Col metodo
```dart
void notifyListeners() 
```
[API](https://api.flutter.dev/flutter/foundation/ChangeNotifier/notifyListeners.html), notifica agli *observers* il cambiamento di stato. La classe la abbiamo aggiunta a `Counter` come *mixin*, potevamo estenderla.

## la macchina a stati

Invitiamo a riflettere sulla differenza sostanziale fra `InheritedWidget` e `Provider`. Quest'ultimo ci permette di leggere il nostro approccio in termini di **macchina a stati**
regolata da un evoluzione di stato del tipo
```
x(t), u(t) -> x(t+1)
```
dato lo stato `x(t)` il suo prossimo `x(t+1)` valore dipende anche da un'azione esterna `u(t)`.

## materiali

[1] `Provider` [qui](https://pub.dev/packages/provider/install).  
